package com.github.nikolaymakhonin.utils_ui.patterns.mvp;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;

public abstract class BaseView<TPresenter, TViewModel> extends RelativeLayout {

    //region Constructors

    public BaseView(Context context) {
        super(context);
        initControls();
    }

    public BaseView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initControls();
    }

    public BaseView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initControls();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BaseView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initControls();
    }

    //endregion

    //region Init Controls

    protected void initControls() {

    }

    //endregion

    //region Attached

    private boolean _attached;

    public boolean isAttached() {
        return _attached;
    }

    private final Subject<Boolean, Boolean> _attachedSubject = PublishSubject.create();

    public Observable<Boolean> attachedObservable() {
        return _attachedSubject;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        _attachedSubject.onNext(_attached = true);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        _attachedSubject.onNext(_attached = false);
    }

    //endregion

    //region Update View

    public abstract void updateView(TViewModel placeInfo);

    //endregion
}
