package com.github.nikolaymakhonin.utils_ui.contracts;

import rx.Observable;

public interface IActivityPermissionsCallback {
    Observable<RequestPermissionsResult> getRequestPermissionsObservable();
}
