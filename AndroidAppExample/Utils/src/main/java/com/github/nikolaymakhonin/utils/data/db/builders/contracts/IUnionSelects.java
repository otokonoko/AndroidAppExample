package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

import com.github.nikolaymakhonin.utils.lists.list.IList;

public interface IUnionSelects extends ISelect {
    ISelect getInnerSelect();
    void setInnerSelect(ISelect select);    
    IList<IUnionSelect> getUnionSelects();
}