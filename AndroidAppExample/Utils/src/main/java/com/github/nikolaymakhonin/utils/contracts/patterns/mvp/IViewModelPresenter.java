package com.github.nikolaymakhonin.utils.contracts.patterns.mvp;

import com.github.nikolaymakhonin.utils.contracts.patterns.ITreeModified;

public interface IViewModelPresenter<TView extends IViewModelView, TViewModel extends ITreeModified>
    extends IViewPresenter<TView>
{

    TViewModel getViewModel();

    void setViewModel(TViewModel value);

}
