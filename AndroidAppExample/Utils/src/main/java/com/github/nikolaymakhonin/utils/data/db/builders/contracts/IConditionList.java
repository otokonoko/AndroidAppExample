package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

import com.github.nikolaymakhonin.utils.lists.list.IList;

public interface IConditionList extends ICondition {
    IList<ICondition> Conditions();
    String getSeparator();
}
