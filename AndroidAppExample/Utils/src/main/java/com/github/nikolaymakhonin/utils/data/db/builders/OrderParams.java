package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.CompareUtils;
import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IOrderParams;
import com.github.nikolaymakhonin.utils.strings.StringUtilsExt;

import java.util.List;

public class OrderParams implements IOrderParams {
    private final String _field;
    private final String _tableName;
    private final boolean _descending;
    
    public OrderParams(String field, boolean descending) {
        _field = field;
        _tableName = null;
        _descending = descending;
    }
    
    public OrderParams(String tableName, String field, boolean descending) {
        _field = field;
        _tableName = tableName;
        _descending = descending;
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        if (!StringUtilsExt.isNullOrEmpty(_tableName)) sb.append("[").append(_tableName).append("].");
        sb.append(_field);
        if (_descending) sb.append(" DESC");
    }
    
    @Override
    public void addBindValues(List<Object> destBindValues) {
    }

    @Override
    public String getTableName() {
        return _tableName;
    }   

    @Override
    public String getFieldName() {
        return _field;
    }   

    @Override
    public boolean getDescending() {
        return _descending;
    }   

    @Override
    public ICloneable Clone() {
        return new OrderParams(_tableName, _field, _descending);
    }

    @Override
    public int compareTo(IOrderParams another) {
        int i;
        if ((i = CompareUtils.Compare(getTableName(), another.getTableName(), true)) != 0) return i;
        if ((i = CompareUtils.Compare(getFieldName(), another.getFieldName(), true)) != 0) return i;
        return 0;
    }
    
    @Override
    public int hashCode() {
        
        return (_tableName == null ? 0 : _tableName.hashCode()) ^ _field.hashCode();
    }
    
    @Override
    public boolean equals(Object o) {
        IOrderParams another = (IOrderParams)o;
        return compareTo(another) == 0;
    }
}
