package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.CloneUtils;
import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ICondition;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IConditionList;
import com.github.nikolaymakhonin.utils.lists.list.IList;
import com.github.nikolaymakhonin.utils.lists.list.SortedList;

import java.util.List;

public class ConditionList implements IConditionList {
    private final String            _separator;
    private       IList<ICondition> _conditions;
    
    public ConditionList(String separator) {
        _separator = separator;
    }
    
    public ConditionList(IConditionList sourceList) {
        _separator = sourceList.getSeparator();
        CloneUtils.CloneList(sourceList.Conditions(), Conditions());
    }
    
    @Override
    public ICloneable Clone() {
        return new ConditionList(this);
    }
    
    @Override
    public IList<ICondition> Conditions() {
        return _conditions != null ? _conditions : (_conditions = new SortedList<>(false, false));
    }
    
    @Override
    public String getSeparator() {
        return _separator;
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        if (_conditions != null) {
            int length = _conditions.size();
            if (length > 0) {
                int firstLength = sb.length();
                int oldlength = firstLength;
                String separator = " " + _separator + " ";
                for (int i = 0; i < length; i++) {
                    if (sb.length() > oldlength) sb.append(separator);
                    ICondition condition = _conditions.get(i);
                    oldlength = sb.length();
                    condition.appendSqlString(sb);
                }
                if (sb.length() == oldlength && sb.length() > firstLength) {
                    sb.setLength(sb.length() - separator.length());
                }
            }
        }
    }
    
    @Override
    public void addBindValues(List<Object> destBindValues) {
        if (_conditions != null) {
            for (ICondition condition : _conditions) {
                condition.addBindValues(destBindValues);
            }
        }
    }    
}
