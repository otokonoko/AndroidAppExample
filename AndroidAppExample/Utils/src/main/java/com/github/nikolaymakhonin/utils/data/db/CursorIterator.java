package com.github.nikolaymakhonin.utils.data.db;

import java.util.Iterator;

import android.database.Cursor;

public abstract class CursorIterator<T> implements Iterator<T> {
    private final Cursor _cursor;
    boolean _firstRow = true;
    
    public CursorIterator(Cursor cursor) {
        _cursor = cursor;
    }
    
    protected abstract void initFieldIndexes(Cursor cursor);
    
    protected abstract T readItem(Cursor cursor);
    
    @Override
    public boolean hasNext() {
        if (_firstRow) {
            if (!_cursor.moveToFirst()) return false;
            _firstRow = false;
            initFieldIndexes(_cursor);
            return true;
        } else {
            return _cursor.moveToNext();
        }
    }
    
    @Override
    public T next() {
        return readItem(_cursor);
    }
    
    @Override
    public void remove() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }
}
