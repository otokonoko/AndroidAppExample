package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.CloneUtils;
import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IField;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IJoin;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ISelect;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ITable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IWhere;
import com.github.nikolaymakhonin.utils.lists.list.IList;
import com.github.nikolaymakhonin.utils.lists.list.SortedList;

import java.util.List;

public class Select implements ISelect {
    private ITable        _table;
    private boolean       _distinct;
    private IWhere        _where;
    private IList<IJoin>  _joins;
    private IList<IField> _selectFields;
    
    public Select(ITable table) {
        _table = table;
    }
    
    public Select(ISelect sourceSelect) {
        setTable((ITable)sourceSelect.getTable().Clone());
        setDistinct(sourceSelect.getDistinct());
        setWhere(CloneUtils.Clone(sourceSelect.getWhere()));
        CloneUtils.CloneList(sourceSelect.Joins(), Joins());
        CloneUtils.CloneList(sourceSelect.SelectFields(), SelectFields());
    }

    @Override
    public ICloneable Clone() {
        return new Select(this);
    }    
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        sb.append("SELECT ");
        if (_distinct) sb.append("DISTINCT ");
        int length;
        if (_selectFields == null || (length = _selectFields.size()) == 0) {
            sb.append("[").append(_table.getAliasName()).append("].*");
        } else {
            for (int i = 0; i < length; i++) {
                if (i > 0) sb.append(", ");
                _selectFields.get(i).appendSqlString(sb);
            }
        }
        sb.append(" FROM ");
        _table.appendSqlString(sb);
        sb.append('\n');
        if (_joins != null) {
            for (IJoin join : _joins) {
                join.appendSqlString(sb);
            }
        }
        if (_where != null) {
            _where.appendSqlString(sb);
        }
    }
    
    @Override
    public void addBindValues(List<Object> destBindValues) {
        if (_selectFields != null) {
            for (IField selectField : _selectFields) {
                selectField.addBindValues(destBindValues);
            }
        }
        _table.addBindValues(destBindValues);
        if (_joins != null) {
            for (IJoin join : _joins) {
                join.addBindValues(destBindValues);
            }
        }
        if (_where != null) {
            _where.addBindValues(destBindValues);
        }
    }
    
    @Override
    public IList<IJoin> Joins() {
        return _joins != null ? _joins : (_joins = new SortedList<>(false, false));
    }
    
    @Override
    public IWhere getWhere() {
        return _where;
    }
    
    @Override
    public void setWhere(IWhere where) {
        _where = where;
    }
    
    @Override
    public IList<IField> SelectFields() {
        return _selectFields != null ? _selectFields : (_selectFields = new SortedList<>(true, true));
    }
    
    @Override
    public boolean getDistinct() {
        return _distinct;
    }
    
    @Override
    public void setDistinct(boolean distinct) {
        _distinct = distinct;
    }
    
    @Override
    public ITable getTable() {
        return _table;
    }

    @Override
    public void setTable(ITable table) {
        _table = table;
    }
}
