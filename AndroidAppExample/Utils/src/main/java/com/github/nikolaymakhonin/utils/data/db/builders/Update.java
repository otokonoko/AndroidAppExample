package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IFieldValues;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ITable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IUpdate;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IWhere;

import java.util.List;
import java.util.Map.Entry;

public class Update implements IUpdate {
    
    private IWhere       _where;
    private ITable       _table;
    private IFieldValues _fieldValues;
    
    public Update(ITable table) {
        _table = table;
    }
    
    public Update(ITable table, String field, Object value) {
        _table = table;
        _fieldValues.addFieldValues(field, value);
    }
    
    public Update(ITable table, String[] fields, Object[] values) {
        _table = table;
        _fieldValues.addFieldValues(fields, values);
    }
    
    public Update(IUpdate sourceUpdate) {
        _table = (ITable)sourceUpdate.getTable().Clone();
        _where = (IWhere)sourceUpdate.getWhere().Clone();
        _fieldValues = (IFieldValues)sourceUpdate.fieldValues().Clone(); 
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        sb.append("UPDATE ");
        _table.appendSqlString(sb);
        sb.append(" SET ");
        boolean bFirst = true;
        for (Entry<String, Object> entry : _fieldValues) {
            if (!bFirst) sb.append(", ");
            bFirst = false;
            sb.append("[").append(entry.getKey()).append("]").append(" = ?");            
        }
        sb.append('\n');
        if (_where != null) _where.appendSqlString(sb);
    }
    
    @Override
    public void addBindValues(List<Object> destBindValues) {
        _table.addBindValues(destBindValues);
        for (Entry<String, Object> entry : _fieldValues) {
            destBindValues.add(entry.getValue());            
        }
        if (_where != null) _where.addBindValues(destBindValues);
    }

    @Override
    public ICloneable Clone() {
        return new Update(this);
    }

    @Override
    public ITable getTable() {
        return _table;
    }

    @Override
    public void setTable(ITable table) {
        _table = table;
    }

    @Override
    public IWhere getWhere() {
        return _where;
    }

    @Override
    public void setWhere(IWhere where) {
        _where = where;
    }
    
    @Override
    public IFieldValues fieldValues() {
        return _fieldValues != null ? _fieldValues : (_fieldValues = new FieldValues());
    }      
}
