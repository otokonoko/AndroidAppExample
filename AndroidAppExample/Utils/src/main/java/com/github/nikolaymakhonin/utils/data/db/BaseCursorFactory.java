package com.github.nikolaymakhonin.utils.data.db;

import java.util.Arrays;
import java.util.List;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;

public abstract class BaseCursorFactory implements SQLiteDatabase.CursorFactory {
    private final List<Object> _values;

    public List<Object> getValues() { return _values; }
    
    public BaseCursorFactory(Object[] values) {
        this(Arrays.asList(values));
    }
    
    public BaseCursorFactory(List<Object> values) {
        _values = values;
    }
    
    @Override
    public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver masterQuery, String editTable, SQLiteQuery query) {
        bindValues(query, _values);
        return new SQLiteCursor(masterQuery, editTable, query);
    }
    
    public abstract void bindValues(SQLiteQuery query, List<Object> values);
}
