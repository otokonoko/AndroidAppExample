package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

public interface ISelectStatementTable extends ISelectStatement {
    ITable getTable();
    void setTable(ITable table);
}