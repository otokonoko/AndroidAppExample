package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

public enum JoinType {
    Inner, Left, LeftOuter, Cross  
}