package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ICondition;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IWhere;

import java.util.List;

public class Where implements IWhere {
    private ICondition _condition;
    
    /** For many conditions use IConditionList */
    public Where(ICondition condition) {
        _condition = condition;
    }
    
    public Where(IWhere sourceWhere) {
        setCondition((ICondition)getCondition().Clone());
    }
    
    @Override
    public ICloneable Clone() {
        return new Where(this);
    }
    
    @Override
    public ICondition getCondition() {
        return _condition;
    }
    
    @Override
    public void setCondition(ICondition condition) {
        _condition = condition;
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        if (_condition == null) return;
        StringBuilder conditionsStr = new StringBuilder();
        _condition.appendSqlString(conditionsStr);
        if (conditionsStr.length() > 0) {
            sb.append("WHERE ").append(conditionsStr);
            sb.append('\n');
        }
    }
    
    @Override
    public void addBindValues(List<Object> destBindValues) {
        if (_condition == null) return;
        _condition.addBindValues(destBindValues);
    }    
}
