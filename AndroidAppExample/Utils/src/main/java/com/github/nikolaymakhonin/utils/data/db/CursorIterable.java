package com.github.nikolaymakhonin.utils.data.db;

import java.util.Iterator;

import android.database.Cursor;

public abstract class CursorIterable<T> implements Iterable<T> {
    private final Cursor _cursor;
    
    public CursorIterable(Cursor cursor) {
        _cursor = cursor;
    }
    
    public abstract Iterator<T> iterator(Cursor cursor);  
    
    @Override
    public Iterator<T> iterator() {
        return iterator(_cursor);
    }
    
    public void close() {
        _cursor.close();
    }
}
