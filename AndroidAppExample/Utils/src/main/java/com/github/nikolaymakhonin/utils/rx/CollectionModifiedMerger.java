package com.github.nikolaymakhonin.utils.rx;

import com.github.nikolaymakhonin.utils.contracts.patterns.IDisposable;
import com.github.nikolaymakhonin.utils.contracts.patterns.ITreeModified;
import com.github.nikolaymakhonin.utils.lists.list.CollectionChangedEventArgs;
import com.github.nikolaymakhonin.utils.lists.list.ICollectionChangedList;
import com.github.nikolaymakhonin.utils.serialization.ReadItemFunc;
import com.github.nikolaymakhonin.utils.serialization.WriteItemAction;

import java.util.Hashtable;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action0;

public class CollectionModifiedMerger<T extends ITreeModified> extends CollectionSerializer<T> implements IDisposable {

    private final Subscription _collectionChangedSubscription;

    private final Hashtable<Observable, Action0> _itemDetachFuncs = new Hashtable<>();

    public CollectionModifiedMerger(
        ICollectionChangedList<T> collectionChangedList, WriteItemAction<T> writeItemAction, ReadItemFunc<T> readItemFunc
    ) {
        this(collectionChangedList, collectionChangedList.CollectionChanged(), writeItemAction, readItemFunc);
    }

    public CollectionModifiedMerger(
        ICollectionChangedList<T> collectionChangedList, Observable<CollectionChangedEventArgs> collectionChanged,
        WriteItemAction<T> writeItemAction, ReadItemFunc<T> readItemFunc
    ) {
        super(collectionChangedList, writeItemAction, readItemFunc);

        _collectionChangedSubscription = collectionChanged
            .subscribe(e -> {
                switch (e.getChangedType()) {
                    case Added:
                        for (Object item: e.getNewItems()) {
                            bindItem((T)item);
                        }
                        break;
                    case Removed:
                        for (Object item: e.getOldItems()) {
                            unBindItem((T)item);
                        }
                        break;
                    case Setted:
                        for (Object item: e.getOldItems()) {
                            unBindItem((T)item);
                        }
                        for (Object item: e.getNewItems()) {
                            bindItem((T)item);
                        }
                        break;
                }
                Modified().onNext(null);
            });

    }

    public ICollectionChangedList<T> getList() {
        return _collectionChangedList;
    }

    //region Bind Items

    private void bindItem(T item) {
        if (item == null) {
            return;
        }
        Observable observable = item.TreeModified();
        if (_itemDetachFuncs.containsKey(observable)) {
            return;
        }
        Action0 detachFunc = _treeModifiedMerger.attach(observable);
        _itemDetachFuncs.put(observable, detachFunc);
    }

    private void unBindItem(T item) {
        if (item == null) {
            return;
        }
        Observable observable = item.TreeModified();
        Action0 detachFunc = _itemDetachFuncs.get(observable);
        if (detachFunc != null) {
            detachFunc.call();
            _itemDetachFuncs.remove(observable);
        }
    }

    //endregion

    //region Dispose

    private boolean _disposed;

    @Override
    public void dispose() {
        synchronized (_collectionChangedList.Locker()) {
            _disposed = true;
            _collectionChangedSubscription.unsubscribe();
            for (Observable key : _itemDetachFuncs.keySet()) {
                Action0 detachFunc = _itemDetachFuncs.get(key);
                detachFunc.call();
            }
            _itemDetachFuncs.clear();
        }
    }

    //endregion
}
