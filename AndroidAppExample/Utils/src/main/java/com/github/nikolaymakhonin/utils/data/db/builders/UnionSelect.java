package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.logger.Log;
import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ISelect;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IUnionSelect;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.UnionSelectType;

import java.util.List;

public class UnionSelect implements IUnionSelect {
    private static final String LOG_TAG = "UnionSelect";
    private UnionSelectType _unionType;
    private ISelect         _select;
    
    public UnionSelect(UnionSelectType unionType, ISelect select) {
        _unionType = unionType;
        _select = select;
    }
    
    public UnionSelect(IUnionSelect sourceSelect) {
        setSelect((ISelect)sourceSelect.getSelect().Clone());
    }
    
    @Override
    public ICloneable Clone() {
        return new UnionSelect(this);
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        if (_select != null) {
            switch (_unionType) {
                case Union:
                    sb.append("UNION");
                    break;
                case UnionAll:
                    sb.append("UNION ALL");
                    break;
                case Intersect:
                    sb.append("INTERSECT");
                    break;
                case Except:
                    sb.append("EXCEPT");
                    break;
                default:
                    Log.e(LOG_TAG, "Unknown UnionType: " + _unionType);
                    return;
            }
            sb.append('\n');
            _select.appendSqlString(sb);
        }
    }

    @Override
    public void addBindValues(List<Object> destBindValues) {
        if (_select != null) _select.addBindValues(destBindValues);
    }

    @Override
    public UnionSelectType getUnionType() {
        return _unionType;
    }

    @Override
    public void setUnionType(UnionSelectType unionType) {
        _unionType = unionType;
    }

    @Override
    public ISelect getSelect() {
        return _select;
    }

    @Override
    public void setSelect(ISelect select) {
        _select = select;
    }
}
