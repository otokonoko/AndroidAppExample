package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.CloneUtils;
import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IField;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ISelect;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ISelectSorted;
import com.github.nikolaymakhonin.utils.lists.list.IList;

import java.util.List;

public class SelectSorted extends SelectStatementSorted implements ISelectSorted {
    ISelect _innerSelect;
    
    public SelectSorted(ISelect innerSelect) {
        _innerSelect = innerSelect;
    } 
    
    public SelectSorted(ISelectSorted sortedSelect) {
        setInnerSelect(sortedSelect.getInnerSelect());
        setPaging(sortedSelect.getPaging());
        CloneUtils.CloneList(sortedSelect.Orders(), Orders());
    }
    
    @Override
    public ICloneable Clone() {
        return new SelectSorted(this);
    }
    
    protected void appendStatement(StringBuilder sb) {
        _innerSelect.appendSqlString(sb);
    }
    
    @Override
    protected void appendStatementBindValues(List<Object> destBindValues) {
        _innerSelect.addBindValues(destBindValues);
    }
    
    @Override
    public ISelect getInnerSelect() {
        return _innerSelect;
    }
    
    @Override
    public void setInnerSelect(ISelect innerSelect) {
        _innerSelect = innerSelect;
    }

    @Override
    public IList<IField> SelectFields() {
        return _innerSelect.SelectFields();
    }

    @Override
    public boolean getDistinct() {
        return _innerSelect.getDistinct();
    }

    @Override
    public void setDistinct(boolean distinct) {
        _innerSelect.setDistinct(distinct);
    }
}
