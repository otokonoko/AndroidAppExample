package com.github.nikolaymakhonin.utils.data.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.github.nikolaymakhonin.logger.Log;
import com.github.nikolaymakhonin.utils.RefParam;
import com.github.nikolaymakhonin.utils.contracts.patterns.ILocker;
import com.github.nikolaymakhonin.utils.data.db.builders.ConditionEqualsValues;
import com.github.nikolaymakhonin.utils.data.db.builders.TableSimple;
import com.github.nikolaymakhonin.utils.data.db.builders.Update;
import com.github.nikolaymakhonin.utils.data.db.builders.Where;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ICondition;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ISelectFilter;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IUpdate;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IWhere;

import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Func1;

public class SQLiteQueryExecutor implements ILocker {
    private static final String  LOG_TAG = "SQLiteQueryExecutor";

    private final SQLiteOpenHelper _dbHelper;

    private final Object _locker = new Object();

    public SQLiteQueryExecutor(SQLiteOpenHelper dbHelper) {
        _dbHelper = dbHelper;
    }

    @Override
    public Object Locker() {
        return _locker;
    }

    //region Insert / Update / Replace

    private enum InsertMode {
        Ignore, Replace, Update
    }

    public boolean insertOrReplace(String dbName, String[] idFields, Object[] idValues,
        String[] fields, Object[] values)
    {
        return insertRow(InsertMode.Replace, dbName, idFields, idValues, fields, values);
    }

    public boolean insertOrUpdate(String dbName, String[] idFields, Object[] idValues,
        String[] fields, Object[] values)
    {
        return insertRow(InsertMode.Update, dbName, idFields, idValues, fields, values);
    }

    public boolean insertOrIgnore(String dbName, String[] idFields, Object[] idValues,
        String[] fields, Object[] values) {
        return insertRow(InsertMode.Ignore, dbName, idFields, idValues, fields, values);
    }

    private boolean insertRow(InsertMode insertMode, String dbName,
        String[] idFields, Object[] idValues,
        String[] fields, Object[] values)
    {
        synchronized (_locker) {
            RefParam<String> queryInsertStr   = new RefParam<>();
            RefParam<String> queryUpdateStr   = new RefParam<>("");
            List<Object>     insertBindValues = new ArrayList<>();
            List<Object>     updateBindValues = new ArrayList<>();
            SQLiteDatabase   db               = _dbHelper.getWritableDatabase();
            try {
                SQLiteStatement insertStatement = getInsertStatement(insertMode, db, dbName,
                    ArrayUtils.addAll(idFields, fields), ArrayUtils.addAll(idValues, values), queryInsertStr,
                    insertBindValues
                );

                if (insertStatement == null) {
                    return false;
                }

                boolean errorResult = false;
                db.beginTransaction();
                long id = -1;
                try {
                    id = insertStatement.executeInsert();
                    if (id < 0) {
                        if (insertMode != InsertMode.Update) {
                            errorResult = insertMode == InsertMode.Ignore;
                            throw new Exception("Insert error: return id < 0");
                        }
                        SQLiteStatement statement = getUpdateStatement(db, dbName, idFields, idValues, fields, values,
                            queryInsertStr, updateBindValues
                        );

                        try {
                            statement.execute();
                        } finally {
                            statement.close();
                        }
                    }
                    db.setTransactionSuccessful();
                    Log.d(LOG_TAG, "------------ Sql ------------\n" + String
                        .format(queryInsertStr.value.replace("?", "%s"), insertBindValues.toArray()) + ((id >= 0)
                        ? ""
                        : String.format(queryUpdateStr.value.replace("?", "%s"), updateBindValues.toArray())));
                    return true;
                } catch (Exception e) {
                    Log.e(LOG_TAG, "------------ Sql ------------\n" + String
                        .format(queryInsertStr.value.replace("?", "%s"), insertBindValues.toArray()) + ((id >= 0)
                        ? ""
                        : String.format(queryUpdateStr.value.replace("?", "%s"), updateBindValues.toArray())), e);
                    return errorResult;
                } finally {
                    db.endTransaction();
                }
            } finally {
                db.close();
            }
        }
    }

    //endregion

    //region Remove

    public boolean remove(String dbName, String[] idFields, Object[] idValues) {
        synchronized (_locker) {
            RefParam<String> queryStr = new RefParam<>();
            List<Object>     bindValues = new ArrayList<>();
            SQLiteDatabase   db = _dbHelper.getWritableDatabase();
            try {
                SQLiteStatement statement = getRemoveStatement(db, dbName, idFields, idValues, queryStr, bindValues);

                if (statement == null) {
                    return false;
                }

                db.beginTransaction();
                try {
                    statement.execute();
                    db.setTransactionSuccessful();
                    Log.d(
                        "DataBaseEntitySaver",
                        "------------ Sql ------------\n" + String.format(queryStr.value.replace("?", "%s"), bindValues.toArray()));
                } catch (Exception e) {
                    Log.e(
                        "DataBaseEntitySaver",
                        "------------ Sql ------------\n" + String.format(queryStr.value.replace("?", "%s"), bindValues.toArray()), e);
                    return false;
                } finally {
                    statement.close();
                    db.endTransaction();
                }
            } finally {
                db.close();
            }
            return true;
        }
    }

    //endregion

    //region Create Statements

    private SQLiteStatement getRemoveStatement(SQLiteDatabase db, String dbName, String[] idFields, Object[] idValues,
        RefParam<String> queryStr, List<Object> outBindValues)
    {
        StringBuilder query = new StringBuilder();
        SQLiteQueryHelper.appendDeleteFrom(query, dbName);

        // append where
        ICondition condition = new ConditionEqualsValues(null, idFields, idValues);
        IWhere where = new Where(condition);

        outBindValues.clear();
        where.appendSqlString(query);
        where.addBindValues(outBindValues);

        query.append("\n");

        queryStr.value = query.toString();
        try {
            SQLiteStatement statement = db.compileStatement(queryStr.value);

            int count = outBindValues.size();
            for (int i = 0; i < count; i++) {
                SQLiteUtils.BindObject(statement, i + 1, outBindValues.get(i));
            }
            return statement;
        } catch (Exception e) {
            Log.e("DataBaseEntitySaver", "------------ Sql ------------\n" + queryStr.value, e);
            return null;
        }
    }

    private SQLiteStatement getInsertStatement(InsertMode insertMode, SQLiteDatabase db, String dbName,
        String[] fields, Object[] values,
        RefParam<String> queryStr, List<Object> outBindValues)
    {
        StringBuilder query = new StringBuilder();
        switch (insertMode) {
            case Update:
            case Ignore:
                SQLiteQueryHelper.appendInsertOrIgnore(query, dbName);
                break;
            case Replace:
                SQLiteQueryHelper.appendInsertOrReplace(query, dbName);
                break;
            default:
                throw new IllegalStateException("Unknown InsertMode: " + insertMode);
        }

        SQLiteQueryHelper.appendInsertValues(query, fields);
        queryStr.value = query.toString();

        try {
            for (int i = 0; i < values.length; i++) {
                outBindValues.add(values[i]);
            }

            SQLiteStatement statement = db.compileStatement(queryStr.value);

            for (int i = 0; i < values.length; i++) {
                SQLiteUtils.BindObject(statement, i, values[i]);
            }

            return statement;
        } catch (Exception e) {
            Log.e(LOG_TAG, "------------ Sql ------------\n" + queryStr.value, e);
            return null;
        }
    }

    private SQLiteStatement getUpdateStatement(SQLiteDatabase db, String dbName,
        String[] idFields, Object[] idValues,
        String[] fields, Object[] values,
        RefParam<String> queryStr, List<Object> outBindValues)
    {
        StringBuilder query  = new StringBuilder();
        IUpdate       update = new Update(new TableSimple(dbName));

        // append update values
        for (int i = 0; i < fields.length; i++) {
            update.fieldValues().addFieldValues(fields[i], values[i]);
        }

        // append where
        ICondition condition = new ConditionEqualsValues(null, idFields, idValues);
        IWhere     where     = new Where(condition);

        update.setWhere(where);

        outBindValues.clear();
        update.appendSqlString(query);
        update.addBindValues(outBindValues);

        // bind values
        queryStr.value = query.toString();
        try {
            SQLiteStatement statement = db.compileStatement(queryStr.value);

            int count = outBindValues.size();
            for (int i = 0; i < count; i++) {
                SQLiteUtils.BindObject(statement, i + 1, outBindValues.get(i));
            }

            return statement;
        } catch (Exception e) {
            Log.e(LOG_TAG, "------------ Sql ------------\n" + queryStr.value, e);
            return null;
        }
    }

    //endregion

    //region Select

    public <T> boolean select(List<T> destList, ISelectFilter selectFilter, Func1<Cursor, CursorIterator<T>>
        createCursorIterator) {
        StringBuilder query = new StringBuilder();
        List<Object> bindValues = new ArrayList<>();
        selectFilter.appendSqlString(query);
        selectFilter.addBindValues(bindValues);
        String queryStr = query.toString();

        synchronized (_locker) {
            Cursor         cursor;
            SQLiteDatabase db = _dbHelper.getReadableDatabase();
            if (db == null) {
                return false;
            }
            try {
                try {
                    cursor = SQLiteUtils.rawQuery(db, queryStr, selectFilter.getSelect().getInnerSelect().getTable().getAliasName(), bindValues);
                    Log.d("DataBaseModel", "------------ Sql ------------\n" + String.format(queryStr.replace("?", "%s"), bindValues.toArray()));
                } catch (Exception e) {
                    Log.e("DataBaseModel", "------------ Sql ------------\n" + String.format(queryStr.replace("?", "%s"), bindValues.toArray()), e);
                    return false;
                }

                CursorIterable<T> items = new CursorIterableSimple<>(cursor, createCursorIterator);

                try {
                    for (T item : items) {
                        destList.add(item);
                    }
                    return true;
                } finally {
                    items.close();
                }
            } finally {
                db.close();
            }
        }
    }

    //endregion

}
