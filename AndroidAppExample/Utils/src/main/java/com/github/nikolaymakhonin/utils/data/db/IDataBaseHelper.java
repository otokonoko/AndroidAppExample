package com.github.nikolaymakhonin.utils.data.db;

import android.database.sqlite.SQLiteDatabase;

import com.github.nikolaymakhonin.utils.contracts.patterns.IDisposable;
import com.github.nikolaymakhonin.utils.contracts.patterns.ILocker;

public interface IDataBaseHelper extends ILocker, IDisposable {
    SQLiteDatabase getReadableDatabase();
    
    SQLiteDatabase getWritableDatabase();
    
    boolean isDisposed();
}
