package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

public enum UnionSelectType {
    Union, UnionAll, Except, Intersect  
}

