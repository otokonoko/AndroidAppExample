package com.github.nikolaymakhonin.utils.contracts.patterns.mvp;

import rx.Observable;

public interface IView {
    boolean isAttached();

    Observable<Boolean> attachedObservable();
}
