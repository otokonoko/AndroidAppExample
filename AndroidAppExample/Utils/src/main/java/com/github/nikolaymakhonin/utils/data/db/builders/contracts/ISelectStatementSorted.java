package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

import com.github.nikolaymakhonin.utils.lists.list.IList;

public interface ISelectStatementSorted extends ISelectStatement {
    PagingParams getPaging();
    void setPaging(PagingParams pagingParams);    
    IList<IOrderParams> Orders();
}
