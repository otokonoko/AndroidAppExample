package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

public interface IJoin extends IStatement {
    ITable getTable();
    void setTable(ITable table);
    JoinType getJoinType();
    void setJoinType(JoinType joinType);
    ICondition getCondition();
    /** For many conditions use IConditionList */
    void setCondition(ICondition condition);
}