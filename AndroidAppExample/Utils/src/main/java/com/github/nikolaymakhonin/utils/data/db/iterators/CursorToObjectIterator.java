package com.github.nikolaymakhonin.utils.data.db.iterators;

import android.annotation.TargetApi;
import android.database.Cursor;
import android.os.Build;

import com.github.nikolaymakhonin.utils.ClassUtils;
import com.github.nikolaymakhonin.utils.data.db.CursorIterator;
import com.github.nikolaymakhonin.utils.data.db.SQLiteUtils;

import rx.functions.Func0;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CursorToObjectIterator<T> extends CursorIterator<T> {

    private final Func0<T> _constructor;
    private final Class<T> _type;

    public CursorToObjectIterator(Cursor cursor, Class<T> type, Func0<T> constructor) {
        super(cursor);
        _type = type;
        if (constructor == null) {
            constructor = () -> ClassUtils.newInstance(_type);
        }
        _constructor = constructor;
    }

    private int _dataIndex;

    @Override
    protected void initFieldIndexes(Cursor cursor) {
        _dataIndex = cursor.getColumnIndex("Data");
    }

    @Override
    protected T readItem(Cursor cursor) {
        T object = SQLiteUtils.GetObject(cursor, _dataIndex, _type, _constructor);
        return object;
    }
}
