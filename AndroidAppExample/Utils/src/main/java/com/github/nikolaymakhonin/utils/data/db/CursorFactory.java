package com.github.nikolaymakhonin.utils.data.db;

import java.util.List;

import android.database.sqlite.SQLiteQuery;

public class CursorFactory extends BaseCursorFactory {
    public CursorFactory(List<Object> values) {
        super(values);
    }

    @Override
    public void bindValues(SQLiteQuery query, List<Object> values) {
        SQLiteUtils.BindObjects(query, values);
    }   
}