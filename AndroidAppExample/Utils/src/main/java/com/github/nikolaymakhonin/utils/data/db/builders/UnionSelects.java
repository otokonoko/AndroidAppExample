package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.CloneUtils;
import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IField;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IJoin;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ISelect;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ITable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IUnionSelect;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IUnionSelects;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IWhere;
import com.github.nikolaymakhonin.utils.lists.list.IList;
import com.github.nikolaymakhonin.utils.lists.list.SortedList;

import java.util.List;

public class UnionSelects implements IUnionSelects {
    private ISelect             _select;
    private IList<IUnionSelect> _unionSelects;
    
    public UnionSelects(ISelect select) {
        _select = select;
    }
    
    public UnionSelects(IUnionSelects unionSelects) {
        setInnerSelect(unionSelects.getInnerSelect());
        CloneUtils.CloneList(unionSelects.getUnionSelects(), getUnionSelects());
    }
    
    @Override
    public ICloneable Clone() {
        return new UnionSelects(this);
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        _select.appendSqlString(sb);                            
        for (IUnionSelect unionSelect : _unionSelects) {
            unionSelect.appendSqlString(sb);
        }
    }
    
    @Override
    public void addBindValues(List<Object> destBindValues) {
        _select.addBindValues(destBindValues);            
        if (_unionSelects != null) {
            for (IUnionSelect unionSelect : _unionSelects) {
                unionSelect.addBindValues(destBindValues);
            }
        }
    }
    
    @Override
    public ISelect getInnerSelect() {
        return _select;
    }
    
    @Override
    public void setInnerSelect(ISelect select) {
        _select = select;
    }
    
    @Override
    public IList<IUnionSelect> getUnionSelects() {
        return _unionSelects != null ? _unionSelects : (_unionSelects = new SortedList<>(false, false));
    }

    @Override
    public IList<IJoin> Joins() {
        return _select.Joins();
    }

    @Override
    public IWhere getWhere() {
        return _select.getWhere();
    }

    @Override
    public void setWhere(IWhere where) {
        _select.setWhere(where);
    }

    @Override
    public ITable getTable() {
        return _select.getTable();
    }

    @Override
    public void setTable(ITable table) {
        _select.setTable(table);
    }

    @Override
    public IList<IField> SelectFields() {
        return _select.SelectFields();
    }

    @Override
    public boolean getDistinct() {
        return _select.getDistinct();
    }

    @Override
    public void setDistinct(boolean distinct) {
        _select.setDistinct(distinct);
    }
}
