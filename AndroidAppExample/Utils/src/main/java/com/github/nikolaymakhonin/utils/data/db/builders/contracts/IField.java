package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

public interface IField extends IStatement, Comparable<IField> {
    String getAliasName();
}
