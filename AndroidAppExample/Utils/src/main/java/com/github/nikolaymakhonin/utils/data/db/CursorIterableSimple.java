package com.github.nikolaymakhonin.utils.data.db;

import java.util.Iterator;

import android.database.Cursor;

import rx.functions.Func1;

public class CursorIterableSimple<T> extends CursorIterable<T> {
    private final Func1<Cursor, CursorIterator<T>> _createCursorIterator;
    
    public CursorIterableSimple(Cursor cursor, Func1<Cursor, CursorIterator<T>> createCursorIterator) {
        super(cursor);
        _createCursorIterator = createCursorIterator;
    }
    
    @Override
    public Iterator<T> iterator(Cursor cursor) {
        return _createCursorIterator.call(cursor);
    }
}