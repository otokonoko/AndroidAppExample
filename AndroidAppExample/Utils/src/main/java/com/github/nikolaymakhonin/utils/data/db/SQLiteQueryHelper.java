package com.github.nikolaymakhonin.utils.data.db;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.github.nikolaymakhonin.logger.Log;
import com.github.nikolaymakhonin.utils.RefParam;
import com.github.nikolaymakhonin.utils.data.db.builders.ConditionEqualsValues;
import com.github.nikolaymakhonin.utils.data.db.builders.TableSimple;
import com.github.nikolaymakhonin.utils.data.db.builders.Update;
import com.github.nikolaymakhonin.utils.data.db.builders.Where;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ICondition;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IUpdate;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IWhere;

import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SQLiteQueryHelper {

    private static final String  LOG_TAG              = "SQLiteQueryHelper";
    private static final Pattern splitBatchSqlPattern = Pattern.compile("(.*?;)[ \\t]*[\\r\\n]+", Pattern.DOTALL);

    public static List<String> splitBatchSql(final String sql) {
        final ArrayList<String> statements = new ArrayList<>();
        final Matcher           m          = splitBatchSqlPattern.matcher(sql);
        while (m.find()) {
            statements.add(m.group(1));
        }
        return statements;
    }

    public static void appendInsertOrReplace(StringBuilder query, String tableName) {
        query.append("INSERT OR REPLACE INTO [").append(tableName).append("]\n");
    }

    public static void appendInsertOrIgnore(StringBuilder query, String tableName) {
        query.append("INSERT OR IGNORE INTO [").append(tableName).append("]\n");
    }

    public static void appendInsertValues(StringBuilder query, String... fieldNames) {
        appendInsertValues(query, 1, fieldNames);
    }

    public static void appendInsertValues(StringBuilder query, int rowsCount, String... fieldNames) {
        int length = fieldNames.length;
        query.append("(");
        for (int i = 0; i < length; i++) {
            if (i > 0)
                query.append(", ");
            query.append("[").append(fieldNames[i]).append("]");
        }
        query.append(")\nVALUES ");
        for (int nRow = 0; nRow < rowsCount; nRow++) {
            query.append(nRow > 0 ? ",(" : "(");
            for (int i = 0; i < length; i++) {
                if (i > 0)
                    query.append(", ");
                query.append("?");
            }
            query.append(")\n");
        }
    }

    public static void appendDeleteFrom(StringBuilder query, String tableName) {
        query.append("DELETE FROM [").append(tableName).append("]\n");
    }
}
