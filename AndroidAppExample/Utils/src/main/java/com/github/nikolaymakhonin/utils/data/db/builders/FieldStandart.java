package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.CompareUtils;
import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IField;
import com.github.nikolaymakhonin.utils.strings.StringUtilsExt;

import java.util.List;

public class FieldStandart implements IField {
    private final String _field;
    private final String _tableName;
    private final String _aliasName;
    
    public FieldStandart(String tableName, String field) {
        _field = field;
        _tableName = tableName;
        _aliasName = null;
    }
    
    public FieldStandart(String tableName, String field, String aliasName) {
        _field = field;
        _tableName = tableName;
        _aliasName = aliasName;
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        if (!StringUtilsExt.isNullOrEmpty(_tableName)) {
            sb.append("[").append(_tableName).append("].");
        }
        sb.append("[").append(_field).append("]");
        if (!StringUtilsExt.isNullOrEmpty(_aliasName)) {
            sb.append(" AS [").append(_aliasName).append("]");
        }
    }
    
    @Override
    public void addBindValues(List<Object> destBindValues) {
    }

    @Override
    public ICloneable Clone() {
        return new FieldStandart(_tableName, _field, _aliasName);
    }

    @Override
    public String getAliasName() {
        return (!StringUtilsExt.isNullOrEmpty(_aliasName)) ? _aliasName : _field;
    }   

    @Override
    public int compareTo(IField another) {
        return CompareUtils.Compare(getAliasName(), another.getAliasName(), true);
    }
    
    @Override
    public int hashCode() {
        return  getAliasName().toLowerCase().hashCode();
    }
    
    @Override
    public boolean equals(Object o) {
        IField another = (IField)o;
        return getAliasName().equals(another.getAliasName());
    }
}
