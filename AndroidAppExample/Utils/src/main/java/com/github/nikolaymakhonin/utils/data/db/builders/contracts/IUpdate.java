package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

public interface IUpdate extends IStatement {
    ITable getTable();
    void setTable(ITable table);
    IFieldValues fieldValues();
    IWhere getWhere();
    void setWhere(IWhere where);
}