package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

public interface IUnionSelect extends IStatement {
    UnionSelectType getUnionType();
    void setUnionType(UnionSelectType unionType);
    ISelect getSelect();
    void setSelect(ISelect select);
}