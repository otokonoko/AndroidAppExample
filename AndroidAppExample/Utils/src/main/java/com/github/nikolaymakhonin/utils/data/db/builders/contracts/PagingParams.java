package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

public class PagingParams {
    public final int Skip;
    public final int Count;
    public PagingParams(int skip, int count) {
        Skip = skip;
        Count = count;
    }
}