package com.github.nikolaymakhonin.utils.contracts.patterns.mvp;

import com.github.nikolaymakhonin.utils.CompareUtils;
import com.github.nikolaymakhonin.utils.contracts.patterns.BaseTreeModified;
import com.github.nikolaymakhonin.utils.threads.ThreadUtils;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public abstract class ViewPresenter<TView extends IView>
    extends BaseTreeModified implements IViewPresenter<TView>
{
    //region Bind View

    protected void updateView() {

    }

    private Subscription _viewAttachedSubscription;

    protected void bindViewAttached() {
        boolean    isMainThread           = ThreadUtils.isMainThread();
        Observable viewAttachedObservable = getView().attachedObservable();
        if (!isMainThread) {
            viewAttachedObservable = Observable
                .concatEager(
                    Observable.just(getView().isAttached())
                              .observeOn(AndroidSchedulers.mainThread()),
                    viewAttachedObservable);
        }

        //noinspection RedundantCast
        _viewAttachedSubscription = viewAttachedObservable
            .map((Func1<Boolean, Boolean>) attached -> {
                updateView();
                return attached;
            })
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe((Action1<Boolean>) attached -> {
                if (attached && _viewAttachedSubscription != null) {
                    bindView();
                } else {
                    unBindView();
                }
            });

        if (isMainThread) {
            updateView();
        }
    }

    protected void unBindViewAttached() {
        if (_viewAttachedSubscription != null) {
            _viewAttachedSubscription.unsubscribe();
            _viewAttachedSubscription = null;
        }
        unBindView();
    }

    protected void bindView() {

    }

    protected void unBindView() {

    }

    //endregion

    //region Properties

    //region View

    private TView _view;

    @Override
    public TView getView() {
        return _view;
    }

    @Override
    public void setView(TView value) {
        if (CompareUtils.EqualsObjects(_view, value)) {
            return;
        }
        synchronized (_propertySetLocker) {
            unBindViewAttached();
            _view = value;
            if (isViewBind(false)) {
                bindViewAttached();
            }
        }
        Modified().onNext(null);
    }

    //endregion

    //region AllowBindView

    private boolean _allowBindView = true;

    @Override
    public boolean isAllowBindView() {
        return _allowBindView;
    }

    @Override
    public void setAllowBindView(boolean value) {
        if (CompareUtils.Equals(_allowBindView, value)) {
            return;
        }
        synchronized (_propertySetLocker) {
            unBindViewAttached();
            _allowBindView = value;
            if (isViewBind(false)) {
                bindViewAttached();
            }
        }
        Modified().onNext(null);
    }

    //endregion

    //endregion

    //region State properties

    @Override
    public boolean isViewBind(boolean checkAttached) {
        return isViewBind(checkAttached, _view);
    }

    protected boolean isViewBind(boolean checkAttached, TView view) {
        return _allowBindView && (view != null) && (!checkAttached || view.isAttached());
    }

    //endregion

    @Override
    public void dispose() {
        setView(null);
    }
}
