package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IField;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IOrderParams;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ISelectStatementSorted;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.PagingParams;
import com.github.nikolaymakhonin.utils.lists.list.IList;
import com.github.nikolaymakhonin.utils.lists.list.SortedList;

import java.util.List;

public abstract class SelectStatementSorted implements ISelectStatementSorted {
    private PagingParams        _paging;
    private IList<IOrderParams> _sortOrders;

    public SelectStatementSorted() {
    }

    @Override
    public abstract ICloneable Clone();

    protected abstract void appendStatement(StringBuilder sb);
    protected abstract void appendStatementBindValues(List<Object> destBindValues);
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        appendStatement(sb);
        int length;
        if (_sortOrders != null && (length = _sortOrders.size()) > 0) {
            sb.append("ORDER BY ");
            for (int i = 0; i < length; i++) {
                if (i > 0) sb.append(", ");
                _sortOrders.get(i).appendSqlString(sb);
            }
            sb.append('\n');
        }
        if (_paging != null) {
            sb.append("LIMIT ").append(_paging.Count)
            .append(" OFFSET ").append(_paging.Skip).append("\n");
        }
    }

    @Override
    public void addBindValues(List<Object> destBindValues) {
        appendStatementBindValues(destBindValues);
        if (_sortOrders != null) {
            for (IOrderParams sortOrders : _sortOrders) {
                sortOrders.addBindValues(destBindValues);
            }
        }
    }

    @Override
    public IList<IOrderParams> Orders() {
        return _sortOrders != null ? _sortOrders : (_sortOrders = new SortedList<>(false, false));
    }

    @Override
    public PagingParams getPaging() {
        return _paging;
    }

    @Override
    public void setPaging(PagingParams pagingParams) {
        _paging = pagingParams;
    }

    @Override
    public abstract IList<IField> SelectFields();

    @Override
    public abstract boolean getDistinct();

    @Override
    public abstract void setDistinct(boolean distinct);
}
