package com.github.nikolaymakhonin.utils.lists.map;

import com.github.nikolaymakhonin.utils.CompareUtils;

import rx.functions.Func1;

public class SortDictIgnoreCase<TValue> extends SortDict<String, TValue> {
    public SortDictIgnoreCase() {
        setComparator(CompareUtils.StringComparatorIgnoreCase);
        setGetKeyHashCode(hashCoder);
    }
    
    private static Func1<String, Integer> hashCoder = key -> key == null ? 0 : key.toLowerCase().hashCode();
}
