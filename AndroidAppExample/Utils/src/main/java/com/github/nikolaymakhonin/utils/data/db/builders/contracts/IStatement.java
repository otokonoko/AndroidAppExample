package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;

import java.util.List;

public interface IStatement extends ICloneable {
    void appendSqlString(StringBuilder sb);
    /** Добавляемые значения могут быть только следующих типов: null, integer types, float types, String, byte[]. DateTime, TimeSpan */
    void addBindValues(List<Object> destBindValues);
}
