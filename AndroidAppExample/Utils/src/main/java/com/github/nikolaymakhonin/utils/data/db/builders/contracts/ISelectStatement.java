package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

import com.github.nikolaymakhonin.utils.lists.list.IList;

public interface ISelectStatement extends IStatement {
    /** null or Empty == all fields */
    IList<IField> SelectFields();
    boolean getDistinct();
    void setDistinct(boolean distinct);
}
