package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ICondition;
import com.github.nikolaymakhonin.utils.strings.StringUtilsExt;

import java.util.List;

public class ConditionInList<T> implements ICondition {
    protected final Iterable<? extends T> _list;
    protected final String _fieldName;
    protected final String _table;
    protected final boolean _notInList;
    
    public ConditionInList(String table, String fieldName, Iterable<? extends T> list) {
        this(table, fieldName, list, false);
    }
      
    public ConditionInList(String table, String fieldName, Iterable<? extends T> list, boolean notInList) {
        _list = list;
        _fieldName = fieldName;
        _notInList = notInList;
        _table = table;
    }
      
    @Override
    public ICondition Clone() {
        return new ConditionInList<>(_table, _fieldName, _list, _notInList);
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        if (_list == null) return;

        boolean bContainsNull = false;
        boolean bContainsNotNull = false;
        for (T item : _list) {
            if (item == null) {
                bContainsNull = true;
                if (bContainsNotNull) break;
            } else {
                bContainsNotNull = true;
                if (bContainsNull) break;
            }
        }
        
        if (!bContainsNull && !bContainsNotNull) return;
        
        if (bContainsNull) {
            if (bContainsNotNull) sb.append("(");
            if (!StringUtilsExt.isNullOrEmpty(_table)) sb.append("[").append(_table).append("].");
            sb.append("[").append(_fieldName).append("]");
            sb.append(_notInList ? " IS NOT NULL " : " IS NULL ");
            if (bContainsNotNull) sb.append(_notInList ? "AND " : "OR ");
        }
        
        boolean bFirst = true;
        if (bContainsNotNull) {
            for (T item : _list) {
                if (item == null) continue;
                if (bFirst) {
                    if (!StringUtilsExt.isNullOrEmpty(_table)) sb.append("[").append(_table).append("].");
                    sb.append("[").append(_fieldName).append("]");
                    sb.append(_notInList ? " NOT IN (" : " IN (");
                    bFirst = false;
                } else {
                    sb.append(",");
                }
                sb.append("?");
            }
        }
        
        if (!bFirst) sb.append(")");
        
        if (bContainsNull && bContainsNotNull) sb.append(")");
        sb.append('\n');
    }        

    @Override
    public void addBindValues(List<Object> destBindValues) {
        if (_list != null) {
            for (T value : _list) {
                if (value != null) destBindValues.add(value);
            }
        }
    }
}

