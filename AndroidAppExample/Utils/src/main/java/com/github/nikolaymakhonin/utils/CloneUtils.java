package com.github.nikolaymakhonin.utils;

import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;

import java.lang.reflect.Array;
import java.util.List;

public class CloneUtils {
    public static <T extends ICloneable> void CloneList(final List<T> sourceList, final List<T> destList) {
        if (sourceList == null) {
            return;
        }
        for (final T item : sourceList) {
            destList.add(Clone(item));
        }
    } 
    
    public static <T extends ICloneable> T[] CloneArray(final T[] sourceArray) {
        if (sourceArray == null) {
            return null;
        }
        final int length = sourceArray.length;
        final T[] destArray = (T[]) Array.newInstance(sourceArray.getClass().getComponentType(), length);
        for (int i = 0; i < length; i++) {
            sourceArray[i] = Clone(destArray[i]);
        }
        return destArray;
    } 
    
    public static <T extends ICloneable> T Clone(final T object) {
        if (object == null) {
            return null;
        }
        return (T)object.Clone();
    }
}
