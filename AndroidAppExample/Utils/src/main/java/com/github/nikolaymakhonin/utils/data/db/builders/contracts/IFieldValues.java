package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.lists.map.IDictionary;

public interface IFieldValues extends IDictionary<String, Object>, ICloneable {
    
    void addFieldValues(String field, Object value);
    
    void addFieldValues(String[] fields, Object[] values);
    
}
