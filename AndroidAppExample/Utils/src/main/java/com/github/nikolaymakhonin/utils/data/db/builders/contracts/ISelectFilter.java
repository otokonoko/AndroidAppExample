package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

import com.github.nikolaymakhonin.utils.lists.list.IList;

public interface ISelectFilter extends ISelectStatementSorted {
    ISelectSorted getSelect();
    IUnionSelects getUnionSelects();
    void setUnionSelects(IUnionSelects unionSelects);
    IList<String> UnionIdFieldNames();
    IList<String> IdFieldNames();
}