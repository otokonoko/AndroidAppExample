package com.github.nikolaymakhonin.utils.data.db.builders.contracts;
                            
public interface IWhere extends IStatement {
    ICondition getCondition();
    /** For many conditions use IConditionList */
    void setCondition(ICondition condition);
}
