package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ICondition;
import com.github.nikolaymakhonin.utils.strings.StringUtilsExt;

import java.util.List;

public class ConditionEqualsFields implements ICondition {
    private final String _table1;
    private final String[] _fields1;
    private final String _table2;
    private final String[] _fields2;
    
    public ConditionEqualsFields(String table1, String[] fields1, String table2, String[] fields2) {
        _table1 = table1;
        _fields1 = fields1;
        _table2 = table2;
        _fields2 = fields2;
        if (_fields1.length != _fields2.length) {
            throw new IllegalArgumentException("fields1.length != fields2.length");
        }
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        int length = _fields1.length;
        for (int i = 0; i < length; i++) {
            if (i > 0) sb.append(" AND ");
            sb.append("((");
            if (!StringUtilsExt.isNullOrEmpty(_table1)) sb.append("[").append(_table1).append("].");
            sb.append("[").append(_fields1[i]).append("] IS NULL AND ");
            if (!StringUtilsExt.isNullOrEmpty(_table2)) sb.append("[").append(_table2).append("].");
            sb.append("[").append(_fields2[i]).append("] IS NULL");
            sb.append(") OR (");
            if (!StringUtilsExt.isNullOrEmpty(_table1)) sb.append("[").append(_table1).append("].");
            sb.append("[").append(_fields1[i]).append("] = ");
            if (!StringUtilsExt.isNullOrEmpty(_table2)) sb.append("[").append(_table2).append("].");
            sb.append("[").append(_fields2[i]).append("]");
            sb.append("))");
        }
    }
    
    @Override
    public void addBindValues(List<Object> destBindValues) {
    }

    @Override
    public ICloneable Clone() {
        return new ConditionEqualsFields(_table1, _fields1.clone(), _table2, _fields2.clone());
    }   
}
