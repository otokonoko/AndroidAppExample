package com.github.nikolaymakhonin.utils.contracts.patterns.mvp;

import com.github.nikolaymakhonin.utils.CompareUtils;
import com.github.nikolaymakhonin.utils.contracts.patterns.ITreeModified;
import com.github.nikolaymakhonin.utils.rx.RxOperators;
import com.github.nikolaymakhonin.utils.threads.ThreadUtils;

import org.javatuples.Pair;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * One view and one viewModel
 */
public abstract class ViewModelPresenter<TView extends IViewModelView, TViewModel extends ITreeModified>
    extends ViewPresenter<TView> implements IViewModelPresenter<TView,TViewModel>
{

    //region Data stream transformation

    protected Observable<Pair<TView, TViewModel>> preUpdateView(Observable<Pair<TView, TViewModel>> dataStream) {
        return dataStream;
    }

    //endregion

    //region Update view subscription

    private Observable<Pair<TView, TViewModel>> _doUpdateViewObservable;

    private boolean _doUpdateViewObservableInitialized;

    private void initDoUpdateViewObservable() {
        if (_doUpdateViewObservableInitialized) {
            return;
        }

        _doUpdateViewObservableInitialized = true;

        //noinspection RedundantCast
        Observable<Pair<TView, TViewModel>> doUpdateViewObservable = TreeModified()
            .filter(o -> isViewBind(true))
            .lift(RxOperators.deferred(250, TimeUnit.MILLISECONDS))
            .filter(o -> isViewBind(true));
        doUpdateViewObservable = preUpdateView(doUpdateViewObservable);
        doUpdateViewObservable.observeOn(AndroidSchedulers.mainThread());

        _doUpdateViewObservable = doUpdateViewObservable;
    }

    private Subscription _doUpdateViewSubscription;

    @Override
    protected void bindView() {
        super.bindView();
        initDoUpdateViewObservable();
        //noinspection RedundantCast
        _doUpdateViewSubscription = _doUpdateViewObservable
            .subscribe(o -> updateView());
    }

    @Override
    protected void unBindView() {
        super.unBindView();
        if (_doUpdateViewSubscription != null) {
            _doUpdateViewSubscription.unsubscribe();
            _doUpdateViewSubscription = null;
        }
    }

    @Override
    protected void updateView() {
        TView view = getView();
        TViewModel viewModel = _viewModel;
        if (!isViewBind(true, view, viewModel)) {
            return;
        }
        updateView(view, viewModel);
    }

    protected void updateView(TView view, TViewModel viewModel) {
        view.updateView(viewModel);
    }

    //endregion

    //region Properties

    //region ViewModel

    private TViewModel _viewModel;

    private Action0 _viewModelUnBindFunc;

    @Override
    public TViewModel getViewModel() {
        return _viewModel;
    }

    @Override
    public void setViewModel(TViewModel value) {
        if (CompareUtils.EqualsObjects(_viewModel, value)) {
            return;
        }
        synchronized (_propertySetLocker) {
//            boolean mustReBindView = _viewModel == null || value == null;
//            if (mustReBindView) {
//                unBindView();
//            }
            if (_viewModelUnBindFunc != null) {
                _viewModelUnBindFunc.call();
                _viewModelUnBindFunc = null;
            }
            _viewModel = value;
            if (_viewModel != null) {
                _viewModelUnBindFunc = _treeModifiedMerger.attach(_viewModel.TreeModified());
            }
            if (isViewBind(false))
            {
                bindViewAttached();
            }
            else
            {
                unBindViewAttached();
            }
        }
        Modified().onNext(null);
    }

    //endregion

    //endregion

    //region State properties

    @Override
    public boolean isViewBind(boolean checkAttached) {
        return isViewBind(checkAttached, getView(), _viewModel);
    }

    protected boolean isViewBind(boolean checkAttached, TView view, TViewModel viewModel) {
        return viewModel != null && super.isViewBind(checkAttached);
    }

    //endregion

    @Override
    public void dispose() {
        setView(null);
    }
}
