package com.github.nikolaymakhonin.utils.rx;

import com.github.nikolaymakhonin.utils.RefParam;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action0;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;

public class DynamicObservablesMerger<T>  {

    private final Object _locker = new Object();

    @SafeVarargs
    public DynamicObservablesMerger(Observable<T>... observables) {
        for (Observable<T> observable: observables) {
            attach(observable);
        }
    }

    /**
     * Return detach func
     */
    public Action0 attach(Observable<T> observable) {
        synchronized (_locker) {
            final RefParam<Action0> detachFunc = new RefParam<>();

            final Subscription subscription = observable.subscribe(
                o -> _emitter.onNext(o),
                o -> _emitter.onError(o),
                () -> {
                    synchronized (_locker) {
                        if (detachFunc.value != null) {
                            detachFunc.value.call();
                            detachFunc.value = null;
                        }
                    }
                }
            );

            detachFunc.value = () -> subscription.unsubscribe();

            return detachFunc.value;
        }
    }

    private final Subject<T, T> _emitter = PublishSubject.create();

    public Observable<T> observable() {
        return _emitter;
    }
}
