package com.github.nikolaymakhonin.utils.data.db.iterators;

import android.annotation.TargetApi;
import android.database.Cursor;
import android.os.Build;

import com.github.nikolaymakhonin.utils.data.db.CursorIterator;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CursorToStringIterator extends CursorIterator<String> {
    public CursorToStringIterator(Cursor cursor) {
        super(cursor);
    }

    private String[] _fieldNames;

    @Override
    protected void initFieldIndexes(Cursor cursor) {
        _fieldNames = cursor.getColumnNames();
    }

    @Override
    protected String readItem(Cursor cursor) {
        StringBuilder line = new StringBuilder();
        int length = _fieldNames.length;
        for (int i = 0; i < length; i++) {
            if (i > 0) line.append('\t');
            int fieldType = cursor.getType(i);
            switch (cursor.getType(i)) {
                case Cursor.FIELD_TYPE_BLOB:
                    line.append("<blob>");
                    break;
                case Cursor.FIELD_TYPE_FLOAT:
                    line.append(cursor.getDouble(i));
                    break;
                case Cursor.FIELD_TYPE_INTEGER:
                    line.append(cursor.getLong(i));
                    break;
                case Cursor.FIELD_TYPE_NULL:
                    line.append("<null>");
                    break;
                case Cursor.FIELD_TYPE_STRING:
                    line.append(cursor.getString(i));
                    break;
                default:
                    line.append("<unknown(").append(fieldType).append(")>");
                    break;
            }
        }
        return line.toString();
    }
}
