package com.github.nikolaymakhonin.utils.rx;

import com.github.nikolaymakhonin.utils.contracts.patterns.ITreeModified;
import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.BaseViewModel;
import com.github.nikolaymakhonin.utils.lists.list.ICollectionChangedList;
import com.github.nikolaymakhonin.utils.serialization.BinaryReader;
import com.github.nikolaymakhonin.utils.serialization.BinaryWriter;
import com.github.nikolaymakhonin.utils.serialization.ReadItemFunc;
import com.github.nikolaymakhonin.utils.serialization.WriteItemAction;

public class CollectionSerializer<T extends ITreeModified> extends BaseViewModel {
    protected final ICollectionChangedList<T> _collectionChangedList;
    protected final WriteItemAction<T>        _writeItemAction;
    protected final ReadItemFunc<T>           _readItemFunc;

    public CollectionSerializer(
        ICollectionChangedList<T> collectionChangedList, WriteItemAction<T> writeItemAction, ReadItemFunc<T> readItemFunc
    )
    {
        _collectionChangedList = collectionChangedList;
        _writeItemAction = writeItemAction;
        _readItemFunc = readItemFunc;
    }

    //region Serialization

    private static final int _currentVersion = 0;

    @Override
    public void Serialize(BinaryWriter writer) throws Exception {
        writer.write(_currentVersion);

        synchronized (_collectionChangedList.Locker()) {
            writer.WriteCollection(_collectionChangedList, _writeItemAction);
        }
    }

    @Override
    public Object DeSerialize(BinaryReader reader) throws Exception {
        //noinspection UnusedAssignment
        int version = reader.readInt();

        synchronized (_collectionChangedList.Locker()) {
            _collectionChangedList.clear();
            reader.ReadCollection(_collectionChangedList, _readItemFunc);
        }
        return this;
    }

    //endregion
}
