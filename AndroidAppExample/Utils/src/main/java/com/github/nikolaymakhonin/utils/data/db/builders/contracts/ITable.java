package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

public interface ITable extends IStatement {
    String getAliasName();
}
