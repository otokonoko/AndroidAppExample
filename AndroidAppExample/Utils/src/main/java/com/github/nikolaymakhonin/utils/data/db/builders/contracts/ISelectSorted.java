package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

public interface ISelectSorted extends ISelectStatementSorted, ISelectStatementSelect {
}
