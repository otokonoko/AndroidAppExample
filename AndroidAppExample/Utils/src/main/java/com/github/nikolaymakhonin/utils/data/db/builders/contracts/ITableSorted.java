package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

public interface ITableSorted extends ISelectStatementSorted, ISelectStatementTable {
}