package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.CloneUtils;
import com.github.nikolaymakhonin.utils.CompareUtils;
import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ICondition;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IField;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IJoin;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IOrderParams;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ISelectFilter;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ISelectSorted;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IUnionSelects;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.JoinType;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.PagingParams;
import com.github.nikolaymakhonin.utils.lists.list.IList;
import com.github.nikolaymakhonin.utils.lists.list.SortedList;

import java.util.List;

public class SelectFilter implements ISelectFilter {
    private final ISelectSorted _selectSorted;
    private       IUnionSelects _unionSelects;
    private       IJoin         _unionJoin;
    private final String _unionTableAliasName = "unionSelect";
    private IList<String> _unionIdFieldNames;
    private IList<String> _idFieldNames;
    
    public SelectFilter(ISelectSorted selectSorted) {
        _selectSorted = selectSorted;
    }
    
    public SelectFilter(ISelectFilter selectFilter) {
        _selectSorted = (ISelectSorted)selectFilter.getSelect().Clone();
        setUnionSelects(CloneUtils.Clone(selectFilter.getUnionSelects()));
    }   
    
    @Override
    public ICloneable Clone() {
        return new SelectFilter(this);
    }

    @Override
    public ISelectSorted getSelect() {
        return _selectSorted;
    }

    @Override
    public IUnionSelects getUnionSelects() {
        return _unionSelects;
    }

    @Override
    public void setUnionSelects(IUnionSelects unionSelects) {
        _unionSelects = unionSelects;
    }

    @Override
    public PagingParams getPaging() {
        return _selectSorted.getPaging();
    }

    @Override
    public void setPaging(PagingParams pagingParams) {
        _selectSorted.setPaging(pagingParams);
    }

    @Override
    public IList<IOrderParams> Orders() {
        return _selectSorted.Orders();
    }

    @Override
    public IList<IField> SelectFields() {
        return _selectSorted.SelectFields();
    }

    @Override
    public boolean getDistinct() {
        return _selectSorted.getDistinct();
    }

    @Override
    public void setDistinct(boolean distinct) {
        _selectSorted.setDistinct(distinct);
    }

    public IList<String> UnionIdFieldNames() {
        return _unionIdFieldNames != null ? _unionIdFieldNames : (_unionIdFieldNames = new SortedList<>(true, true, CompareUtils.StringComparatorIgnoreCase));
    }

    public IList<String> IdFieldNames() {
        return _idFieldNames != null ? _idFieldNames : (_idFieldNames = new SortedList<>(true, true, CompareUtils.StringComparatorIgnoreCase));
    }

    private void replaceUnionJoin() {
        if (_unionJoin != null) {
            _selectSorted.getInnerSelect().Joins().Remove(_unionJoin);
        }
        if (_unionSelects != null) {
            String[]   unionIdFieldNames = UnionIdFieldNames().toArray(String[].class);
            ICondition joinCondition     = new ConditionEqualsFields(_unionTableAliasName, unionIdFieldNames, _selectSorted.getInnerSelect().getTable().getAliasName(), unionIdFieldNames);
            _unionJoin = new Join(JoinType.Inner, new TableSelect(_unionSelects, _unionTableAliasName), joinCondition);
            _selectSorted.getInnerSelect().Joins().add(_unionJoin);
        }        
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        replaceUnionJoin();
        ISelectSorted selectSorted;
        if (_selectSorted.SelectFields().size() > 0) {
            selectSorted = (ISelectSorted)_selectSorted.Clone();
            String tableName = selectSorted.getInnerSelect().getTable().getAliasName();
            for (String idFieldName : IdFieldNames()) {
                selectSorted.SelectFields().add(new FieldStandart(tableName, idFieldName));
            }
        } else {
            selectSorted = _selectSorted;
        }
        selectSorted.appendSqlString(sb);
    }

    @Override
    public void addBindValues(List<Object> destBindValues) {
        replaceUnionJoin();
        _selectSorted.addBindValues(destBindValues);
    }
}
