package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ICondition;
import com.github.nikolaymakhonin.utils.strings.StringUtilsExt;

import java.util.List;

public class ConditionLike implements ICondition {
    
    private final String _table;
    private final String _field;
    private final String _pattern;
    
    public ConditionLike(String table, String field, String pattern) {
        _table = table;
        _field = field;
        _pattern = pattern;
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        if (!StringUtilsExt.isNullOrEmpty(_table)) sb.append("[").append(_table).append("].");
        sb.append("[").append(_field).append("] LIKE ?");
    }
    
    @Override
    public void addBindValues(List<Object> destBindValues) {
        destBindValues.add(_pattern);
    }

    @Override
    public ICloneable Clone() {
        return new ConditionLike(_table, _field, _pattern);
    }   
    
}
