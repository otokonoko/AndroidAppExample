package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IFieldValues;
import com.github.nikolaymakhonin.utils.lists.map.SortDictIgnoreCase;

public class FieldValues extends SortDictIgnoreCase<Object> implements IFieldValues {

    public void addFieldValues(String field, Object value) {
        put(field, value);
    }

    public void addFieldValues(String[] fields, Object[] values) {
        int length = fields.length;
        if (length != values.length) {
            throw new IllegalArgumentException("fields.length != values.length");
        }
        for (int i = 0; i < length; i++) {
            put(fields[i], values[i]);
        }
    }

    @Override
    public ICloneable Clone() {
        FieldValues result = new FieldValues();
        for (Entry<String, Object> entry : this) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }   
}
