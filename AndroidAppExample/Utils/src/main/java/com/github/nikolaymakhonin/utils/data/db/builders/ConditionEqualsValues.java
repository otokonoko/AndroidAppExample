package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ICondition;
import com.github.nikolaymakhonin.utils.strings.StringUtilsExt;

import java.util.List;

public class ConditionEqualsValues implements ICondition {
    
    private final String _table;
    private final String[] _fields;
    private final Object[] _values;
    
    public ConditionEqualsValues(String table, String field, Object value) {
        this(table, new String[] { field }, new Object[] { value });
    }
    
    public ConditionEqualsValues(String table, String[] fields, Object[] values) {
        _table = table;
        _fields = fields;
        _values = values;
        if (_fields.length != _values.length) {
            throw new IllegalArgumentException("fields.length != values.length");
        }
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        int length = _fields.length;
        for (int i = 0; i < length; i++) {
            if (i > 0) sb.append(" AND ");
            if (!StringUtilsExt.isNullOrEmpty(_table)) sb.append("[").append(_table).append("].");
            sb.append("[").append(_fields[i]).append("]");
            if (_values[i] == null) {
                sb.append(" IS NULL");
            } else {
                sb.append(" = ?");
            }
        }
    }
    
    @Override
    public void addBindValues(List<Object> destBindValues) {
        for (Object value : _values) {
            if (value != null) destBindValues.add(value);            
        }
    }

    @Override
    public ICloneable Clone() {
        return new ConditionEqualsValues(_table, _fields.clone(), _values.clone());
    }   
    
}
