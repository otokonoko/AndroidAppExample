package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

public interface ISelectStatementSelect extends ISelectStatement {
    ISelect getInnerSelect();
    void setInnerSelect(ISelect innerSelect);    
}