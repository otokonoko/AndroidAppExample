package com.github.nikolaymakhonin.utils.contracts.patterns.mvp;

import com.github.nikolaymakhonin.utils.contracts.patterns.IDisposable;
import com.github.nikolaymakhonin.utils.contracts.patterns.ITreeModified;

public interface IViewPresenter<TView extends IView> extends ITreeModified, IDisposable {
    TView getView();

    void setView(TView value);

    boolean isAllowBindView();

    void setAllowBindView(boolean value);

    boolean isViewBind(boolean checkAttached);
}
