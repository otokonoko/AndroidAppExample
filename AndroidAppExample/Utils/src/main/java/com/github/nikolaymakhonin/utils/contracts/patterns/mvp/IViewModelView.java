package com.github.nikolaymakhonin.utils.contracts.patterns.mvp;

public interface IViewModelView<TViewModel> extends IView {

    void updateView(TViewModel viewModel);
}
