package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IField;

public class SelectFactory {
    
    public static IField[] createFieldsStandart(String tableName, String[] fieldNames) {
        return createFieldsStandart(tableName, fieldNames, null);
    }
    
    public static IField[] createFieldsStandart(String[] fieldNames) {
        return createFieldsStandart(null, fieldNames, null);
    }
    
    public static IField[] createFieldsStandart(String[] fieldNames, String[] aliasNames) {
        return createFieldsStandart(null, fieldNames, aliasNames);
    }
    
    public static IField[] createFieldsStandart(String tableName, String[] fieldNames, String[] aliasNames) {
        int length = fieldNames.length;
        IField[] result = new IField[length];
        for (int i = 0; i < length; i++) {
            if (aliasNames == null) {
                result[i] = new FieldStandart(tableName, fieldNames[i]);
            } else {
                result[i] = new FieldStandart(tableName, fieldNames[i], aliasNames[i]);
            }
        }
        return result;
    }
    
}
