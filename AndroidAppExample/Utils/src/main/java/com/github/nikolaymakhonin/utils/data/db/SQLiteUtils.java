package com.github.nikolaymakhonin.utils.data.db;

import android.annotation.TargetApi;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteProgram;
import android.os.Build;

import com.github.nikolaymakhonin.logger.Log;
import com.github.nikolaymakhonin.utils.ClassUtils;
import com.github.nikolaymakhonin.utils.ConvertUtils;
import com.github.nikolaymakhonin.utils.RefParam;
import com.github.nikolaymakhonin.utils.data.db.iterators.CursorToStringIterator;
import com.github.nikolaymakhonin.utils.serialization.IStreamSerializable;
import com.github.nikolaymakhonin.utils.serialization.StreamSerializerUtils;
import com.github.nikolaymakhonin.utils.time.DateTime;
import com.github.nikolaymakhonin.utils.time.TimeSpan;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import rx.functions.Action3;
import rx.functions.Func0;
import rx.functions.Func2;


public class SQLiteUtils {

    private static final String LOG_TAG = "SQLiteUtils";

    public static Cursor rawQuery(SQLiteDatabase db, String sql, String tableName, BaseCursorFactory cursorFactory) {
        String[] strArgs = new String[cursorFactory.getValues().size()];
        Arrays.fill(strArgs, "");
        // strArgs: Вот так через жопу реализован android.database.sqlite.SQLiteQuery
        // см. исходники:
        // https://github.com/sqlcipher/android-database-sqlcipher/blob/master/src/net/sqlcipher/database/SQLiteQuery.java
        
        Cursor cursor = db.rawQueryWithFactory(cursorFactory, sql, strArgs, tableName);
        
        return cursor;
    }
    
    public static Cursor rawQuery(SQLiteDatabase db, String sql, String tableName, List<Object> bindValues) {
        return rawQuery(db, sql, tableName, new CursorFactory(bindValues));
    }
    
    public static void BindObjects(SQLiteProgram statement, List<Object> bindValues) {
        int length = bindValues.size();
        for (int i = 0; i < length; i++) {
            BindObject(statement, i, bindValues.get(i));
        }
    }
    
    // region Binb Object
    private static final Action3<SQLiteProgram, Integer, Object> _bindLong = (statement, index, value) -> statement.bindLong(index + 1, (Long) value);
    
    private static final Action3<SQLiteProgram, Integer, Object> _bindInteger = (statement, index, value) -> statement.bindLong(index + 1, (Integer) value);
    
    private static final Action3<SQLiteProgram, Integer, Object> _bindByte = (statement, index, value) -> statement.bindLong(index + 1, (Byte) value);
    
    private static final Action3<SQLiteProgram, Integer, Object> _bindShort = (statement, index, value) -> statement.bindLong(index + 1, (Short) value);
    
    private static final Action3<SQLiteProgram, Integer, Object> _bindDateTime = (statement, index, value) -> statement.bindLong(index + 1, ((DateTime) value).Ticks);
    
    private static final Action3<SQLiteProgram, Integer, Object> _bindTimeSpan = (statement, index, value) -> statement.bindLong(index + 1, ((TimeSpan) value).Ticks);
    
    private static final Action3<SQLiteProgram, Integer, Object> _bindString = (statement, index, value) -> statement.bindString(index + 1, (String) value);
    
    private static final Action3<SQLiteProgram, Integer, Object> _bindDouble = (statement, index, value) -> statement.bindDouble(index + 1, (Double) value);
    
    private static final Action3<SQLiteProgram, Integer, Object> _bindFloat = (statement, index, value) -> statement.bindDouble(index + 1, (Float) value);
    
    private static final Action3<SQLiteProgram, Integer, Object> _bindBlob = (statement, index, value) -> statement.bindBlob(index + 1, (byte[]) value);
    
    private static final Action3<SQLiteProgram, Integer, Object> _bindUUID = (statement, index, value) -> statement.bindBlob(index + 1, ConvertUtils.UUIDToByteArray((UUID) value));
    
    private static Hashtable<Class, Action3<SQLiteProgram, Integer, Object>> _binders;
    
    private static Map<Class, Action3<SQLiteProgram, Integer, Object>> getBinders() {
        if (_binders != null) return _binders;
        _binders = new Hashtable<>();
        
        _binders.put(Long.class, _bindLong);
        _binders.put(Integer.class, _bindInteger);
        _binders.put(Byte.class, _bindByte);
        _binders.put(Short.class, _bindShort);
        _binders.put(long.class, _bindLong);
        _binders.put(int.class, _bindInteger);
        _binders.put(byte.class, _bindByte);
        _binders.put(short.class, _bindShort);
        
        _binders.put(DateTime.class, _bindDateTime);
        _binders.put(TimeSpan.class, _bindTimeSpan);
        
        _binders.put(Double.class, _bindDouble);
        _binders.put(Float.class, _bindFloat);
        _binders.put(double.class, _bindDouble);
        _binders.put(float.class, _bindFloat);
        
        _binders.put(String.class, _bindString);
        _binders.put(byte[].class, _bindBlob);
        _binders.put(UUID.class, _bindUUID);
        
        return _binders;
    }

    public static void BindObject(SQLiteProgram statement, int index, Object value) {
        if (value == null) {
            statement.bindNull(index + 1);
            return;
        }
        
        if (value instanceof IStreamSerializable) {
            RefParam<byte[]> buffer = new RefParam<>();
            if (!StreamSerializerUtils.SaveToByteArray((IStreamSerializable)value, buffer)) {
                return;
            }
            statement.bindBlob(index + 1, buffer.value);
            return;
        }

        Map<Class, Action3<SQLiteProgram, Integer, Object>> binders = getBinders();
        
        binders.get(value.getClass()).call(statement, index, value);
    }
    
    // endregion
    
    // region Get Object
    private static final Func2<Cursor, Integer, Object> _getLong = (cursor, index) -> cursor.getLong(index + 1);
    
    private static final Func2<Cursor, Integer, Object> _getInt = (cursor, index) -> cursor.getInt(index + 1);
    
    private static final Func2<Cursor, Integer, Object> _getShort = (cursor, index) -> cursor.getShort(index + 1);
    
    private static final Func2<Cursor, Integer, Object> _getFloat = (cursor, index) -> cursor.getFloat(index + 1);
    
    private static final Func2<Cursor, Integer, Object> _getDateTime = (cursor, index) -> new DateTime(cursor.getLong(index + 1));
    
    private static final Func2<Cursor, Integer, Object> _getTimeSpan = (cursor, index) -> new TimeSpan(cursor.getLong(index + 1));
    
    private static final Func2<Cursor, Integer, Object> _getString = (cursor, index) -> cursor.getString(index + 1);
    
    private static final Func2<Cursor, Integer, Object> _getDouble = (cursor, index) -> cursor.getDouble(index + 1);
    
    private static final Func2<Cursor, Integer, Object> _getBlob = (cursor, index) -> cursor.getBlob(index + 1);
    
    private static final Func2<Cursor, Integer, Object> _getUUID = (cursor, index) -> ConvertUtils.UUIDFromByteArray(cursor.getBlob(index + 1));
    
    private static Hashtable<Class, Func2<Cursor, Integer, Object>> _readers;
    
    private static Map<Class, Func2<Cursor, Integer, Object>> getReaders() {
        if (_readers != null) return _readers;
        _readers = new Hashtable<>();
        
        _readers.put(Long.class, _getLong);
        _readers.put(Integer.class, _getInt);
        _readers.put(Byte.class, _getShort);
        _readers.put(Short.class, _getShort);
        _readers.put(long.class, _getLong);
        _readers.put(int.class, _getInt);
        _readers.put(byte.class, _getShort);
        _readers.put(short.class, _getShort);
        
        _readers.put(DateTime.class, _getDateTime);
        _readers.put(TimeSpan.class, _getTimeSpan);
        
        _readers.put(Double.class, _getDouble);
        _readers.put(Float.class, _getFloat);
        _readers.put(double.class, _getDouble);
        _readers.put(float.class, _getFloat);
        
        _readers.put(String.class, _getString);
        _readers.put(byte[].class, _getBlob);
        _readers.put(UUID.class, _getUUID);
        
        return _readers;
    }
    
    public static <T> T GetObject(Cursor cursor, int index, Class<T> type, Func0<T> constructor) {
        if (cursor.isNull(index + 1)) return null;

        if (ClassUtils.IsSubClassOrInterface(type, IStreamSerializable.class)) {
            T instance;
            try {
                if (constructor != null) {
                    instance = constructor.call();
                } else {
                    Log.e(LOG_TAG, "Cannot create instance, constructor is null");
                    return null;
                }
            } catch (Exception e) {
                Log.e(LOG_TAG, "type.newInstance()", e);
                return null;
            }
            byte[] buffer = cursor.getBlob(index + 1);
            if (!StreamSerializerUtils.LoadFromByteArray((IStreamSerializable)instance, buffer)) {
                return null;
            }
            return instance;
        }
        
        Map<Class, Func2<Cursor, Integer, Object>> readers = getReaders();

        return (T)readers.get(type).call(cursor, index);
    }
    // endregion

    //region Cursor to String

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static String cursorToString(Cursor cursor) {
        CursorIterableSimple<String> lines = new CursorIterableSimple<>(cursor,
            cur -> new CursorToStringIterator(cur)
        );
        
        StringBuilder sb = new StringBuilder();
        for (String line : lines) {
            sb.append(line).append('\n');
        }
        
        return sb.toString();
    }
    
    //region
}
