package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.logger.Log;
import com.github.nikolaymakhonin.utils.CloneUtils;
import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ICondition;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IJoin;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ITable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.JoinType;

import java.util.List;

public class Join implements IJoin {
    private static final String LOG_TAG = "Join";
    private ITable     _table;
    private ICondition _condition;
    private JoinType   _joinType;
    
    /** For many conditions use IConditionList */
    public Join(JoinType joinType, ITable table, ICondition condition) {
        _table = table;
        _joinType = joinType;
        _condition = condition;
    }
    
    public Join(IJoin sourceJoin) {
        _table = (ITable)sourceJoin.getTable().Clone();
        _joinType = sourceJoin.getJoinType();
        setCondition(CloneUtils.Clone(sourceJoin.getCondition()));
    }
    
    @Override
    public ICloneable Clone() {
        return new Join(this);
    }
    
    @Override
    public JoinType getJoinType() {
        return _joinType;
    }
    
    @Override
    public void setJoinType(JoinType joinType) {
        _joinType = joinType;
    }
    
    @Override
    public ICondition getCondition() {
        return _condition;
    }
    
    @Override
    public void setCondition(ICondition condition) {
        _condition = condition;
    }
    
    @Override
    public ITable getTable() {
        return _table;
    }
    
    @Override
    public void setTable(ITable table) {
        _table = table;
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        switch (_joinType) {
            case Left:
                sb.append("LEFT JOIN ");
                break;
            case Cross:
                sb.append("CROSS JOIN ");
                break;
            case Inner:
                sb.append("INNER JOIN ");
                break;
            case LeftOuter:
                sb.append("LEFT OUTER JOIN ");
                break;
            default:
                Log.e(LOG_TAG, "Unknown JoinType: " + _joinType);
                return;
        }
        
        _table.appendSqlString(sb);
        
        if (_condition != null) {
            StringBuilder conditionsStr = new StringBuilder();
            _condition.appendSqlString(conditionsStr);
            if (conditionsStr.length() > 0) {
                sb.append(" ON ").append(conditionsStr);
            }
        }
        sb.append('\n');
    }
    
    @Override
    public void addBindValues(List<Object> destBindValues) {
        _table.addBindValues(destBindValues);
        _condition.addBindValues(destBindValues);
    }    
}
