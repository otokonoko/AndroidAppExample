package com.github.nikolaymakhonin.utils.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import com.github.nikolaymakhonin.logger.Log;
import com.github.nikolaymakhonin.utils.time.DateTime;
import com.github.nikolaymakhonin.utils.time.TimeSpan;

public abstract class DataBaseHelperBase extends SQLiteOpenHelper implements IDataBaseHelper {

    private static final String LOG_TAG = "DataBaseHelperBase";

    private static final Object _locker = new Object();

    @Override
    public Object Locker() {
        return _locker;
    }

    protected final Context context;
    private final TimeSpan _connectTimeout = TimeSpan.FromSeconds(60);
    private final String _dbName;
    private static final boolean _nativeLibsLoaded      = false;
    private static final Object  _loadNativeLibsLocker  = new Object();
    private final        Object  _createUpgradeDBLocker = new Object();

    public DataBaseHelperBase(
        Context context, String dbName, SQLiteDatabase.CursorFactory cursorFactory, int version
    )
    {
        super(context, dbName, cursorFactory, version);
        this.context = context;
        _dbName = dbName;
    }

    protected abstract void dbCreate(SQLiteDatabase db);

    protected abstract void dbUpgrade(SQLiteDatabase db, int oldVersion, int newVersion);

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(LOG_TAG, "onCreate for DB [" + _dbName + "]");
        synchronized (_locker) {
            try {
                Log.d(LOG_TAG, "Create tables in DB [" + _dbName + "] start");
                dbCreate(db);
                Log.i(LOG_TAG, "Create tables in DB [" + _dbName + "] succesfull");
            } catch (Exception e) {
                Log.e(LOG_TAG, "Create tables in DB [" + _dbName + "] error", e);
            } 
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(LOG_TAG, "onUpgrade for DB [" + _dbName + "]");
        try {
            Log.d(LOG_TAG, "Upgrade tables in DB [" + _dbName + "] start");
            dbUpgrade(db, oldVersion, newVersion);
            Log.i(LOG_TAG, "Upgrade tables in DB [" + _dbName + "] succesfull");
        } catch (Exception e) {
            Log.e(LOG_TAG, "Upgrade tables in DB [" + _dbName + "] error", e);
        } 
    }

    public synchronized SQLiteDatabase getReadableDatabase() {
        try {
            return super.getReadableDatabase();//(_password);
        } catch (Exception e) {
            Log.e(LOG_TAG, "getReadableDatabase", e);
            return null;
        }
    }

    public synchronized SQLiteDatabase getWritableDatabase() {
        try {
            DateTime startTime = new DateTime();
            while (true) {
                try {
                    return super.getWritableDatabase();//(_password);
                } catch (Exception exception) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        if (!(exception instanceof SQLiteDatabaseLockedException)) {
                            Log.e(LOG_TAG, "getWritableDatabase", exception);
                            return null;
                        }
                    } else if (!exception.getMessage().toLowerCase().contains("lock")) {
                        Log.e(LOG_TAG, "getWritableDatabase", exception);
                        return null;
                    }
                    if (new DateTime().Ticks - startTime.Ticks > _connectTimeout.Ticks) {
                        Log.e(LOG_TAG, "Database locked, connection timeout expired", exception);
                        return null;
                    }
                    Thread.sleep(1000);
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "getWritableDatabase", e);
            return null;
        }
    }

    private boolean _disposed;

    @Override
    public void dispose() {
        if (_disposed) {
            return;
        }
        _disposed = true;
        close();
    }

    @Override
    public boolean isDisposed() {
        return _disposed;
    }
}
