package com.github.nikolaymakhonin.utils.data.db.builders;

import java.util.List;

public class ConditionInListString extends ConditionInList<String> {

    public ConditionInListString(boolean ignoreCase, String table, String fieldName, Iterable<? extends String> list) {
        super(table, ignoreCase ? "lower_" + fieldName : fieldName, list);
    }

    public ConditionInListString(boolean ignoreCase, String table, String fieldName, Iterable<? extends String> list, boolean notInList) {
        super(table, ignoreCase ? "lower_" + fieldName : fieldName, list, notInList);
    }


    @Override
    public void addBindValues(List<Object> destBindValues) {
        if (_list != null) {
            for (String value : _list) {
                if (value != null) destBindValues.add(value.toLowerCase());
            }
        }
    }   
}