package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ICondition;

import java.util.List;

public class ConditionSimple implements ICondition {
    private final String _condition;
    
    public ConditionSimple(String condition) {
        _condition = condition;
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        sb.append(_condition);
    }
    
    @Override
    public void addBindValues(List<Object> destBindValues) {
    }

    @Override
    public ICloneable Clone() {
        return new ConditionSimple(_condition);
    }   
}
