package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ITable;
import com.github.nikolaymakhonin.utils.strings.StringUtilsExt;

import java.util.List;

public class TableSimple implements ITable {
    private final String _tableName;
    private final String _aliasName;
    
    public TableSimple(String tableName) {
        _tableName = tableName;
        _aliasName = null;
    }
    
    public TableSimple(String tableName, String aliasName) {
        _tableName = tableName;
        _aliasName = aliasName;
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        sb.append("[").append(_tableName).append("]");
        if (!StringUtilsExt.isNullOrEmpty(_aliasName)) {
            sb.append(" AS ").append("[").append(_aliasName).append("]");
        }
    }
    
    @Override
    public void addBindValues(List<Object> destBindValues) {
    }

    @Override
    public ICloneable Clone() {
        return new FieldSimple(_tableName, _aliasName);
    }

    @Override
    public String getAliasName() {
        return (!StringUtilsExt.isNullOrEmpty(_aliasName)) ? _aliasName : _tableName;
    }   
}
