package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.CompareUtils;
import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IField;
import com.github.nikolaymakhonin.utils.strings.StringUtilsExt;

import java.util.List;

public class FieldSimple implements IField {
    private final String _field;
    private final String _aliasName;
    
    public FieldSimple(String field) {
        _field = field;
        _aliasName = null;
    }
    
    public FieldSimple(String field, String aliasName) {
        _field = field;
        _aliasName = aliasName;
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        sb.append(_field);
        if (!StringUtilsExt.isNullOrEmpty(_aliasName)) {
            sb.append(" AS ").append("[").append(_aliasName).append("]");
        }
    }
    
    @Override
    public void addBindValues(List<Object> destBindValues) {
    }

    @Override
    public ICloneable Clone() {
        return new FieldSimple(_field, _aliasName);
    }

    @Override
    public String getAliasName() {
        return (!StringUtilsExt.isNullOrEmpty(_aliasName)) ? _aliasName : _field;
    }   

    @Override
    public int compareTo(IField another) {
        return CompareUtils.Compare(getAliasName(), another.getAliasName(), true);
    }
    
    @Override
    public int hashCode() {
        return  getAliasName().toLowerCase().hashCode();
    }
    
    @Override
    public boolean equals(Object o) {
        IField another = (IField)o;
        return getAliasName().equals(another.getAliasName());
    }
}
