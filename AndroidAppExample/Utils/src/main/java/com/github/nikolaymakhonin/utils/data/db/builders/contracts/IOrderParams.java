package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

import java.util.List;

public interface IOrderParams extends IStatement, Comparable<IOrderParams> {
    
    void appendSqlString(StringBuilder sb);
    
    void addBindValues(List<Object> destBindValues);
    
    String getTableName();
    
    String getFieldName();
    
    boolean getDescending();
    
}
