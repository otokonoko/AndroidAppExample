package com.github.nikolaymakhonin.utils.data.db.builders.contracts;

import com.github.nikolaymakhonin.utils.lists.list.IList;

public interface ISelect extends ISelectStatementTable {
    IList<IJoin> Joins();
    IWhere getWhere();
    void setWhere(IWhere where);
    ITable getTable();
    void setTable(ITable table);
}
