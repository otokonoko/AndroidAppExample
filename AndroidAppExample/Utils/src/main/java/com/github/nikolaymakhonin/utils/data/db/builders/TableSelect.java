package com.github.nikolaymakhonin.utils.data.db.builders;

import com.github.nikolaymakhonin.utils.contracts.patterns.ICloneable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ISelect;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ITable;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.IUnionSelects;

import java.util.List;

public class TableSelect implements ITable {
    private final ISelect _select;
    private final String  _aliasName;
    
    public TableSelect(ISelect select, String aliasName) {
        _select = select;
        _aliasName = aliasName;
    }
    
    @Override
    public void appendSqlString(StringBuilder sb) {
        sb.append("(");
        _select.appendSqlString(sb);
        sb.append(")");
        sb.append(" AS ").append(_aliasName);
    }
    
    @Override
    public void addBindValues(List<Object> destBindValues) {
        _select.addBindValues(destBindValues);
    }

    @Override
    public ICloneable Clone() {
        return new TableSelect((IUnionSelects)_select.Clone(), _aliasName);
    }

    @Override
    public String getAliasName() {
        return _aliasName;
    }   
}
