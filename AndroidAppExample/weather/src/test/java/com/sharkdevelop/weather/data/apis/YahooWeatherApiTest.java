package com.sharkdevelop.weather.data.apis;

import com.github.nikolaymakhonin.utils.strings.StringUtilsExt;
import com.github.nikolaymakhonin.utils_tests.AndroidTestBase;
import com.sharkdevelop.weather.data.apis.dto.YahooGeoPlaceResponse;
import com.sharkdevelop.weather.data.apis.dto.YahooWeatherForecastResponse;
import com.sharkdevelop.weather.di.components.ServiceComponent;
import com.sharkdevelop.weather.di.factories.ComponentsFactory;
import com.sharkdevelop.weather.di.factories.weather.WeatherDataFactory;
import com.sharkdevelop.weather.presentation.weather.data.Forecast;
import com.sharkdevelop.weather.presentation.weather.data.Place;
import com.sharkdevelop.weather.presentation.weather.data.Weather;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.GreaterOrEqual;
import org.mockito.internal.matchers.GreaterThan;
import org.mockito.internal.matchers.LessOrEqual;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

//docs: https://developer.android.com/training/testing/unit-testing/local-unit-tests.html
@RunWith(MockitoJUnitRunner.class)
public class YahooWeatherApiTest extends AndroidTestBase {

    //region Init

    private ServiceComponent _serviceComponent;

    @Before
    @Override
    public void setup() {
        super.setup();
        _serviceComponent = ComponentsFactory.buildServiceComponent(_mockContext);
    }

    //endregion

    @Test
    public void getGeoPlacesAsString() throws Exception {
        YahooWeatherApi api = _serviceComponent.getYahooWeatherApi();

        api.getBaseApi().getGeoPlacesAsString("select * from geo.places(1) where text=\"nome, ak\"")
            .first()
            .subscribe(response -> {
                Assert.assertFalse(StringUtilsExt.isNullOrEmpty(response));
                _lock.countDown();
            });

        assertTrue("Request timeout", _lock.await(10, TimeUnit.SECONDS));
    }

    @Test
    public void getWeatherForecastAsString() throws Exception {
        YahooWeatherApi api = _serviceComponent.getYahooWeatherApi();

        api.getBaseApi().getWeatherForecastAsString("select * from weather.forecast where woeid in (select woeid from geo.places(2) where text in (\"nome, ak\"))")
            .first()
            .subscribe(response -> {
                Assert.assertFalse(StringUtilsExt.isNullOrEmpty(response));
                _lock.countDown();
            });

        assertTrue("Request timeout", _lock.await(10, TimeUnit.SECONDS));
    }

    @Test
    public void getGeoPlaces() throws Exception {
        YahooWeatherApi api = _serviceComponent.getYahooWeatherApi();

        api.getBaseApi().getGeoPlaces("select * from geo.places(1) where text=\"nome, ak\"")
            .first()
            .subscribe(response -> {
                assertNotNull(response);
                assertNotNull(response.query.results.place);
                assertThat("response results count <= 0", response.query.results.place.size(), is(new GreaterThan(0)));

                checkConverter(response);

                _lock.countDown();
            });

        assertTrue("Request timeout", _lock.await(10, TimeUnit.SECONDS));
    }

    @Test
    public void getWeatherForecast() throws Exception {
        YahooWeatherApi api = _serviceComponent.getYahooWeatherApi();

        WeatherDataFactory dataFactory = new WeatherDataFactory();

        api.getBaseApi().getWeatherForecast("select * from weather.forecast where woeid in (select woeid from geo.places(2) where text in (\"nome, ak\"))")
            .first()
            .subscribe(response -> {
                assertNotNull(response);
                assertNotNull(response.query.results.channel);
                assertThat("response results count <= 0", response.query.results.channel.size(), is(new GreaterThan(0)));

                checkConverter(response);

                _lock.countDown();
            });

        assertTrue("Request timeout", _lock.await(10, TimeUnit.SECONDS));
    }

    @Test
    public void getPlaceInfo() throws Exception {
        YahooWeatherApi api = _serviceComponent.getYahooWeatherApi();

        WeatherDataFactory dataFactory = new WeatherDataFactory();

        int[] counter = { 0 };
        final int limit = 10;

        api.getPlaceInfo("al", limit)
           .subscribe(response -> {
               assertNotNull(response);
               assertNotNull(response.getLeft());
               assertNotNull(response.getRight());

               checkViewModel(WeatherDataFactory.fromYahooDTO(response.getLeft()));
               checkViewModel(WeatherDataFactory.fromYahooDTO(response.getRight()));

               int count = ++counter[0];
               assertThat(count, is(new LessOrEqual<>(limit)));

               if (count == 3) {
                   _lock.countDown();
               }
           });

        assertTrue("Request timeout", _lock.await(20, TimeUnit.SECONDS));
    }

    private void checkConverter(YahooWeatherForecastResponse response) {
        List<Weather> viewModels = WeatherDataFactory.fromYahooDTO(response);
        assertNotNull(viewModels);
        Assert.assertEquals(viewModels.size(), response.query.results.channel.size());

        //noinspection Convert2streamapi
        for (Weather viewModel: viewModels) {
            checkViewModel(viewModel);
        }
    }

    private void checkConverter(YahooGeoPlaceResponse response) {
        List<Place> viewModels = WeatherDataFactory.fromYahooDTO(response);
        assertNotNull(viewModels);
        Assert.assertEquals(viewModels.size(), response.query.results.place.size());

        //noinspection Convert2streamapi
        for (Place viewModel: viewModels) {
            checkViewModel(viewModel);
        }
    }

    private void checkViewModel(Weather viewModel) {
        assertNotNull(viewModel);
        assertNotNull(viewModel.getCloudiness());
        assertNotEquals(viewModel.getTemperature(), 0);
        assertNotEquals(viewModel.getHumidity(), 0);
        assertNotEquals(viewModel.getPressure(), 0);
        assertNotEquals(viewModel.getVisibility(), 0);
        assertNotEquals(viewModel.getWindSpeed(), 0);
        assertNotNull(viewModel.getForecasts());
        assertThat(viewModel.getForecasts().length, is(new GreaterThan(0)));
        for (Forecast forecast: viewModel.getForecasts()) {
            assertNotNull(forecast);
            assertNotNull(forecast.getDate());
            assertNotNull(forecast.getCloudiness());
            assertNotEquals(forecast.getTemperatureMin(), 0);
            assertNotEquals(forecast.getTemperatureMax(), 0);
            assertThat(forecast.getTemperatureMax(), is(new GreaterOrEqual(forecast.getTemperatureMin())));
        }
    }

    private void checkViewModel(Place viewModel) {
        assertNotNull(viewModel);
//        assertNotNull(viewModel.getCityName());
//        assertNotNull(viewModel.getRegionName());
        assertNotNull(viewModel.getCountryName());
        assertNotNull(viewModel.getActualDate());
        assertNotNull(viewModel.getPlaceName());
        assertNotEquals(viewModel.getId(), 0);
    }
}