PRAGMA encoding = "UTF-8";
PRAGMA locale = "en_US";

------------------ Config ------------------

CREATE TABLE [DataStorage] (
    [UUID] guid NOT NULL UNIQUE,
    [Data] image NOT NULL,
    CONSTRAINT [PK_Configs] PRIMARY KEY ([UUID])
);







