package com.sharkdevelop.weather.data.apis.dto;

/*
Generated from:
https://developer.yahoo.com/weather/
by jsonschema2pojo plugin

{"query": {
    "count": 2,
    "created": "2016-06-17T04:11:35Z",
    "lang": "ru",
    "results": {"channel": [
        {
            "units": {
                "distance": "mi",
                "pressure": "in",
                "speed": "mph",
                "temperature": "F"
            },
            "title": "Yahoo! Weather - Alagoas, BR",
            "link": "http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-2344845/",
            "description": "Yahoo! Weather for Alagoas, BR",
            "language": "en-us",
            "lastBuildDate": "Fri, 17 Jun 2016 01:11 AM BRT",
            "ttl": "60",
            "location": {
                "city": "Alagoas",
                "country": "Brazil",
                "region": " BR"
            },
            "wind": {
                "chill": "70",
                "direction": "203",
                "speed": "4"
            },
            "atmosphere": {
                "humidity": "100",
                "pressure": "989.0",
                "rising": "0",
                "visibility": "4.4"
            },
            "astronomy": {
                "sunrise": "5:39 am",
                "sunset": "5:13 pm"
            },
            "item": {
                "title": "Conditions for Alagoas, BR at 12:00 AM BRT",
                "lat": "-9.65535",
                "long": "-36.693802",
                "link": "http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-2344845/",
                "pubDate": "Fri, 17 Jun 2016 12:00 AM BRT",
                "condition": {
                    "code": "26",
                    "date": "Fri, 17 Jun 2016 12:00 AM BRT",
                    "temp": "69",
                    "text": "Cloudy"
                },
                "forecast": [
                    {
                        "code": "30",
                        "date": "17 Jun 2016",
                        "day": "Fri",
                        "high": "80",
                        "low": "70",
                        "text": "Partly Cloudy"
                    }
                ]
            }
        }
    ]}
}}
 */

import com.google.gson.annotations.SerializedName;

import java.net.URI;
import java.util.Date;
import java.util.List;

public class YahooWeatherForecastResponse extends YahooResponse<YahooWeatherForecastResponse.Results> {

    public static class Results {
        @SerializedName("channel")
        public List<Channel> channel;

        public static class Channel {
            @SerializedName("units")
            public Units      units;
            @SerializedName("title")
            public String     title;
            @SerializedName("link")
            public URI        link;
            @SerializedName("description")
            public String     description;
            @SerializedName("language")
            public String     language;
            @SerializedName("lastBuildDate")
            public Date       lastBuildDate;
            @SerializedName("ttl")
            public int        ttl;
            @SerializedName("location")
            public Location   location;
            @SerializedName("wind")
            public Wind       wind;
            @SerializedName("atmosphere")
            public Atmosphere atmosphere;
            @SerializedName("astronomy")
            public Astronomy  astronomy;
            @SerializedName("item")
            public Item       item;

            public enum DistanceUnits {
                @SerializedName("mi")
                Mi
            }

            public enum PressureUnits {
                @SerializedName("in")
                In
            }

            public enum SpeedUnits {
                @SerializedName("mph")
                Mph
            }

            public enum TemperatureUnits {
                @SerializedName("F")
                F
            }

            public static class Units {
                @SerializedName("distance")
                public DistanceUnits    distance;
                @SerializedName("pressure")
                public PressureUnits    pressure;
                @SerializedName("speed")
                public SpeedUnits       speed;
                @SerializedName("temperature")
                public TemperatureUnits temperature;
            }

            public static class Location {
                @SerializedName("city")
                public String city;
                @SerializedName("country")
                public String country;
                @SerializedName("region")
                public String region;
            }

            public static class Wind {
                @SerializedName("chill")
                public int chill;
                @SerializedName("direction")
                public int direction;
                @SerializedName("speed")
                public int speed;
            }

            public static class Atmosphere {
                @SerializedName("humidity")
                public int   humidity;
                @SerializedName("pressure")
                public float pressure;
                @SerializedName("rising")
                public int   rising;
                @SerializedName("visibility")
                public float visibility;
            }

            public static class Astronomy {
                @SerializedName("sunrise")
                public String sunrise;
                @SerializedName("sunset")
                public String sunset;
            }

            public static class Item {
                @SerializedName("title")
                public String         title;
                @SerializedName("lat")
                public float          lat;
                @SerializedName("long")
                public float          lng;
                @SerializedName("link")
                public URI            link;
                @SerializedName("pubDate")
                public Date           pubDate;
                @SerializedName("condition")
                public Condition      condition;
                @SerializedName("forecast")
                public List<Forecast> forecast;

                public static class Condition {
                    @SerializedName("code")
                    public int    code;
                    @SerializedName("date")
                    public Date   date;
                    @SerializedName("temp")
                    public int    temp;
                    @SerializedName("text")
                    public String text;
                }

                public enum DayOfWeek {
                    Sun,
                    Mon,
                    Tue,
                    Wed,
                    Thu,
                    Fri,
                    Sat
                }

                public static class Forecast {
                    @SerializedName("code")
                    public int       code;
                    @SerializedName("date")
                    public Date      date;
                    @SerializedName("day")
                    public DayOfWeek day;
                    @SerializedName("high")
                    public int       high;
                    @SerializedName("low")
                    public int       low;
                    @SerializedName("text")
                    public String    text;
                }
            }
        }
    }
}
