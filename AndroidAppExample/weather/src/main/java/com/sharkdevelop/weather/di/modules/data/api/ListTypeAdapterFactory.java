package com.sharkdevelop.weather.di.modules.data.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Parse single item or list as list.
 * <p>
 * for example:
 * <p>
 * items: "123"
 * items: [ "123" ]
 * <p>
 * In result will be array of strings always
 * <p>
 * Supported list types: List, Set, Array
 */
public class ListTypeAdapterFactory implements TypeAdapterFactory {

    private static final String LOG_TAG = "ListTypeAdapterFactory";

    //region Constructor

    private final Gson _baseGson;

    public ListTypeAdapterFactory() {
        GsonBuilder builder = new GsonBuilder();
        _baseGson = builder.create();
    }

    public ListTypeAdapterFactory(Gson gson) {
        _baseGson = gson;
    }

    //endregion

    //region Helpers

    private enum TypeClassification {
        Unknown,
        Array,
        List
    }

    private static TypeClassification getTypeClassification(Class type) {
        if (type.isArray()) {
            return TypeClassification.Array;
        } else if (type == List.class || type == Collection.class || type == Iterable.class) {
            return TypeClassification.List;
        }

        return TypeClassification.Unknown;
    }

    private static Type getComponentType(TypeToken typeToken) {

        Type type = typeToken.getType();
        if (type instanceof ParameterizedType) {
            Type[] argumentTypes = ((ParameterizedType) type).getActualTypeArguments();
            return argumentTypes[0];
        }

        return Object.class;
    }

    //endregion

    private final Object _locker = new Object();

    @Override
    public <T> TypeAdapter<T> create(final Gson gson, final TypeToken<T> typeToken) {
        final Class rawType = typeToken.getRawType();
        final TypeClassification typeClassification = getTypeClassification(rawType);

        final TypeAdapter<T> baseListTypeAdapter;

        switch (typeClassification) {
            case Array:
            case List:
                break;
            default:
                return null;
        }

        final Type componentType = getComponentType(typeToken);

        if (componentType == null) {
            return null;
        }

        final TypeAdapter<T>     nextAdapter = _baseGson.getAdapter(typeToken);

        return new TypeAdapter<T>() {
            @Override
            public void write(JsonWriter out, T value) throws IOException {
                out.jsonValue(gson.toJson(value));
            }

            @Override
            public T read(JsonReader in) throws IOException {
                if (in.peek() == JsonToken.BEGIN_ARRAY) {
                    return nextAdapter.read(in);
                } else {
                    Object item = gson.fromJson(in, componentType);
                    switch (typeClassification) {
                        case Array:
                            Object array = componentType instanceof Class
                                ? Array.newInstance((Class) componentType, 1)
                                : new Object[1];
                            Array.set(array, 0, item);
                            return (T) array;
                        case List:
                            List list = new ArrayList(1);
                            list.add(item);
                            return (T) list;
                        default:
                            throw new IOException("typeClassification == " + typeClassification);
                    }
                }
            }
        };
    }
}
