package com.sharkdevelop.weather.presentation.weather.presenters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.IRecyclerViewAdapterFactory;
import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.IViewModelPresenter;
import com.github.nikolaymakhonin.utils.lists.list.ICollectionChangedList;
import com.github.nikolaymakhonin.utils.lists.list.SortedList;
import com.github.nikolaymakhonin.utils.rx.RxOperators;
import com.github.nikolaymakhonin.utils_ui.patterns.mvp.BaseRecyclerViewAdapter;
import com.sharkdevelop.weather.R;
import com.sharkdevelop.weather.data.apis.YahooWeatherApi;
import com.sharkdevelop.weather.data.app.weather.WeatherDataStorage;
import com.sharkdevelop.weather.di.factories.weather.WeatherDataFactory;
import com.sharkdevelop.weather.presentation.weather.data.PlaceInfo;
import com.sharkdevelop.weather.presentation.weather.views.IPlaceInfoListItemView;

import org.apache.commons.lang3.tuple.Pair;

import java.util.concurrent.TimeUnit;

import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;

public class PlaceInfoListAdapter extends
                                  BaseRecyclerViewAdapter<IPlaceInfoListItemView, PlaceInfo,
                                      ICollectionChangedList<PlaceInfo>, IViewModelPresenter<IPlaceInfoListItemView, PlaceInfo>>
{
    private final YahooWeatherApi _yahooWeatherApi;
    private final EventBusWeather _eventBusWeather;
    private final WeatherDataStorage _weatherDataStorage;
    private final Context _context;

    public PlaceInfoListAdapter(
        IRecyclerViewAdapterFactory<IPlaceInfoListItemView, PlaceInfo, IViewModelPresenter<IPlaceInfoListItemView, PlaceInfo>> factory,
        YahooWeatherApi yahooWeatherApi, EventBusWeather eventBusWeather, WeatherDataStorage weatherDataStorage,
        Context context
    )
    {
        super(factory);
        _yahooWeatherApi = yahooWeatherApi;
        _eventBusWeather = eventBusWeather;
        _weatherDataStorage = weatherDataStorage;
        _context = context;

        ICollectionChangedList<PlaceInfo> list = _weatherDataStorage.getPlaceInfoList();
        if (!weatherDataStorage.getDataBaseInitialized()) {
            list.clear();
            String[] initialPlaces = _context.getResources().getStringArray(R.array.initial_places);
            if (initialPlaces != null) {
                for (String initialPlace: initialPlaces) {
                    addPlaceByQuery(initialPlace, 1);
                }
            }
        }

        setItems(list);
    }

    private Subscription _addPlaceRequestsSubscription;
    private Subscription _doAddPlaceSubscription;

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        //autofill Places List
        _addPlaceRequestsSubscription = _addPlaceRequests
            .lift(RxOperators.deferred(250, TimeUnit.MILLISECONDS))
            .observeOn(Schedulers.io())
            .flatMap(args -> _yahooWeatherApi.getPlaceInfo(args.getLeft(), args.getRight()))
            .map(response -> {
                PlaceInfo placeInfo = new PlaceInfo();
                placeInfo.setPlace(WeatherDataFactory.fromYahooDTO(response.getLeft()));
                placeInfo.setWeather(WeatherDataFactory.fromYahooDTO(response.getRight()));
                return placeInfo;
            })
            .subscribe(placeInfo -> getItems().add(placeInfo));

        _doAddPlaceSubscription = _eventBusWeather
            .doAddPlace()
            .observeOn(Schedulers.computation())
            .subscribe(query -> addPlaceByQuery(query, 1));
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        _addPlaceRequestsSubscription.unsubscribe();
        _doAddPlaceSubscription.unsubscribe();
    }

    private final Subject<Pair<String, Integer>, Pair<String, Integer>> _addPlaceRequests
        = PublishSubject.create();

    public void addPlaceByQuery(String query, int limit) {
        _addPlaceRequests.onNext(Pair.of(query, limit));
    }
}
