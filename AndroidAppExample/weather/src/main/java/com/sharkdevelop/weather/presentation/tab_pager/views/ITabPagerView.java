package com.sharkdevelop.weather.presentation.tab_pager.views;

import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.IView;
import com.sharkdevelop.weather.presentation.tab_pager.data.TabPageIds;

import rx.Observable;

public interface ITabPagerView extends IView {

    /** see: {@link TabPageIds} */
    void setActiveTabPage(int tabPageId);

    Observable<Integer> onSetActivePageBehaviour();
}
