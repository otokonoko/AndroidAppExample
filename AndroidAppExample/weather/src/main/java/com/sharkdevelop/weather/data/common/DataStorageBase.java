package com.sharkdevelop.weather.data.common;

import com.github.nikolaymakhonin.utils.ClassUtils;
import com.github.nikolaymakhonin.utils.contracts.patterns.ITreeModified;

import java.util.Hashtable;
import java.util.UUID;

import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Func0;
import rx.schedulers.Schedulers;

public abstract class DataStorageBase {

    private static final String                   LOG_TAG              = "DataStorageBase";
    private final        Hashtable<UUID, Action0> _autoSaveDetachFuncs = new Hashtable<>();
    private final        Hashtable<UUID, Object>  _cache               = new Hashtable<>();
    protected final      Object                   _locker              = new Object();

    //region Save/Load from database

    public abstract <T> T loadFromDataBase(UUID id, Class<T> type, Func0<T> constructor);
    public abstract boolean saveToDataBase(UUID id, Object object);
    public abstract boolean removeFromDataBase(UUID id);

    //endregion

    //region Save/Load with cache

    public <T> T loadObject(UUID id, boolean useCache, Class<T> type, Func0<T> constructor) {
        synchronized (_locker) {
            T object = null;
            if (useCache) {
                object = (T) _cache.get(id);
            }
            if (object == null && type != null) {
                object = loadFromDataBase(id, type, constructor);
            }
            return object;
        }
    }

    public void saveObject(UUID id, boolean useCache, Object object) {
        synchronized (_locker) {
            if (useCache) {
                _cache.put(id, object);
            }
            saveToDataBase(id, object);
        }
    }

    public void removeObject(UUID id) {
        removeFromCache(id);
        removeFromDataBase(id);
    }

    public void removeFromCache(UUID id) {
        synchronized (_locker) {
            _cache.remove(id);
        }
    }

    //endregion

    //region get/load object

    public  <T> T getObject(UUID id, boolean useCache) {
        return getObjectPrivate(id, useCache, false, null, (Func0<T>) null);
    }

    public  <T extends ITreeModified> T getObject(UUID id, boolean useCache, boolean autoSave) {
        return getObjectPrivate(id, useCache, true, null, (Func0<T>) null);
    }

    public  <T> T getObject(UUID id, boolean useCache, Class<T> type) {
        return getObjectPrivate(id, useCache, false, type);
    }

    public  <T extends ITreeModified> T getObject(UUID id, boolean useCache, boolean autoSave, Class<T> type) {
        return getObjectPrivate(id, useCache, true, type);
    }

    public  <T> T getObject(UUID id, boolean useCache, Class<T> type, Func0<T> constructor) {
        return getObjectPrivate(id, useCache, false, type, constructor);
    }

    public  <T extends ITreeModified> T getObject(UUID id, boolean useCache, boolean autoSave, Class<T> type, Func0<T> constructor) {
        return getObjectPrivate(id, useCache, true, type, constructor);
    }

    private <T> T getObjectPrivate(UUID id, boolean useCache, boolean autoSave, Class<T> type) {
        return getObjectPrivate(id, useCache, autoSave, type, () -> ClassUtils.newInstance(type));
    }

    private  <T> T getObjectPrivate(UUID id, boolean useCache, boolean autoSave, Class<T> type, Func0<T> constructor) {
        synchronized (_locker) {
            T object = loadObject(id, useCache, type, constructor);
            if (object == null) {
                if (constructor != null) {
                    object = constructor.call();
                    if (autoSave) {
                        bindAutoSave(id, (ITreeModified) object);
                    }
                    saveObject(id, useCache, object);
                }
            } else if (autoSave) {
                bindAutoSave(id, (ITreeModified) object);
            }
            return object;
        }
    }

    public void setObject(UUID id, boolean useCache, Object object) {
        setObjectPrivate(id, useCache, false, object);
    }

    public void setObject(UUID id, boolean useCache, boolean autoSave, ITreeModified object) {
        setObjectPrivate(id, useCache, true, object);
    }

    private void setObjectPrivate(UUID id, boolean useCache, boolean autoSave, Object object) {
        if (autoSave) {
            bindAutoSave(id, (ITreeModified) object);
        }
        saveObject(id, useCache, object);
    }

    //endregion

    //region Auto Save

    public void bindAutoSave(UUID id, ITreeModified object) {
        synchronized (_locker) {
            if (_autoSaveDetachFuncs.containsKey(id)) {
                return;
            }
            Subscription subscription = object
                .TreeModified()
                .observeOn(Schedulers.io())
                .subscribe(o -> saveToDataBase(id, object));
            _autoSaveDetachFuncs.put(id, () -> subscription.unsubscribe());
        }
    }

//    public <T extends ITreeModified> void bindAutoSave(UUID id, ICollectionChangedList<T> collection, WriteItemAction<T>
//        writeItemAction, ReadItemFunc<T> readItemFunc
//    ) {
//        synchronized (_locker) {
//            if (_autoSaveDetachFuncs.containsKey(id)) {
//                return;
//            }
//            CollectionModifiedMerger merger = new CollectionModifiedMerger(collection, writeItemAction, readItemFunc);
//            Subscription subscription = merger
//                .TreeModified()
//                .observeOn(Schedulers.io())
//                .subscribe(o -> saveToDataBase(id, merger));
//            _autoSaveDetachFuncs.put(id, () -> {
//                subscription.unsubscribe();
//                merger.dispose();
//            });
//        }
//    }

    public void unBindAutoSave(UUID id) {
        synchronized (_locker) {
            Action0 detachFunc = _autoSaveDetachFuncs.get(id);
            if (detachFunc != null) {
                detachFunc.call();
            }
        }
    }

    //endregion
}
