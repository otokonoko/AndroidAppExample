package com.sharkdevelop.weather.di.factories.weather;

import com.github.nikolaymakhonin.utils.time.DateTime;
import com.sharkdevelop.weather.data.apis.dto.YahooGeoPlaceResponse;
import com.sharkdevelop.weather.data.apis.dto.YahooWeatherForecastResponse;
import com.sharkdevelop.weather.presentation.weather.data.Forecast;
import com.sharkdevelop.weather.presentation.weather.data.Place;
import com.sharkdevelop.weather.presentation.weather.data.Weather;

import java.util.ArrayList;
import java.util.List;

public class WeatherDataFactory {

    private static final String LOG_TAG = "WeatherDataFactory";

    public static Place fromYahooDTO(YahooGeoPlaceResponse.Results.Place data) {
        if (data == null) {
            return null;
        }

        Place viewModel = new Place();

        YahooGeoPlaceResponse.Results.Place.PlaceInfo[] placeInfos = {
            data.country,
            data.admin1,
            data.admin2,
            data.admin3,
            data.locality1,
            data.locality2
        };

        for (YahooGeoPlaceResponse.Results.Place.PlaceInfo placeInfo : placeInfos) {
            if (placeInfo == null) {
                continue;
            }

            if (placeInfo.type == null) {
                viewModel.setRegionName(placeInfo.content);
                continue;
            }

            switch (placeInfo.type) {
                case Country:
                    viewModel.setCountryName(placeInfo.content);
                    break;
                case Town:
                    viewModel.setCityName(placeInfo.content);
                    break;
                case State:
                case District:
                case Province:
                    viewModel.setRegionName(placeInfo.content);
                    break;
            }
        }

        viewModel.setId(data.woeid);

        return viewModel;
    }

    public static List<Place> fromYahooDTO(YahooGeoPlaceResponse data) {
        if (data == null) {
            return null;
        }

        List<Place> viewModel = new ArrayList<>(data.query.results.place.size());

        for (YahooGeoPlaceResponse.Results.Place place: data.query.results.place) {
            viewModel.add(fromYahooDTO(place));
        }

        return viewModel;
    }

    public static Weather fromYahooDTO(YahooWeatherForecastResponse.Results.Channel data) {
        if (data == null) {
            return null;
        }

        Weather viewModel = new Weather();

        viewModel.setHumidity(data.atmosphere.humidity);
        viewModel.setPressure(data.atmosphere.pressure);
        viewModel.setTemperature(data.item.condition.temp);
        viewModel.setVisibility(data.atmosphere.visibility);
        viewModel.setWindSpeed(data.wind.speed);
        viewModel.setCloudiness(data.item.condition.text);

        int length = data.item.forecast.size();
        Forecast[] forecasts = new Forecast[length];

        for (int i = 0; i < length; i++) {
            YahooWeatherForecastResponse.Results.Channel.Item.Forecast source = data.item.forecast.get(i);

            Forecast dest = new Forecast();
            dest.setDate(new DateTime(source.date));
            dest.setTemperatureMin(source.low);
            dest.setTemperatureMax(source.high);
            dest.setCloudiness(source.text);

            forecasts[i] = dest;
        }

        viewModel.setForecasts(forecasts);

        return viewModel;
    }

    public static List<Weather> fromYahooDTO(YahooWeatherForecastResponse data) {
        if (data == null) {
            return null;
        }

        List<Weather> viewModel = new ArrayList<>(data.query.results.channel.size());

        //noinspection Convert2streamapi
        for (YahooWeatherForecastResponse.Results.Channel channel: data.query.results.channel) {
            viewModel.add(fromYahooDTO(channel));
        }

        return viewModel;
    }

}
