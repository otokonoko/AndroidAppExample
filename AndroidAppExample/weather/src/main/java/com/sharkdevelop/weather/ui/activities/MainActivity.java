package com.sharkdevelop.weather.ui.activities;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

import com.astuetz.PagerSlidingTabStrip;
import com.crashlytics.android.Crashlytics;
import com.github.florent37.materialviewpager.MaterialViewPager;
import com.github.florent37.materialviewpager.header.HeaderDesign;
import com.github.nikolaymakhonin.common_di.contracts.IHasAppComponentBase;
import com.github.nikolaymakhonin.logger.Log;
import com.github.nikolaymakhonin.utils_ui.contracts.IActivityPermissionsCallback;
import com.github.nikolaymakhonin.utils_ui.contracts.RequestPermissionsResult;
import com.sharkdevelop.weather.R;
import com.sharkdevelop.weather.di.components.AppComponent;
import com.sharkdevelop.weather.presentation.tab_pager.data.TabPageIds;
import com.sharkdevelop.weather.presentation.tab_pager.presenters.TabsFragmentAdapter;
import com.sharkdevelop.weather.presentation.tab_pager.views.ITabPagerView;
import com.sharkdevelop.weather.presentation.weather.views.IPlaceInfoAddView;
import com.sharkdevelop.weather.ui.contracts.IFragmentWithHeader;
import com.trello.rxlifecycle.ActivityEvent;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import io.fabric.sdk.android.Fabric;
import rebus.header.view.HeaderCompactView;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;

public class MainActivity extends RxAppCompatActivity
    implements IActivityPermissionsCallback, ITabPagerView, IPlaceInfoAddView
{

    private static final int LAYOUT = R.layout.activity_main;

    private static final String LOG_TAG = "MainActivity";

    private DrawerLayout         _drawerLayout;

    private MaterialViewPager     _materialViewPager;
    private ViewPager             _viewPager;
    private Toolbar               _toolbar;
    private PagerSlidingTabStrip  _tabStrip;
    private ActionBarDrawerToggle _drawerToggle;

    private NavigationView       _navigationView;

    private FloatingActionButton _floatingActionButton;

    protected AppComponent _appComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppDefault);

        super.onCreate(savedInstanceState);
        //noinspection ConstantIfStatement
        if (false) {
            Fabric.with(this, new Crashlytics());
        }
        setContentView(LAYOUT);

        initControls();
    }

    //region Life Cycle

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        _drawerToggle.syncState();
    }

    @Override
    protected void onResume() {
        super.onResume();
        _attachedSubject.onNext(_attached = true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        _attachedSubject.onNext(_attached = false);
    }

    //endregion

    //region Init controls

    private void initControls() {
        _appComponent = ((IHasAppComponentBase<AppComponent>) getApplicationContext()).getAppComponent();

        _drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        _materialViewPager = (MaterialViewPager) findViewById(R.id.viewPager);
        _viewPager = _materialViewPager.getViewPager();
        _toolbar = _materialViewPager.getToolbar();
        _tabStrip = _materialViewPager.getPagerTitleStrip();

        _navigationView = (NavigationView) findViewById(R.id.navigation);

        _floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);

        _appComponent.createTabPagerPresenter().setView(this);
        _appComponent.createPlaceInfoAddPresenter().setView(this);

        initToolbar();
        initActionBarToggle();
        initNavigationView();
        initTabPager();
        initFloatingButton();
    }

    private void initToolbar() {
        setSupportActionBar(_toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setHomeButtonEnabled(true);

        _drawerToggle = new ActionBarDrawerToggle(this, _drawerLayout, 0, 0);
        _drawerLayout.addDrawerListener(_drawerToggle);

        setTitle("");
        _toolbar.setOnMenuItemClickListener(menuItem -> false);
    }

    private void initActionBarToggle() {
        ActionBarDrawerToggle actionBarToggle = new ActionBarDrawerToggle(
            this,
            _drawerLayout,
            _toolbar,
            R.string.action_bar_menu_open,
            R.string.action_bar_menu_close);

        _drawerLayout.addDrawerListener(actionBarToggle);

        actionBarToggle.syncState();
    }

    private void initTabPager() {
        TabsFragmentAdapter tabPageAdapter = new TabsFragmentAdapter(getSupportFragmentManager(), _appComponent.getEventBusCommon());
        _viewPager.setAdapter(tabPageAdapter);
        _viewPager.setOffscreenPageLimit(_viewPager.getAdapter().getCount());
        _viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                int tabPageId;
                switch (position) {
                    case 0:
                        tabPageId = TabPageIds.PlaceInfoList;
                        break;
                    case 1:
                        tabPageId = TabPageIds.PlaceInfoDetails;
                        break;
                    default:
                        return;
                }
                _onSetActivePageSubject.onNext(tabPageId);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        _tabStrip.setViewPager(_viewPager);

        //Set tabs text color
        int textColor;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textColor = getResources().getColor(R.color.textPrimaryLight, getTheme());
        } else {
            //noinspection deprecation
            textColor = getResources().getColor(R.color.textPrimaryLight);
        }
        _tabStrip.setTextColor(textColor);

        //Switch header background
        _materialViewPager.setMaterialViewPagerListener(page -> {

            Fragment fragment = tabPageAdapter.getItem(page);
            if (fragment instanceof IFragmentWithHeader) {
                IFragmentWithHeader fragmentWithHeader = (IFragmentWithHeader) fragment;
                Drawable            headerDrawable;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    headerDrawable = getResources().getDrawable(fragmentWithHeader.getHeaderDrawableResId(), getTheme());
                } else {
                    //noinspection deprecation
                    headerDrawable = getResources().getDrawable(fragmentWithHeader.getHeaderDrawableResId());
                }
                return HeaderDesign.fromColorResAndDrawable(fragmentWithHeader.getHeaderColorResId(), headerDrawable);
            }

            return null;
        });
    }

    private void initNavigationView() {

        HeaderCompactView headerCompactView = new HeaderCompactView(this, false);
        headerCompactView.background().setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.primaryDark));
        headerCompactView.background().setImageResource(R.drawable.navigation_header_background_6);
        headerCompactView.avatar().setImageResource(R.drawable.navigation_header_icon);
        headerCompactView.username("Material Design");
        headerCompactView.email("nikolay.makhonin@gmail.com");
        headerCompactView.setOnHeaderClickListener(view ->
            _drawerLayout.closeDrawers()
        );
        _navigationView.addHeaderView(headerCompactView);
        _navigationView.setNavigationItemSelectedListener(menuItem -> true);
    }

    private void initFloatingButton() {
        _floatingActionButton.setOnClickListener(view -> runAddPlaceDialog());
    }

    //endregion

    //region Permissions Handler

    private final Subject<RequestPermissionsResult, RequestPermissionsResult> _requestPermissionsSubject = PublishSubject.create();

    private final Observable<RequestPermissionsResult> _requestPermissionsObservable = _requestPermissionsSubject
            .compose(bindUntilEvent(ActivityEvent.DESTROY))
            .observeOn(AndroidSchedulers.mainThread());

    @Override
    public Observable<RequestPermissionsResult> getRequestPermissionsObservable() {
        return _requestPermissionsObservable;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        _requestPermissionsSubject.onNext(new RequestPermissionsResult(requestCode, permissions, grantResults));
    }

    //endregion

    //region TabPager presentation

    @Override
    public void setActiveTabPage(int tabPageId) {
        switch (tabPageId) {
            case TabPageIds.PlaceInfoList:
                _viewPager.setCurrentItem(0);
                break;
            case TabPageIds.PlaceInfoDetails:
                _viewPager.setCurrentItem(1);
                break;
            default:
                Log.e(LOG_TAG, "Error tabPageId: " + tabPageId);
                break;
        }
    }

    private final Subject<Integer, Integer> _onSetActivePageSubject = BehaviorSubject.create(TabPageIds.PlaceInfoList);

    @Override
    public Observable<Integer> onSetActivePageBehaviour() {
        return _onSetActivePageSubject;
    }

    //region Attached

    private boolean _attached;

    public boolean isAttached() {
        return _attached;
    }

    private final Subject<Boolean, Boolean> _attachedSubject = PublishSubject.create();

    public Observable<Boolean> attachedObservable() {
        return _attachedSubject;
    }

    //endregion

    //endregion

    //region Add place

    private void runAddPlaceDialog() {
        View               dialoglayout = getLayoutInflater().inflate(R.layout.place_info_dialog_add, null);
        EditText           queryTextView = (EditText) dialoglayout.findViewById(R.id.query);

        AlertDialog.Builder builder      = new AlertDialog.Builder(this);
        builder.setMessage("Add place:");
        builder.setPositiveButton("Add", (dialog, which) -> {
            String query = queryTextView.getText().toString();
            _doAddPlace.onNext(query);
        });
        builder.setNegativeButton("Cancel", null);
        builder.setView(dialoglayout);
        final AlertDialog dialog = builder.create();
        dialog.show();
        queryTextView.setOnKeyListener((v, keyCode, event) -> {
            if (KeyEvent.KEYCODE_ENTER == keyCode) {
                String query = queryTextView.getText().toString();
                _doAddPlace.onNext(query);
                dialog.dismiss();
                return true;
            }
            return false;
        });
    }

    private final Subject _doAddPlace = PublishSubject.create();

    @Override
    public boolean isAvailable() {
        return _floatingActionButton.getVisibility() == View.VISIBLE;
    }

    @Override
    public void setAvailable(boolean value) {
        _floatingActionButton.setVisibility(value ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public Observable<String> doAddPlace() {
        return _doAddPlace;
    }

    //endregion
}
