package com.sharkdevelop.weather.di.components;

import com.github.nikolaymakhonin.common_di.components.AppComponentBase;
import com.github.nikolaymakhonin.common_di.scopes.PerApplication;
import com.sharkdevelop.weather.di.modules.AppModule;
import com.sharkdevelop.weather.presentation.common.EventBusCommon;
import com.sharkdevelop.weather.presentation.tab_pager.presenters.TabPagerPresenter;
import com.sharkdevelop.weather.presentation.weather.presenters.PlaceInfoAddPresenter;
import com.sharkdevelop.weather.presentation.weather.presenters.PlaceInfoDetailsPresenter;
import com.sharkdevelop.weather.presentation.weather.presenters.PlaceInfoListAdapter;

import dagger.Component;

@PerApplication
@Component(dependencies = { ServiceComponent.class }, modules = { AppModule.class })
public interface AppComponent extends AppComponentBase, ServiceComponent
{
    EventBusCommon getEventBusCommon();
    PlaceInfoAddPresenter createPlaceInfoAddPresenter();
    TabPagerPresenter createTabPagerPresenter();
    PlaceInfoListAdapter createPlaceInfoListAdapter();
    PlaceInfoDetailsPresenter createPlaceInfoDetailsPresenter();
}

