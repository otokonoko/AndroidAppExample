package com.sharkdevelop.weather.data.common;

import android.annotation.TargetApi;
import android.database.Cursor;
import android.os.Build;
import android.util.Log;

import com.github.nikolaymakhonin.utils.data.db.CursorIterator;
import com.github.nikolaymakhonin.utils.data.db.SQLiteQueryExecutor;
import com.github.nikolaymakhonin.utils.data.db.SQLiteUtils;
import com.github.nikolaymakhonin.utils.data.db.builders.Select;
import com.github.nikolaymakhonin.utils.data.db.builders.SelectFilter;
import com.github.nikolaymakhonin.utils.data.db.builders.SelectSorted;
import com.github.nikolaymakhonin.utils.data.db.builders.TableSimple;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ISelect;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ISelectFilter;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ISelectSorted;
import com.github.nikolaymakhonin.utils.data.db.builders.contracts.ITable;
import com.github.nikolaymakhonin.utils.data.db.iterators.CursorToObjectIterator;
import com.github.nikolaymakhonin.utils.lists.list.IList;
import com.github.nikolaymakhonin.utils.serialization.IStreamSerializable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import rx.functions.Func0;
import rx.functions.Func1;

public class SQLiteDataStorage extends DataStorageBase {

    private static final String LOG_TAG    = "SQLiteDataStorage";
    private static final String TABLE_NAME = "DataStorage";
    private final SQLiteQueryExecutor _db;

    public SQLiteDataStorage(SQLiteQueryExecutor db) {
        _db = db;
    }

    //region Save/Load from database

    @Override
    public <T> T loadFromDataBase(UUID id, Class<T> type, Func0<T> constructor) {
        synchronized (_locker) {
            //create SelectFilter
            ITable        table       = new TableSimple(TABLE_NAME);
            ISelect       select       = new Select(table);
            ISelectSorted selectSorted = new SelectSorted(select);
            ISelectFilter selectFilter = new SelectFilter(selectSorted);

            //create CursorIterator
            Func1<Cursor, CursorIterator<T>> cursorCursorIterator
                = cursor -> new CursorToObjectIterator<>(cursor, type, constructor);

            List<T> destList = new ArrayList<>(1);

            boolean result = _db.select(
                destList,
                selectFilter,
                cursorCursorIterator
            );

            if (!result) return null;

            if (destList.size() != 1) {
                Log.e(LOG_TAG, "Select result count != 1");
                if (destList.size() == 0) return null;
            }

            return destList.get(0);
        }
    }

    @Override
    public boolean saveToDataBase(UUID id, Object object) {
        synchronized (_locker) {
            return _db.insertOrReplace(
                TABLE_NAME,
                new String[] { "UUID" },
                new Object[] { id },
                new String[] { "Data" },
                new Object[] { object }
            );
        }
    }

    @Override
    public boolean removeFromDataBase(UUID id) {
        synchronized (_locker) {
            return _db.remove(
                TABLE_NAME,
                new String[] { "UUID" },
                new Object[] { id }
            );
        }
    }

    //endregion

}

