package com.sharkdevelop.weather.presentation.weather.data;

import com.github.nikolaymakhonin.utils.CompareUtils;
import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.BaseViewModel;
import com.github.nikolaymakhonin.utils.serialization.BinaryReader;
import com.github.nikolaymakhonin.utils.serialization.BinaryWriter;
import com.github.nikolaymakhonin.utils.strings.StringUtilsExt;
import com.github.nikolaymakhonin.utils.time.DateTime;
import com.sharkdevelop.weather.presentation.weather.helpers.StringConverters;

import java.text.DecimalFormat;

public class Weather extends BaseViewModel {

    //region Properties

    //region Humidity

    private float _humidity;

    public float getHumidity() {
        return _humidity;
    }

    public void setHumidity(float value) {
        if (CompareUtils.Equals(_humidity, value)) {
            return;
        }
        _humidity = value;
        Modified().onNext(null);
    }

    //endregion

    //region Pressure

    private float _pressure;

    public float getPressure() {
        return _pressure;
    }

    public void setPressure(float value) {
        if (CompareUtils.Equals(_pressure, value)) {
            return;
        }
        _pressure = value;
        Modified().onNext(null);
    }

    //endregion

    //region Temperature

    private float _temperature;

    public float getTemperature() {
        return _temperature;
    }

    public void setTemperature(float value) {
        if (CompareUtils.Equals(_temperature, value)) {
            return;
        }
        _temperature = value;
        Modified().onNext(null);
    }

    //endregion

    //region Cloudiness

    private String _cloudiness;

    public String getCloudiness() {
        return _cloudiness;
    }

    public void setCloudiness(String value) {
        if (CompareUtils.Equals(_cloudiness, value)) {
            return;
        }
        _cloudiness = value;
        Modified().onNext(null);
    }

    //endregion

    //region Visibility

    private float _visibility;

    public float getVisibility() {
        return _visibility;
    }

    public void setVisibility(float value) {
        if (CompareUtils.Equals(_visibility, value)) {
            return;
        }
        _visibility = value;
        Modified().onNext(null);
    }

    //endregion

    //region WindSpeed

    private float _windSpeed;

    public float getWindSpeed() {
        return _windSpeed;
    }

    public void setWindSpeed(float value) {
        if (CompareUtils.Equals(_windSpeed, value)) {
            return;
        }
        _windSpeed = value;
        Modified().onNext(null);
    }

    //endregion

    //region Forecast

    private Forecast[] _forecasts;

    public Forecast[] getForecasts() {
        return _forecasts;
    }

    public void setForecasts(Forecast[] value) {
        if (CompareUtils.EqualsObjects(_forecasts, value)) {
            return;
        }
        _forecasts = value;
        Modified().onNext(null);
    }

    //endregion

    //region ActualDate

    private DateTime _actualDate;

    public DateTime getActualDate() {
        return _actualDate;
    }

    public void setActualDate(DateTime value) {
        if (CompareUtils.Equals(_actualDate, value)) {
            return;
        }
        _actualDate = value;
        Modified().onNext(null);
    }

    //endregion

    //endregion

    //region Calculated Properties

    public String getTemperatureString() {
        return StringConverters.temperatureToString(_temperature);
    }

    public String getWindSpeedString() {
        return StringConverters.windSpeedToString(_windSpeed);
    }

    public String getPressureString() {
        return StringConverters.pressureToString(_pressure);
    }

    public String getHumidityString() {
        return StringConverters.humidityToString(_humidity);
    }

    public String getVisibilityString() {
        return StringConverters.visibilityToString(_visibility);
    }

    //endregion

    //region Serialization

    private static final int _currentVersion = 0;

    @Override
    public void Serialize(BinaryWriter writer) throws Exception {
        writer.write(_currentVersion);

        writer.WriteNullable(_actualDate);
        writer.write(_humidity);
        writer.write(_pressure);
        writer.write(_temperature);
        writer.write(_visibility);
        writer.write(_windSpeed);
        writer.WriteNullable(_cloudiness);

        writer.WriteCollection(_forecasts, (w, o) -> o.Serialize(w));
    }

    @Override
    public Object DeSerialize(BinaryReader reader) throws Exception {
        //noinspection UnusedAssignment
        int version = reader.readInt();

        setActualDate(reader.ReadNullableDateTime());
        setHumidity(reader.readFloat());
        setPressure(reader.readFloat());
        setTemperature(reader.readFloat());
        setVisibility(reader.readFloat());
        setWindSpeed(reader.readFloat());
        setCloudiness(reader.ReadNullableString());

        setForecasts(reader.ReadArray(Forecast[].class, r -> (Forecast) new Forecast().DeSerialize(r)));

        return this;
    }

    //endregion

}
