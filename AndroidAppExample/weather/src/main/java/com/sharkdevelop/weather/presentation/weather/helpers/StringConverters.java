package com.sharkdevelop.weather.presentation.weather.helpers;

import android.annotation.SuppressLint;

import com.github.nikolaymakhonin.utils.time.DateTime;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class StringConverters {

    public static String temperatureToString(float value) {
        StringBuilder sb = new StringBuilder();

        if (value > 0) {
            sb.append('+');
        }

        sb.append(new DecimalFormat("#F").format(value));

        return sb.toString();
    }

    public static String pressureToString(float value) {
        return new DecimalFormat("#.# in").format(value);
    }

    public static String visibilityToString(float value) {
        return new DecimalFormat("#.# mi").format(value);
    }

    public static String windSpeedToString(float value) {
        return new DecimalFormat("# mph").format(value);
    }

    public static String humidityToString(float value) {
        return new DecimalFormat("#.#").format(value);
    }

    public static String temperatureToString(float min, float max) {
        return temperatureToString(min) + " .. " + temperatureToString(max);
    }

    @SuppressLint("SimpleDateFormat")
    public static String shortDateToString(DateTime value) {
        if (value == null) {
            return null;
        }
        return new SimpleDateFormat("dd MMM yyyy").format(value.toDate());
    }

}
