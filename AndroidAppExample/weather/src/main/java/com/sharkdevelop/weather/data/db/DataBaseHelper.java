package com.sharkdevelop.weather.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.github.nikolaymakhonin.logger.Log;
import com.github.nikolaymakhonin.utils.data.db.DataBaseHelperBase;
import com.github.nikolaymakhonin.utils.data.db.SQLiteQueryHelper;
import com.github.nikolaymakhonin.utils.data.resources.RawResourceHelper;
import com.sharkdevelop.weather.R;

import java.util.Locale;

public class DataBaseHelper extends DataBaseHelperBase {
	public DataBaseHelper(Context context, String dbName) {
		super(context, dbName, null, 1);
		Locale.setDefault(new Locale("en", "US"));
	}

	@Override
	public void dbCreate(SQLiteDatabase db) {
	    db.setLocale(new Locale("en", "US"));
		String dbCreateSql = RawResourceHelper.getText(context, R.raw.db_create_v1);
		for (String statement : SQLiteQueryHelper.splitBatchSql(dbCreateSql)) {
	        Log.d("StatisticDataBaseModel", "------------ Sql ------------\n" + statement);
    		db.execSQL(statement);
        }
	}

	@Override
	public void dbUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}
}