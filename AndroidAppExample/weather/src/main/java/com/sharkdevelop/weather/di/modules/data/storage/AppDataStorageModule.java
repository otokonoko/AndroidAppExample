package com.sharkdevelop.weather.di.modules.data.storage;

import android.database.sqlite.SQLiteOpenHelper;

import com.github.nikolaymakhonin.common_di.scopes.PerService;
import com.github.nikolaymakhonin.utils.data.db.SQLiteQueryExecutor;
import com.sharkdevelop.weather.data.app.CommonDataStorage;
import com.sharkdevelop.weather.data.common.MemoryDataStorage;
import com.sharkdevelop.weather.data.common.SQLiteDataStorage;

import dagger.Module;
import dagger.Provides;

@Module
public class AppDataStorageModule {

    @Provides
    @PerService
    public static MemoryDataStorage getMemoryDataStorage() {
        return new MemoryDataStorage();
    }

    @Provides
    @PerService
    public static SQLiteDataStorage getSQLiteDataStorage(SQLiteQueryExecutor sqliteQueryExecutor) {
        return new SQLiteDataStorage(sqliteQueryExecutor);
    }

    @Provides
    @PerService
    public static CommonDataStorage getCommonDataStorage(SQLiteDataStorage dataStorage) {
        return new CommonDataStorage(dataStorage);
    }

}
