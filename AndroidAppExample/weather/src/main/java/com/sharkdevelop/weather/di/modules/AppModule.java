package com.sharkdevelop.weather.di.modules;

import com.github.nikolaymakhonin.common_di.modules.app.AppModuleBase;
import com.github.nikolaymakhonin.common_di.scopes.PerApplication;
import com.sharkdevelop.weather.data.app.CommonDataStorage;
import com.sharkdevelop.weather.di.modules.presentation.WeatherPresentationModule;
import com.sharkdevelop.weather.presentation.common.EventBusCommon;
import com.sharkdevelop.weather.presentation.tab_pager.presenters.TabPagerPresenter;

import dagger.Module;
import dagger.Provides;

@Module(includes = { AppModuleBase.class, WeatherPresentationModule.class })
public class AppModule {

    @Provides
    @PerApplication
    public EventBusCommon getEventBusCommon() {
        return new EventBusCommon();
    }

    @Provides
    public static TabPagerPresenter createTabPagerPresenter(EventBusCommon eventBusCommon, CommonDataStorage commonDataStorage)
    {
        return new TabPagerPresenter(eventBusCommon, commonDataStorage);
    }

}
