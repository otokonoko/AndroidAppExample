package com.sharkdevelop.weather.data.apis;

import com.sharkdevelop.weather.data.apis.dto.YahooGeoPlaceResponse;
import com.sharkdevelop.weather.data.apis.dto.YahooWeatherForecastResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import rx.Observable;

import static br.com.zbra.androidlinq.Linq.stream;

public class YahooWeatherApi {

    private final YahooWeatherApiBase _baseApi;

    public YahooWeatherApi(YahooWeatherApiBase baseApi) {
        _baseApi = baseApi;
    }

    public YahooWeatherApiBase getBaseApi() {
        return _baseApi;
    }

    private static final Pattern _yqlExcapeRegex = Pattern.compile("[\\'\"]");

    /** Return combined place and weather info */
    public Observable<Pair<YahooGeoPlaceResponse.Results.Place, YahooWeatherForecastResponse.Results.Channel>> getPlaceInfo(
        String query, int limit
    )
    {
        //docs: https://developer.yahoo.com/yql/guide/select.html
        //test form: https://developer.yahoo.com/weather/

        String escapeQuery = _yqlExcapeRegex.matcher(query).replaceAll("\\$&");
        String getPlacesQuery = String.format("select * from geo.places(%s) where text=\"%s\"", limit, escapeQuery);

        return _baseApi.getGeoPlaces(getPlacesQuery).flatMap(placeResponse -> {

            List<YahooGeoPlaceResponse.Results.Place> places = placeResponse.query.results.place;

            if (places.size() == 0) {
                return null;
            }

            Map<Integer, YahooGeoPlaceResponse.Results.Place> placesMap =
                stream(places)
                    .toMap(p -> p.woeid, p -> p);

            Set<Integer> ids = placesMap.keySet();

            String getWeathersQuery = String.format("select * from weather.forecast where woeid in ( %s )",
                    StringUtils.join(ids, ","));

            return Observable.zip(
                Observable.from(ids),
                _baseApi.getWeatherForecast(getWeathersQuery)
                        .flatMapIterable(weatherResponse -> weatherResponse.query.results.channel),
                (id, channel) -> Pair.of(placesMap.get(id), channel)
            );
        });
    }
}
