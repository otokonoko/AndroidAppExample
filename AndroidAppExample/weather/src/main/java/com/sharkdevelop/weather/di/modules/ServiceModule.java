package com.sharkdevelop.weather.di.modules;

import com.github.nikolaymakhonin.common_di.modules.service.ServiceModuleBase;
import com.sharkdevelop.weather.di.modules.data.api.HttpModule;
import com.sharkdevelop.weather.di.modules.data.api.WebApiModule;
import com.sharkdevelop.weather.di.modules.data.db.DataBaseModule;
import com.sharkdevelop.weather.di.modules.data.storage.DataStorageModule;

import dagger.Module;

@Module(includes = {
    ServiceModuleBase.class,
    HttpModule.class,
    WebApiModule.class,
    DataStorageModule.class,
    DataBaseModule.class
})
public class ServiceModule {

}
