package com.sharkdevelop.weather.ui.contracts;

public interface IFragmentWithHeader {
    int getHeaderColorResId();
    int getHeaderDrawableResId();
}
