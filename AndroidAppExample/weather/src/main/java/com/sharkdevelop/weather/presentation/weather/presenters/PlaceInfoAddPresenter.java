package com.sharkdevelop.weather.presentation.weather.presenters;

import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.ViewPresenter;
import com.sharkdevelop.weather.presentation.common.EventBusCommon;
import com.sharkdevelop.weather.presentation.tab_pager.data.TabPageIds;
import com.sharkdevelop.weather.presentation.weather.views.IPlaceInfoAddView;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PlaceInfoAddPresenter extends ViewPresenter<IPlaceInfoAddView> {

    private final EventBusCommon  _eventBusCommon;
    private final EventBusWeather _eventBusWeather;

    public PlaceInfoAddPresenter(EventBusCommon eventBusCommon, EventBusWeather eventBusWeather) {
        _eventBusCommon = eventBusCommon;
        _eventBusWeather = eventBusWeather;
    }

    private Subscription _onSetActiveTabPageSubscription;
    private Subscription _doAddPlaceSubscription;

    @Override
    protected void bindView() {
        super.bindView();

        _onSetActiveTabPageSubscription = _eventBusCommon
            .onSetActiveTabPageBehaviour()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(tabPageId -> {
                IPlaceInfoAddView view = getView();
                if (!isViewBind(true, view)) {
                    return;
                }
                switch (tabPageId) {
                    case TabPageIds.PlaceInfoList:
                        view.setAvailable(true);
                        break;
                    default:
                        view.setAvailable(false);
                        break;
                }
            });

        _doAddPlaceSubscription = getView()
            .doAddPlace()
            .observeOn(Schedulers.computation())
            .subscribe(query -> {
                _eventBusWeather.doAddPlace().onNext(query);
            });
    }

    @Override
    protected void unBindView() {
        super.unBindView();

        if (_doAddPlaceSubscription != null) {
            _doAddPlaceSubscription.unsubscribe();
            _doAddPlaceSubscription = null;
        }
        if (_onSetActiveTabPageSubscription != null) {
            _onSetActiveTabPageSubscription.unsubscribe();
            _onSetActiveTabPageSubscription = null;
        }
    }
}
