package com.sharkdevelop.weather.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.nikolaymakhonin.common_di.contracts.IHasAppComponentBase;
import com.github.nikolaymakhonin.utils_ui.controls.RecyclerViewMaterialAdapterExt;
import com.sharkdevelop.weather.R;
import com.sharkdevelop.weather.di.components.AppComponent;
import com.sharkdevelop.weather.presentation.weather.presenters.PlaceInfoListAdapter;
import com.sharkdevelop.weather.ui.contracts.IFragmentWithHeader;

public class PlaceInfoListFragment extends Fragment implements IFragmentWithHeader {

    private static final int LAYOUT = R.layout.fragment_place_info_list;
    private static final int HEADER_COLOR = R.color.toolBarForBackground8;
    private static final int HEADER_DRAWABLE = R.drawable.navigation_header_background_8;

    private View                           _contentView;
    private RecyclerView                   _recyclerView;
    private RecyclerViewMaterialAdapterExt _recyclerViewMaterialAdapter;
    private PlaceInfoListAdapter           _placeInfoListAdapter;

    private AppComponent _appComponent;

    //region Override methods

    @Override
    public int getHeaderColorResId() {
        return HEADER_COLOR;
    }

    @Override
    public int getHeaderDrawableResId() {
        return HEADER_DRAWABLE;
    }

    //endregion

    //region Create Instance

    public static PlaceInfoListFragment getInstance() {
        Bundle                args     = new Bundle();
        PlaceInfoListFragment fragment = new PlaceInfoListFragment();
        fragment.setArguments(args);

        return fragment;
    }

    //endregion

    //region Init Controls

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState)
    {
        _appComponent = ((IHasAppComponentBase<AppComponent>)getContext().getApplicationContext()).getAppComponent();

        _contentView = inflater.inflate(LAYOUT, container, false);
        initControls();

        return _contentView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initControls() {
        _recyclerView = (RecyclerView) _contentView.findViewById(R.id.recyclerView);
        _recyclerView.setLayoutManager(new LinearLayoutManager(getContext().getApplicationContext()));
        _recyclerView.setHasFixedSize(true);
        _placeInfoListAdapter = _appComponent.createPlaceInfoListAdapter();
        _recyclerViewMaterialAdapter = new RecyclerViewMaterialAdapterExt(_placeInfoListAdapter);
        _recyclerView.setAdapter(_recyclerViewMaterialAdapter);
        MaterialViewPagerHelper.registerRecyclerView(getActivity(), _recyclerView, null);
    }

    //endregion
}
