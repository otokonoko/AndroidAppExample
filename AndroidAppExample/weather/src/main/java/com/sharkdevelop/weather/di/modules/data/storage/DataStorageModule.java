package com.sharkdevelop.weather.di.modules.data.storage;

import dagger.Module;

@Module(includes = { AppDataStorageModule.class, WeatherDataStorageModule.class })
public class DataStorageModule {

}
