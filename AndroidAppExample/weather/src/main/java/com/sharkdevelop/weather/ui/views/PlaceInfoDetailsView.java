package com.sharkdevelop.weather.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.nikolaymakhonin.common_di.contracts.IHasAppComponentBase;
import com.github.nikolaymakhonin.utils_ui.patterns.mvp.BaseView;
import com.sharkdevelop.weather.R;
import com.sharkdevelop.weather.di.components.AppComponent;
import com.sharkdevelop.weather.presentation.weather.data.Forecast;
import com.sharkdevelop.weather.presentation.weather.data.PlaceInfo;
import com.sharkdevelop.weather.presentation.weather.presenters.PlaceInfoListItemPresenter;
import com.sharkdevelop.weather.presentation.weather.views.IPlaceInfoView;

public class PlaceInfoDetailsView extends BaseView<PlaceInfoListItemPresenter, PlaceInfo> implements IPlaceInfoView {

    private static final int LAYOUT = R.layout.place_info_details;

    protected AppComponent _appComponent;
    private   LinearLayout _tables;
    private   TableLayout  _placeTable;
    private   TableLayout  _weatherTable;
    private   TableLayout  _forecastTable;

    //region Constructors

    public PlaceInfoDetailsView(Context context) {
        super(context);
    }

    public PlaceInfoDetailsView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PlaceInfoDetailsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public PlaceInfoDetailsView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    //endregion

    //region Init Controls

    @Override
    protected void initControls() {
        super.initControls();
        _appComponent = ((IHasAppComponentBase<AppComponent>) getContext().getApplicationContext()).getAppComponent();
        LayoutInflater.from(getContext()).inflate(LAYOUT, this, true);
        _tables = (LinearLayout) findViewById(R.id.tables);

        _placeTable = createTableView("Place:");
        _weatherTable = createTableView("Weather:");
        _forecastTable = createTableView("Forecast:");

        ViewGroup.LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(layoutParams);

        _appComponent.createPlaceInfoDetailsPresenter().setView(this);
    }

    private TableLayout createTableView(String title) {
        View        view          = inflate(getContext(), R.layout.place_info_table, null);
        _tables.addView(view);
        TableLayout tableLayout   = (TableLayout) view.findViewById(R.id.table);
        TextView    titleTextView = (TextView) view.findViewById(R.id.title);
        titleTextView.setText(title);
        return tableLayout;
    }

    //endregion

    //region Update View

    private void updateDetailsRow(TableLayout table, int nRow, String... cellTexts) {
        TableRow row = (TableRow) table.getChildAt(nRow);

        TextView fieldNameTextView   = (TextView) row.findViewById(R.id.fieldName);
        TextView fieldValueTextView  = (TextView) row.findViewById(R.id.fieldValue);
        TextView fieldValue2TextView = (TextView) row.findViewById(R.id.fieldValue2);

        fieldNameTextView.setText(cellTexts[0]);
        fieldValueTextView.setText(cellTexts[1]);
        if (cellTexts.length > 2) {
            fieldValue2TextView.setVisibility(VISIBLE);
            fieldValue2TextView.setText(cellTexts[2]);
        } else {
            fieldValue2TextView.setVisibility(GONE);
        }
    }

    private void setTableRowCount(TableLayout table, int rowCount) {
        while (rowCount > table.getChildCount()) {
            inflate(getContext(), R.layout.place_info_row, table);
        }
        while (rowCount < table.getChildCount()) {
            table.removeViewAt(rowCount);
        }
    }

    @Override
    public void updateView(PlaceInfo placeInfo) {
        if (placeInfo != null) {
            setTableRowCount(_placeTable, 3);
            updateDetailsRow(_placeTable, 0, "City", placeInfo.getPlace().getCityName());
            updateDetailsRow(_placeTable, 1, "Region", placeInfo.getPlace().getRegionName());
            updateDetailsRow(_placeTable, 2, "Country", placeInfo.getPlace().getCountryName());

            setTableRowCount(_weatherTable, 6);
            updateDetailsRow(_weatherTable, 0, "Temperature", placeInfo.getWeather().getTemperatureString());
            updateDetailsRow(_weatherTable, 1, "Cloudiness", placeInfo.getWeather().getCloudiness());
            updateDetailsRow(_weatherTable, 2, "WindSpeed", placeInfo.getWeather().getWindSpeedString());
            updateDetailsRow(_weatherTable, 3, "Pressure", placeInfo.getWeather().getPressureString());
            updateDetailsRow(_weatherTable, 4, "Visibility", placeInfo.getWeather().getVisibilityString());
            updateDetailsRow(_weatherTable, 5, "Humidity", placeInfo.getWeather().getHumidityString());

            Forecast[] forecasts = placeInfo.getWeather().getForecasts();
            setTableRowCount(_forecastTable, forecasts.length);
            for (int i = 0; i < forecasts.length; i++) {
                Forecast forecast = forecasts[i];

                updateDetailsRow(_forecastTable, i, forecast.getDateString(), forecast.getTemperaturesString(),
                    forecast.getCloudiness()
                );
            }
        }
    }

    //endregion
}
