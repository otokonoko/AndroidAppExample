package com.sharkdevelop.weather.di.modules.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import com.github.nikolaymakhonin.common_di.scopes.PerService;
import com.github.nikolaymakhonin.utils.data.db.SQLiteQueryExecutor;
import com.sharkdevelop.weather.data.db.DataBaseHelper;

import dagger.Module;
import dagger.Provides;

@Module
public class DataBaseModule {

    private static final String DB_NAME = "AppDataBase";

    @Provides
    @PerService
    public static SQLiteOpenHelper getDataBaseHelper(Context appContext) {
        return new DataBaseHelper(appContext, DB_NAME);
    }

    @Provides
    @PerService
    public static SQLiteQueryExecutor getSQLiteQueryExecutor(SQLiteOpenHelper sqliteOpenHelper) {
        return new SQLiteQueryExecutor(sqliteOpenHelper);
    }

}
