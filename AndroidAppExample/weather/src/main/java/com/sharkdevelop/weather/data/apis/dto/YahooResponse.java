package com.sharkdevelop.weather.data.apis.dto;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/*
Generated from:
http://developer.yahoo.com/yql/console/?q=select%20*%20from%20geo.places(1)%20where%20text%3D%22nome%2C%20ak%22&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys
by jsonschema2pojo plugin

{"query": {
    "count": 1,
    "created": "2016-06-17T01:48:28Z",
    "lang": "en-US",
    "results": { ... }
}}
 */

public class YahooResponse<TResult> {

    @SerializedName("query")
    public Query<TResult> query;

    public static class Query<TResult> {
        @SerializedName("count")
        public int     count;
        @SerializedName("created")
        public Date    created;
        @SerializedName("lang")
        public String  lang;
        @SerializedName("results")
        public TResult results;
    }
}