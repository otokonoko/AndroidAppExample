package com.sharkdevelop.weather.di.factories;

import android.content.Context;
import android.support.annotation.NonNull;

import com.github.nikolaymakhonin.common_di.modules.service.ServiceModuleBase;
import com.sharkdevelop.weather.di.components.AppComponent;
import com.sharkdevelop.weather.di.components.DaggerAppComponent;
import com.sharkdevelop.weather.di.components.DaggerServiceComponent;
import com.sharkdevelop.weather.di.components.ServiceComponent;

public final class ComponentsFactory {

    public static AppComponent buildAppComponent(@NonNull Context appContext) {

        ServiceComponent serviceComponent = buildServiceComponent(appContext);

        AppComponent appComponent = DaggerAppComponent.builder()
            .serviceComponent(serviceComponent)
            .build();

        return appComponent;
    }

    public static ServiceComponent buildServiceComponent(@NonNull Context appContext) {
        ServiceComponent serviceComponent = DaggerServiceComponent.builder()
            .serviceModuleBase(new ServiceModuleBase(appContext))
            .build();

        return serviceComponent;
    }
}
