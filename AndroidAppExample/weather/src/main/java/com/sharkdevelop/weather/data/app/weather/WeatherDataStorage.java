package com.sharkdevelop.weather.data.app.weather;

import android.content.Context;

import com.github.nikolaymakhonin.utils.lists.list.ICollectionChangedList;
import com.github.nikolaymakhonin.utils.lists.list.SortedList;
import com.github.nikolaymakhonin.utils.rx.CollectionModifiedMerger;
import com.github.nikolaymakhonin.utils.serialization.WriteItemAction;
import com.sharkdevelop.weather.data.common.DataStorageBase;
import com.sharkdevelop.weather.presentation.weather.data.PlaceInfo;

import java.util.UUID;

public class WeatherDataStorage {

    private final DataStorageBase _dataStorage;

    private static UUID DATA_BASE_INITIALIZED = UUID.fromString("2155d6b9-6609-4e28-bcec-36f4c7861290");
    private static UUID PLACE_INFO_LIST_ID = UUID.fromString("05e1ce88-089f-4809-9ef5-9c0e75c1789c");
    private static UUID SELECTED_PLACE_ID  = UUID.fromString("69c18b78-22aa-4100-9322-bd5b620a91d4");
    private static UUID ACTIVE_TAB_PAGE_ID = UUID.fromString("b9c9b6fd-eedb-46a0-9ff5-7b1f5e766736");

    public WeatherDataStorage(DataStorageBase dataStorage) {
        _dataStorage = dataStorage;
    }

    public ICollectionChangedList<PlaceInfo> getPlaceInfoList() {
        CollectionModifiedMerger<PlaceInfo> collectionModifiedMerger = _dataStorage.getObject(
            PLACE_INFO_LIST_ID,
            true, true, CollectionModifiedMerger.class,
            () -> new CollectionModifiedMerger(
                new SortedList(true, true),
                (WriteItemAction<PlaceInfo>) (w, o) -> o.Serialize(w),
                r -> new PlaceInfo().DeSerialize(r))
        );

        return collectionModifiedMerger.getList();
    }

    public PlaceInfo getSelectedPlace() {
        PlaceInfo placeInfo =
            _dataStorage.getObject(
                SELECTED_PLACE_ID,
                true, true);

        return placeInfo;
    }

    public void setSelectedPlace(PlaceInfo placeInfo) {
        _dataStorage.setObject(
            SELECTED_PLACE_ID,
            true, true,
            placeInfo);
    }

    public Integer getActiveTabPage() {
        Integer tabPageId =
            _dataStorage.getObject(
                ACTIVE_TAB_PAGE_ID,
                true);

        return tabPageId;
    }

    public void setActiveTabPage(Integer tabPageId) {
        _dataStorage.setObject(
            ACTIVE_TAB_PAGE_ID,
            true,
            tabPageId);
    }

    public Boolean getDataBaseInitialized() {
        Boolean value =
            _dataStorage.getObject(
                DATA_BASE_INITIALIZED,
                true);

        return value;
    }

    public void setDataBaseInitialized(Boolean value) {
        _dataStorage.setObject(
            DATA_BASE_INITIALIZED,
            true,
            value);
    }
}
