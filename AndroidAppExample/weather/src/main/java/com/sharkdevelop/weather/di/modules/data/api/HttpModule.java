package com.sharkdevelop.weather.di.modules.data.api;

import android.annotation.SuppressLint;
import android.content.Context;

import com.github.nikolaymakhonin.common_di.BuildConfig;
import com.github.nikolaymakhonin.common_di.scopes.PerService;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.inject.Named;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;

@Module
public class HttpModule {

    @Provides
    @PerService
    public static Picasso getPicasso(Context applicationContext, OkHttpDownloader okHttpDownloader) {
        Picasso.Builder builder = new Picasso.Builder(applicationContext);
        builder.downloader(okHttpDownloader);
        Picasso instance = builder.build();
        instance.setIndicatorsEnabled(true);
        instance.setLoggingEnabled(true);
        return instance;
    }

    @Provides
    @PerService
    public static OkHttpDownloader getOkHttpDownloader(OkHttpClient okHttpClient) {
        return new OkHttpDownloader(okHttpClient);
    }

    @Provides
    public static okhttp3.OkHttpClient getOkHttpClient3(
        @Named("Unsafe") okhttp3.OkHttpClient unsafeClient,
        @Named("Safe") okhttp3.OkHttpClient safeClient)
    {
        if (BuildConfig.DEBUG) {
            return unsafeClient;
        } else  {
            return safeClient;
        }
    }

    @Provides
    public static OkHttpClient getOkHttpClient2(
        @Named("Unsafe") OkHttpClient unsafeClient,
        @Named("Safe") OkHttpClient safeClient)
    {
        if (BuildConfig.DEBUG) {
            return unsafeClient;
        } else  {
            return safeClient;
        }
    }

    @Provides
    @PerService
    @Named("Unsafe")
    @SuppressLint("TrustAllX509TrustManager")
    public static SSLSocketFactory getUnsafeSSLSocketFactory() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = {
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] chain, String authType) {
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] chain, String authType) {
                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[]{};
                    }
                }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            return sslSocketFactory;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Provides
    @PerService
    @Named("Unsafe")
    public static OkHttpClient getUnsafeOkHttpClient2() {
        final SSLSocketFactory sslSocketFactory = getUnsafeSSLSocketFactory();

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setSslSocketFactory(sslSocketFactory);
        okHttpClient.setHostnameVerifier((hostname, session) -> true);
        return okHttpClient;
    }

    @Provides
    @PerService
    @Named("Unsafe")
    public static okhttp3.OkHttpClient getUnsafeOkHttpClient3() {
        final SSLSocketFactory sslSocketFactory = getUnsafeSSLSocketFactory();

        okhttp3.OkHttpClient.Builder builder = new okhttp3.OkHttpClient.Builder();
        builder.sslSocketFactory(sslSocketFactory);
        builder.hostnameVerifier((hostname, session) -> true);

        okhttp3.OkHttpClient okHttpClient = builder.build();
        return okHttpClient;
    }

    @Provides
    @Named("Safe")
    public static OkHttpClient getSafeOkHttpClient2() {
        return new OkHttpClient();
    }

    @Provides
    @Named("Safe")
    public static okhttp3.OkHttpClient getSafeOkHttpClient3() {
        okhttp3.OkHttpClient.Builder builder = new okhttp3.OkHttpClient.Builder();
        okhttp3.OkHttpClient okHttpClient = builder.build();
        return okHttpClient;
    }
}
