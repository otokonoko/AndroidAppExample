package com.sharkdevelop.weather;

import android.support.multidex.MultiDexApplication;

import com.github.nikolaymakhonin.common_di.contracts.IHasAppComponentBase;
import com.github.nikolaymakhonin.logger.Log;
import com.sharkdevelop.weather.di.components.AppComponent;
import com.sharkdevelop.weather.di.factories.ComponentsFactory;

public class App extends MultiDexApplication implements IHasAppComponentBase<AppComponent> {

    //region DI

    private AppComponent _appComponent;

    @Override
    public AppComponent getAppComponent() {
        return _appComponent;
    }

    //endregion

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d("App", "onCreate");

        Thread.setDefaultUncaughtExceptionHandler(Log.uncaughtExceptionHandler);

        _appComponent = ComponentsFactory.buildAppComponent(this);

//        _appComponent.initFabric();
    }
}
