package com.sharkdevelop.weather.di.modules.data.api;

import com.github.nikolaymakhonin.common_di.scopes.PerService;
import com.github.nikolaymakhonin.utils.data.apis.converters.ToStringConverterFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.sharkdevelop.weather.data.apis.YahooWeatherApi;
import com.sharkdevelop.weather.data.apis.YahooWeatherApiBase;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.DateTimeParser;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Named;

import br.com.zbra.androidlinq.Linq;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class YahooWeatherModule {

    private static final String LOG_TAG = "YahooWeatherModule";

    @Provides
    @PerService
    @Named("YahooWeatherGson")
    public static Gson getYahooWeatherApiGson() {

        //Docs: http://joda-time.sourceforge.net/apidocs/org/joda/time/format/DateTimeFormat.html
        String[] datePatterns = {
            //Fri, 17 Jun 2016 12:14 AM AKDT
            "EEE, dd MMM yyyy hh:mm a z",
            //            "EEE, dd MMM yyyy hh:mm a Z",
            //17 Jun 2016
            "dd MMM yyyy",
            //2016-06-17T08:14:22Z
            "yyyy-MM-dd'T'HH:mm:ss", "yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyy-MM-dd'T'HH:mm:ss'Z'z",
            "yyyy-MM-dd'T'HH:mm:ss'Z'Z"
        };

        Map<String, DateTimeZone> timeZones = Linq.stream(DateTimeUtils.getDefaultTimeZoneNames())
                                                  .toMap(o -> o.getKey(), o -> o.getValue());

        DateTime now              = new DateTime();
        DateTime timeZoneInstant1 = new DateTime(2007, 1, 1, 0, 0);
        DateTime timeZoneInstant2 = timeZoneInstant1.minusMonths(6);

        //Add support parse all time zones
        for (String timeZoneId : DateTimeZone.getAvailableIDs()) {
            //            TimeZone.getTimeZone(timeZoneId).getID()
            DateTimeZone dateTimeZone = DateTimeZone.forID(timeZoneId);
            String[] timeZoneKeys = {
                dateTimeZone.getNameKey(timeZoneInstant1.getMillis()),
                dateTimeZone.getNameKey(timeZoneInstant2.getMillis())
            };
            for (String timeZoneKey : timeZoneKeys) {
                if (timeZones.containsKey(timeZoneKey)) {
                    continue;
                }
                if (timeZoneKey.indexOf('-') >= 0 || timeZoneKey.indexOf('+') >= 0) {
                    continue;
                }
                timeZones.put(timeZoneKey, dateTimeZone);
            }
        }

        DateTimeUtils.setDefaultTimeZoneNames(timeZones);

        List<DateTimeParser> dateTimeParsers = Linq.stream(datePatterns)
                                                   .select(pattern -> DateTimeFormat.forPattern(pattern).getParser())
                                                   .toList();

        DateTimeParser[] parsers = dateTimeParsers.toArray(new DateTimeParser[dateTimeParsers.size()]);

        DateTimeFormatter dateTimeFormatter = new DateTimeFormatterBuilder()
            .append(null, parsers).toFormatter()
            .withLocale(Locale.US);

        // Creates the json object which will manage the information received
        GsonBuilder builder = new GsonBuilder()
            .registerTypeAdapter(Date.class, (JsonDeserializer<Date>) (json, typeOfT, context) -> {
                String dateAsString = json.getAsJsonPrimitive().getAsString();

                DateTime date = dateTimeFormatter.parseDateTime(dateAsString);

                return date.toDate();
            });

        // Register an adapter to manage the date types as long values
        //see: http://stackoverflow.com/questions/5671373/unparseable-date-1302828677828-trying-to-deserialize-with-gson-a-millisecond
        builder.registerTypeAdapterFactory(new ListTypeAdapterFactory(builder.create()));

        Gson gson = builder.create();

        return gson;
    }

    @Provides
    @PerService
    @Named("YahooWeatherRetrofit")
    public static Retrofit getYahooWeatherRetrofit(@Named("YahooWeatherGson") Gson gson, OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://query.yahooapis.com/").client(okHttpClient)
                                                  .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                                                  .addConverterFactory(new ToStringConverterFactory())
                                                  .addConverterFactory(GsonConverterFactory.create(gson)).build();

        return retrofit;
    }

    @Provides
    @PerService
    public static YahooWeatherApiBase getYahooWeatherApiBase(@Named("YahooWeatherRetrofit") Retrofit retrofit) {
        YahooWeatherApiBase api = retrofit.create(YahooWeatherApiBase.class);
        return api;
    }

    @Provides
    @PerService
    public static YahooWeatherApi getYahooWeatherApi(YahooWeatherApiBase baseApi) {
        return new YahooWeatherApi(baseApi);
    }

}
