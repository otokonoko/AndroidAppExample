package com.sharkdevelop.weather.presentation.common;

import com.sharkdevelop.weather.presentation.tab_pager.data.TabPageIds;

import org.apache.commons.lang3.tuple.Pair;

import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;

public class EventBusCommon {

    //region CurrentActiveTabPage

    private final Subject<Integer, Integer> _doSetActiveTabPageSubject = PublishSubject.create();

    /** see: {@link TabPageIds} */
    public Subject<Integer, Integer> doSetActiveTabPage() {
        return _doSetActiveTabPageSubject;
    }

    private final Subject<Integer, Integer> _onSetActiveTabPageSubject = BehaviorSubject.create(0);

    /** see: {@link TabPageIds} */
    public Subject<Integer, Integer> onSetActiveTabPageBehaviour() {
        return _onSetActiveTabPageSubject;
    }

    //endregion

    private final Subject<Pair<Integer, String>, Pair<Integer, String>> _doSetTabPageNameSubject = PublishSubject.create();

    /** subject of [tabPageId, tabPageName] */
    public Subject<Pair<Integer, String>, Pair<Integer, String>> doSetTabPageName() {
        return _doSetTabPageNameSubject;
    }
}
