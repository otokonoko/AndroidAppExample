package com.sharkdevelop.weather.presentation.weather.data;

import com.github.nikolaymakhonin.utils.CompareUtils;
import com.github.nikolaymakhonin.utils.StreamUtils;
import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.BaseViewModel;
import com.github.nikolaymakhonin.utils.serialization.BinaryReader;
import com.github.nikolaymakhonin.utils.serialization.BinaryWriter;
import com.github.nikolaymakhonin.utils.strings.StringUtilsExt;
import com.github.nikolaymakhonin.utils.time.DateTime;

public class Place extends BaseViewModel {

    //region Properties

    //region CityName

    private String _cityName;

    public String getCityName() {
        return _cityName;
    }

    public void setCityName(String value) {
        if (CompareUtils.Equals(_cityName, value)) {
            return;
        }
        _cityName = value;
        Modified().onNext(null);
    }

    //endregion

    //region CountryName

    private String _countryName;

    public String getCountryName() {
        return _countryName;
    }

    public void setCountryName(String value) {
        if (CompareUtils.Equals(_countryName, value)) {
            return;
        }
        _countryName = value;
        Modified().onNext(null);
    }

    //endregion

    //region StateName

    private String _regionName;

    public String getRegionName() {
        return _regionName;
    }

    public void setRegionName(String value) {
        if (CompareUtils.Equals(_regionName, value)) {
            return;
        }
        _regionName = value;
        Modified().onNext(null);
    }

    //endregion

    //region Id

    private int _id;

    public int getId() {
        return _id;
    }

    public void setId(int value) {
        if (CompareUtils.Equals(_id, value)) {
            return;
        }
        _id = value;
        Modified().onNext(null);
    }

    //endregion

    //region ActualDate

    private DateTime _actualDate;

    public DateTime getActualDate() {
        return _actualDate;
    }

    public void setActualDate(DateTime value) {
        if (CompareUtils.Equals(_actualDate, value)) {
            return;
        }
        _actualDate = value;
        Modified().onNext(null);
    }

    //endregion

    //endregion

    //region Calculated Properties

    public String getPlaceName() {
        StringBuilder sb = new StringBuilder();

        if (!StringUtilsExt.isNullOrEmpty(_cityName)) {
            sb.append(_cityName);
        } else if (!StringUtilsExt.isNullOrEmpty(_regionName)) {
            sb.append(_regionName);
        }

        if (!StringUtilsExt.isNullOrEmpty(_countryName)) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(_countryName);
        }

        return sb.toString();
    }

    //endregion

    //region Serialization

    private static final int _currentVersion = 0;

    @Override
    public void Serialize(BinaryWriter writer) throws Exception {
        writer.write(_currentVersion);

        writer.WriteNullable(_actualDate);
        writer.WriteNullable(_cityName);
        writer.WriteNullable(_countryName);
        writer.WriteNullable(_regionName);
        writer.write(_id);
    }

    @Override
    public Object DeSerialize(BinaryReader reader) throws Exception {
        //noinspection UnusedAssignment
        int version = reader.readInt();

        setActualDate(reader.ReadNullableDateTime());
        setCityName(reader.ReadNullableString());
        setCountryName(reader.ReadNullableString());
        setRegionName(reader.ReadNullableString());
        setId(reader.readInt());

        return this;
    }

    //endregion
}

