package com.sharkdevelop.weather.presentation.weather.views;

import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.IView;
import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.IViewModelView;
import com.sharkdevelop.weather.presentation.weather.data.PlaceInfo;
import com.sharkdevelop.weather.presentation.weather.presenters.PlaceInfoListItemPresenter;

import rx.Observable;

public interface IPlaceInfoAddView extends IView {

    boolean isAvailable();

    void setAvailable(boolean value);

    /** observable of search query */
    Observable<String> doAddPlace();
}
