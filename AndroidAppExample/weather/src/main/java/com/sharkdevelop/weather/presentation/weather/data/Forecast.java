package com.sharkdevelop.weather.presentation.weather.data;

import android.annotation.SuppressLint;

import com.github.nikolaymakhonin.utils.CompareUtils;
import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.BaseViewModel;
import com.github.nikolaymakhonin.utils.serialization.BinaryReader;
import com.github.nikolaymakhonin.utils.serialization.BinaryWriter;
import com.github.nikolaymakhonin.utils.time.DateTime;
import com.sharkdevelop.weather.presentation.weather.helpers.StringConverters;

import org.joda.time.format.DateTimeFormat;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class Forecast extends BaseViewModel {

    //region Properties

    //region Date

    private DateTime _date;

    public DateTime getDate() {
        return _date;
    }

    public void setDate(DateTime value) {
        if (CompareUtils.Equals(_date, value)) {
            return;
        }
        _date = value;
        Modified().onNext(null);
    }

    //endregion

    //region Cloudiness

    private String _cloudiness;

    public String getCloudiness() {
        return _cloudiness;
    }

    public void setCloudiness(String value) {
        if (CompareUtils.Equals(_cloudiness, value)) {
            return;
        }
        _cloudiness = value;
        Modified().onNext(null);
    }

    //endregion

    //region TemperatureMax

    private float _temperatureMax;

    public float getTemperatureMax() {
        return _temperatureMax;
    }

    public void setTemperatureMax(float value) {
        if (CompareUtils.Equals(_temperatureMax, value)) {
            return;
        }
        _temperatureMax = value;
        Modified().onNext(null);
    }

    //endregion

    //region TemperatureMin

    private float _temperatureMin;

    public float getTemperatureMin() {
        return _temperatureMin;
    }

    public void setTemperatureMin(float value) {
        if (CompareUtils.Equals(_temperatureMin, value)) {
            return;
        }
        _temperatureMin = value;
        Modified().onNext(null);
    }

    //endregion

    //endregion

    //region Calculated Properties

    public String getDateString() {
        return StringConverters.shortDateToString(_date);
    }

    public String getTemperaturesString() {
        return StringConverters.temperatureToString(_temperatureMin, _temperatureMax);
    }

    //endregion

    //region Serialization

    private static final int _currentVersion = 0;

    @Override
    public void Serialize(BinaryWriter writer) throws Exception {
        writer.write(_currentVersion);

        writer.WriteNullable(_date);
        writer.WriteNullable(_cloudiness);
        writer.write(_temperatureMax);
        writer.write(_temperatureMin);
    }

    @Override
    public Object DeSerialize(BinaryReader reader) throws Exception {
        //noinspection UnusedAssignment
        int version = reader.readInt();

        setDate(reader.ReadNullableDateTime());
        setCloudiness(reader.ReadNullableString());
        setTemperatureMax(reader.readFloat());
        setTemperatureMin(reader.readFloat());

        return this;
    }

    //endregion
}
