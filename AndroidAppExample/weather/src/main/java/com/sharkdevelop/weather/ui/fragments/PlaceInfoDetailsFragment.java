package com.sharkdevelop.weather.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.sharkdevelop.weather.R;
import com.sharkdevelop.weather.ui.contracts.IFragmentWithHeader;

/** Docs: <a href="https://developers.google.com/places/android-api/start?hl=ru#connect-client">developers.google.com introduce</a> */

/** Docs: <a href="https://developers.google.com/android/guides/api-client#handle_connection_failures">developers.google.com details</a> */
public class PlaceInfoDetailsFragment extends Fragment implements IFragmentWithHeader
{

    private static final int LAYOUT          = R.layout.fragment_place_info_details;
    private static final int HEADER_COLOR    = R.color.toolBarForBackground7;
    private static final int HEADER_DRAWABLE = R.drawable.navigation_header_background_7;

    private static final String LOG_TAG = "PlaceInfoDetailsFragment";

    private static final String BUNDLE_KEY_MAP_STATE = "mapData";

    private View                 _contentView;
    private ObservableScrollView _scrollView;

    //region Override methods

    @Override
    public int getHeaderColorResId() {
        return HEADER_COLOR;
    }

    @Override
    public int getHeaderDrawableResId() {
        return HEADER_DRAWABLE;
    }

    //endregion

    //region Create Instance

    public static PlaceInfoDetailsFragment getInstance() {
        Bundle                   args     = new Bundle();
        PlaceInfoDetailsFragment fragment = new PlaceInfoDetailsFragment();
        fragment.setArguments(args);

        return fragment;
    }

    //endregion

    //region Init Controls

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState)
    {
        _contentView = inflater.inflate(LAYOUT, container, false);
        initControls(savedInstanceState);
//        initGoogleApiClient();

        return _contentView;
    }

    private void initControls(@Nullable Bundle savedInstanceState) {
        _scrollView = (ObservableScrollView) _contentView.findViewById(R.id.scrollView);

        initScrollView();
    }

    private void initScrollView() {
        MaterialViewPagerHelper.registerScrollView(getActivity(), _scrollView, null);
    }

    //endregion

    //region Life cycle

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    //endregion
}
