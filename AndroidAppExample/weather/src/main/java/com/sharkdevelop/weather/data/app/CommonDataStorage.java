package com.sharkdevelop.weather.data.app;

import com.sharkdevelop.weather.data.common.DataStorageBase;

import java.util.UUID;

public class CommonDataStorage {

    private final DataStorageBase _dataStorage;

    private static UUID ACTIVE_TAB_PAGE_ID = UUID.fromString("b9c9b6fd-eedb-46a0-9ff5-7b1f5e766736");

    public CommonDataStorage(DataStorageBase dataStorage) {
        _dataStorage = dataStorage;
    }

    public Integer getActiveTabPage() {
        Integer tabPageId =
            _dataStorage.getObject(
                ACTIVE_TAB_PAGE_ID,
                true);

        return tabPageId;
    }

    public void setActiveTabPage(Integer tabPageId) {
        _dataStorage.setObject(
            ACTIVE_TAB_PAGE_ID,
            true,
            tabPageId);
    }
}
