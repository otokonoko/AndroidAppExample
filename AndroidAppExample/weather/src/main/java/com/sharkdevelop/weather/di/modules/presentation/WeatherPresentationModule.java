package com.sharkdevelop.weather.di.modules.presentation;

import com.github.nikolaymakhonin.common_di.scopes.PerApplication;
import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.IRecyclerViewAdapterFactory;
import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.IViewModelPresenter;
import com.sharkdevelop.weather.data.apis.YahooWeatherApi;
import com.sharkdevelop.weather.data.app.weather.WeatherDataStorage;
import com.sharkdevelop.weather.di.factories.weather.PlaceInfoListFactory;
import com.sharkdevelop.weather.presentation.common.EventBusCommon;
import com.sharkdevelop.weather.presentation.weather.data.PlaceInfo;
import com.sharkdevelop.weather.presentation.weather.presenters.EventBusWeather;
import com.sharkdevelop.weather.presentation.weather.presenters.PlaceInfoAddPresenter;
import com.sharkdevelop.weather.presentation.weather.presenters.PlaceInfoDetailsPresenter;
import com.sharkdevelop.weather.presentation.weather.presenters.PlaceInfoListAdapter;
import com.sharkdevelop.weather.presentation.weather.views.IPlaceInfoListItemView;

import dagger.Module;
import dagger.Provides;

@Module()
public class WeatherPresentationModule {

    @Provides
    public static PlaceInfoListAdapter createPlaceInfoListAdapter(
        IRecyclerViewAdapterFactory<IPlaceInfoListItemView, PlaceInfo, IViewModelPresenter<IPlaceInfoListItemView, PlaceInfo>> factory,
        YahooWeatherApi yahooWeatherApi, EventBusWeather eventBusWeather, WeatherDataStorage weatherDataStorage)
    {
        return new PlaceInfoListAdapter(factory, yahooWeatherApi, eventBusWeather, weatherDataStorage, context);
    }

    @Provides
    public static IRecyclerViewAdapterFactory<IPlaceInfoListItemView, PlaceInfo, IViewModelPresenter<IPlaceInfoListItemView,
        PlaceInfo>> createPlaceInfoListFactory(EventBusCommon eventBusCommon, EventBusWeather eventBusWeather) {
        return new PlaceInfoListFactory(eventBusCommon, eventBusWeather);
    }

    @Provides
    @PerApplication
    public EventBusWeather getEventBusWeather() {
        return new EventBusWeather();
    }

    @Provides
    public static PlaceInfoDetailsPresenter createPlaceInfoDetailsPresenter(EventBusWeather eventBusWeather, WeatherDataStorage weatherDataStorage)
    {
        return new PlaceInfoDetailsPresenter(eventBusWeather, weatherDataStorage);
    }

    @Provides
    public static PlaceInfoAddPresenter createPlaceInfoAddPresenter(EventBusCommon eventBusCommon, EventBusWeather eventBusWeather)
    {
        return new PlaceInfoAddPresenter(eventBusCommon, eventBusWeather);
    }

}
