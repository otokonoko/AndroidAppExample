package com.sharkdevelop.weather.data.apis.dto;

/*
Generated from:
http://developer.yahoo.com/yql/console/?q=select%20*%20from%20geo.places(1)%20where%20text%3D%22nome%2C%20ak%22&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys
by jsonschema2pojo plugin

{"query": {
    "count": 1,
    "created": "2016-06-17T01:48:28Z",
    "lang": "en-US",
    "results": {"place": {
        "lang": "en-US",
        "uri": "http://where.yahooapis.com/v1/place/2460286",
        "woeid": "2460286",
        "placeTypeName": {
            "code": "7",
            "content": "Town"
        },
        "name": "Nome",
        "country": {
            "code": "US",
            "type": "Country",
            "woeid": "23424977",
            "content": "United States"
        },
        "admin1": {
            "code": "US-AK",
            "type": "State",
            "woeid": "2347560",
            "content": "Alaska"
        },
        "admin2": {
            "code": "",
            "type": "County",
            "woeid": "12587569",
            "content": "Nome Census Area"
        },
        "admin3": {
            "code": "",
            "type": "County",
            "woeid": "12587569",
            "content": "Nome Census Area"
        },
        "locality1": {
            "code": "",
            "type": "Town",
            "woeid": "2460286",
            "content": "Nome"
        },
        "locality2": {
            "code": "",
            "type": "Town",
            "woeid": "2460286",
            "content": "Nome"
        },
        "postal": {
            "type": "Zip Code",
            "woeid": "12799801",
            "content": "99762"
        },
        "centroid": {
            "latitude": "64.499474",
            "longitude": "-165.405792"
        },
        "boundingBox": {
            "southWest": {
                "latitude": "64.487633",
                "longitude": "-165.501526"
            },
            "northEast": {
                "latitude": "64.561073",
                "longitude": "-165.325775"
            }
        },
        "areaRank": "1",
        "popRank": "1",
        "timezone": {
            "type": "Time Zone",
            "woeid": "56043670",
            "content": "America/Nome"
        }
    }}
}}
 */

import com.google.gson.annotations.SerializedName;

import java.net.URI;
import java.util.List;

public class YahooGeoPlaceResponse extends YahooResponse<YahooGeoPlaceResponse.Results> {

    public static class Results {
        @SerializedName("place")
        public List<Place> place;

        public static class Place {
            @SerializedName("lang")
            public String        lang;
            @SerializedName("uri")
            public URI           uri;
            @SerializedName("woeid")
            public int           woeid;
            @SerializedName("placeTypeName")
            public PlaceTypeName placeTypeName;
            @SerializedName("name")
            public String        name;
            @SerializedName("country")
            public PlaceInfo     country;
            @SerializedName("admin1")
            public PlaceInfo     admin1;
            @SerializedName("admin2")
            public PlaceInfo     admin2;
            @SerializedName("admin3")
            public PlaceInfo     admin3;
            @SerializedName("locality1")
            public PlaceInfo     locality1;
            @SerializedName("locality2")
            public PlaceInfo     locality2;
            @SerializedName("postal")
            public PlaceInfo     postal;
            @SerializedName("centroid")
            public Centroid      centroid;
            @SerializedName("boundingBox")
            public BoundingBox   boundingBox;
            @SerializedName("areaRank")
            public int           areaRank;
            @SerializedName("popRank")
            public int           popRank;
            @SerializedName("timezone")
            public PlaceInfo     timezone;

            public static class PlaceTypeName {
                @SerializedName("code")
                public int    code;
                @SerializedName("content")
                public String content;
            }

            public enum PlaceInfoType {
                @SerializedName("Town")
                Town,

                @SerializedName("Country")
                Country,

                @SerializedName("State")
                State,

                @SerializedName("Time Zone")
                TimeZone,

                @SerializedName("County")
                County,

                @SerializedName("District")
                District,

                @SerializedName("Province")
                Province,

                @SerializedName("Department")
                Department
            }

            public static class PlaceInfo {
                @SerializedName("code")
                public String        code;
                @SerializedName("type")
                public PlaceInfoType type;
                @SerializedName("woeid")
                public int           woeid;
                @SerializedName("content")
                public String        content;
            }

            public static class Centroid {
                @SerializedName("latitude")
                public float latitude;
                @SerializedName("longitude")
                public float longitude;
            }

            public static class BoundingBox {
                @SerializedName("southWest")
                public Centroid southWest;
                @SerializedName("northEast")
                public Centroid northEast;
            }
        }
    }
}
