package com.sharkdevelop.weather.presentation.tab_pager.presenters;

import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.ViewPresenter;
import com.sharkdevelop.weather.data.app.CommonDataStorage;
import com.sharkdevelop.weather.data.app.weather.WeatherDataStorage;
import com.sharkdevelop.weather.presentation.common.EventBusCommon;
import com.sharkdevelop.weather.presentation.tab_pager.views.ITabPagerView;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class TabPagerPresenter extends ViewPresenter<ITabPagerView> {

    private final EventBusCommon    _eventBusCommon;
    private final CommonDataStorage _commonDataStorage;

    public TabPagerPresenter(EventBusCommon eventBusCommon, CommonDataStorage commonDataStorage) {
        _eventBusCommon = eventBusCommon;
        _commonDataStorage = commonDataStorage;

        _eventBusCommon
            .doSetActiveTabPage()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(tabPageId -> {
                _commonDataStorage.setActiveTabPage(tabPageId);
                setActiveTabPage(tabPageId);
            });
    }

    private void setActiveTabPage(Integer tabPageId) {
        ITabPagerView view = getView();
        if (isViewBind(true, view)) {
            view.setActiveTabPage(tabPageId);
            _eventBusCommon.onSetActiveTabPageBehaviour().onNext(tabPageId);
        }
    }

    private Subscription _onSetActivePageSubscription;

    @Override
    protected void bindView() {
        super.bindView();

        Integer activeTabPage = _commonDataStorage.getActiveTabPage();
        if (activeTabPage != null) {
            setActiveTabPage(activeTabPage);
        }

        _onSetActivePageSubscription = getView()
            .onSetActivePageBehaviour()
            .observeOn(Schedulers.computation())
            .subscribe(tabPageId -> {
                _commonDataStorage.setActiveTabPage(tabPageId);
                _eventBusCommon.onSetActiveTabPageBehaviour().onNext(tabPageId);
            });
    }

    @Override
    protected void unBindView() {
        super.unBindView();

        if (_onSetActivePageSubscription != null) {
            _onSetActivePageSubscription.unsubscribe();
            _onSetActivePageSubscription = null;
        }
    }
}
