package com.sharkdevelop.weather.data.apis;

import com.sharkdevelop.weather.data.apis.dto.YahooGeoPlaceResponse;
import com.sharkdevelop.weather.data.apis.dto.YahooWeatherForecastResponse;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

//Client ID:  dj0yJmk9MlV0Zmp5SUE5RUhMJmQ9WVdrOVEwVlpWRzFNTkdjbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0yYg--
//Client Secret:  64c47f22849b40759add3f572c1a433f08fc9986
//https://query.yahooapis.com/v1/public/yql
// ?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22nome%2C%20ak%22)
// &format=json
// &env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys

//docs: http://square.github.io/retrofit/
/** BaseUrl: <a href="https://query.yahooapis.com/">https://query.yahooapis.com/</a> */
public interface YahooWeatherApiBase {

    @GET("v1/public/yql?format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys")
    Observable<String> getWeatherForecastAsString(@Query("q") String query);

    @GET("v1/public/yql?format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys")
    Observable<String> getGeoPlacesAsString(@Query("q") String query);

    @GET("v1/public/yql?format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys")
    Observable<YahooWeatherForecastResponse> getWeatherForecast(@Query("q") String query);

    @GET("v1/public/yql?format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys")
    Observable<YahooGeoPlaceResponse> getGeoPlaces(@Query("q") String query);

}
