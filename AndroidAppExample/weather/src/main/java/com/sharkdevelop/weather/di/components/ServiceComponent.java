package com.sharkdevelop.weather.di.components;

import com.github.nikolaymakhonin.common_di.components.ServiceComponentBase;
import com.github.nikolaymakhonin.common_di.scopes.PerService;
import com.sharkdevelop.weather.data.apis.YahooWeatherApi;
import com.sharkdevelop.weather.data.app.CommonDataStorage;
import com.sharkdevelop.weather.data.app.weather.WeatherDataStorage;
import com.sharkdevelop.weather.di.modules.ServiceModule;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import dagger.Component;

@PerService
@Component(modules = { ServiceModule.class })
public interface ServiceComponent extends ServiceComponentBase
{
    YahooWeatherApi getYahooWeatherApi();
    OkHttpDownloader getOkHttpDownloader();
    Picasso getPicasso();
    WeatherDataStorage getWeatherDataStorage();
    CommonDataStorage getCommonDataStorage();
}
