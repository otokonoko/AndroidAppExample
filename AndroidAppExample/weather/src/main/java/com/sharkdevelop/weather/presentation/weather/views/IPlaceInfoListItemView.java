package com.sharkdevelop.weather.presentation.weather.views;

import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.IViewModelView;
import com.sharkdevelop.weather.presentation.weather.data.PlaceInfo;

import rx.Observable;

public interface IPlaceInfoListItemView extends IViewModelView<PlaceInfo> {

    Observable onClick();

}
