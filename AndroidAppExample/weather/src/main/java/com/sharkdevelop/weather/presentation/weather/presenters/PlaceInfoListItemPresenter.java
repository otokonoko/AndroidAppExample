package com.sharkdevelop.weather.presentation.weather.presenters;

import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.ViewModelPresenter;
import com.sharkdevelop.weather.presentation.common.EventBusCommon;
import com.sharkdevelop.weather.presentation.tab_pager.data.TabPageIds;
import com.sharkdevelop.weather.presentation.weather.data.PlaceInfo;
import com.sharkdevelop.weather.presentation.weather.views.IPlaceInfoListItemView;

import org.apache.commons.lang3.tuple.Pair;

import rx.Subscription;
import rx.schedulers.Schedulers;

public class PlaceInfoListItemPresenter extends ViewModelPresenter<IPlaceInfoListItemView, PlaceInfo> {

    private final EventBusCommon  _eventBusCommon;
    private final EventBusWeather _eventBusWeather;

    public PlaceInfoListItemPresenter(EventBusCommon eventBusCommon, EventBusWeather eventBusWeather) {
        _eventBusCommon = eventBusCommon;
        _eventBusWeather = eventBusWeather;
    }

    private void onClickHandler() {
        PlaceInfo viewModel = getViewModel();

        if (viewModel == null) {
            return;
        }

        _eventBusCommon.doSetActiveTabPage().onNext(TabPageIds.PlaceInfoDetails);
        _eventBusCommon.doSetTabPageName().onNext(
            Pair.of(TabPageIds.PlaceInfoDetails, viewModel.getPlace().getPlaceName())
        );
        _eventBusWeather.onSelectPlace().onNext(viewModel);
    }

    private Subscription _onClickSubscription;

    @Override
    protected void bindView() {
        super.bindView();

        _onClickSubscription = getView()
            .onClick()
            .observeOn(Schedulers.computation())
            .subscribe(o -> onClickHandler());
    }

    @Override
    protected void unBindView() {
        super.unBindView();

        if (_onClickSubscription != null) {
            _onClickSubscription.unsubscribe();
            _onClickSubscription = null;
        }
    }
}
