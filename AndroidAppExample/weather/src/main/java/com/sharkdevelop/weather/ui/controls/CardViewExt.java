package com.sharkdevelop.weather.ui.controls;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.support.v7.cardview.R;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;

import com.github.nikolaymakhonin.utils.CompareUtils;

public class CardViewExt extends CardView {

    private ColorStateList _backgroundColorStateList;
    private Integer _cardBackgroundColor;

    public CardViewExt(Context context) {
        super(context);
        init(context, null, 0);
    }

    public CardViewExt(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public CardViewExt(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CardView, defStyleAttr,
            R.style.CardView);

        if (a.hasValue(R.styleable.CardView_cardBackgroundColor)) {
            _backgroundColorStateList = a.getColorStateList(R.styleable.CardView_cardBackgroundColor);
        }

        a.recycle();
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (_backgroundColorStateList != null) {
            int defaultColor = _cardBackgroundColor != null
                ? _cardBackgroundColor
                : _backgroundColorStateList.getDefaultColor();
            int backgroundColor = _backgroundColorStateList.getColorForState(getDrawableState(), defaultColor);
            setCardBackgroundColor(backgroundColor);
        }
    }

    @Override
    public void setCardBackgroundColor(int color) {
        if (CompareUtils.Equals(_cardBackgroundColor, color)) {
            return;
        }
        super.setCardBackgroundColor(color);
        _cardBackgroundColor = color;
    }
}
