package com.sharkdevelop.weather.data.common;

import java.util.Hashtable;
import java.util.UUID;

import rx.functions.Func0;

public class MemoryDataStorage extends DataStorageBase {

    private final Hashtable<UUID, Object>  _dataBase        = new Hashtable<>();

    //region Save/Load from database

    @Override
    public <T> T loadFromDataBase(UUID id, Class<T> type, Func0<T> constructor) {
        synchronized (_locker) {
            return (T) _dataBase.get(id);
        }
    }

    @Override
    public boolean saveToDataBase(UUID id, Object object) {
        synchronized (_locker) {
            _dataBase.put(id, object);
            return true;
        }
    }

    @Override
    public boolean removeFromDataBase(UUID id) {
        synchronized (_locker) {
            _dataBase.remove(id);
            return true;
        }
    }

    //endregion

}

