package com.sharkdevelop.weather.presentation.weather.presenters;

import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.ViewModelPresenter;
import com.sharkdevelop.weather.data.app.weather.WeatherDataStorage;
import com.sharkdevelop.weather.presentation.weather.data.PlaceInfo;
import com.sharkdevelop.weather.presentation.weather.views.IPlaceInfoView;

import rx.Subscription;
import rx.schedulers.Schedulers;

public class PlaceInfoDetailsPresenter extends ViewModelPresenter<IPlaceInfoView, PlaceInfo> {

    private final EventBusWeather _eventBusWeather;
    private final WeatherDataStorage _weatherDataStorage;
    private       Subscription       _onSelectPlaceSubscription;

    public PlaceInfoDetailsPresenter(EventBusWeather eventBusWeather, WeatherDataStorage weatherDataStorage) {
        _eventBusWeather = eventBusWeather;
        _weatherDataStorage = weatherDataStorage;

        setViewModel(_weatherDataStorage.getSelectedPlace());

        _onSelectPlaceSubscription = _eventBusWeather
            .onSelectPlace()
            .observeOn(Schedulers.computation())
            .subscribe(viewModel -> {
                _weatherDataStorage.setSelectedPlace(viewModel);
                setViewModel(viewModel);
            });
    }

    @Override
    public void dispose() {
        super.dispose();

        _onSelectPlaceSubscription.unsubscribe();
    }
}
