package com.sharkdevelop.weather.presentation.weather.presenters;

import com.sharkdevelop.weather.presentation.tab_pager.data.TabPageIds;
import com.sharkdevelop.weather.presentation.weather.data.PlaceInfo;

import org.apache.commons.lang3.tuple.Pair;

import rx.subjects.PublishSubject;
import rx.subjects.Subject;

public class EventBusWeather {

    private final Subject<PlaceInfo, PlaceInfo> _onSelectPlace = PublishSubject.create();

    public Subject<PlaceInfo, PlaceInfo> onSelectPlace() {
        return _onSelectPlace;
    }

    private final Subject<String, String> _doAddPlaceSubject = PublishSubject.create();

    /** see: {@link TabPageIds} */
    public Subject<String, String> doAddPlace() {
        return _doAddPlaceSubject;
    }

}
