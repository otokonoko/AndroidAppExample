package com.sharkdevelop.weather.presentation.weather.data;

import com.github.nikolaymakhonin.utils.CompareUtils;
import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.BaseViewModel;
import com.github.nikolaymakhonin.utils.serialization.BinaryReader;
import com.github.nikolaymakhonin.utils.serialization.BinaryWriter;

import rx.functions.Action0;

public class PlaceInfo extends BaseViewModel {

    //region Properties

    //region Place

    private Place _place;

    private Action0 _placeUnBindFunc;

    public Place   getPlace() {
        return _place;
    }

    public void setPlace(Place value) {
        if (CompareUtils.EqualsObjects(_place, value)) {
            return;
        }
        synchronized (_propertySetLocker) {
            if (_placeUnBindFunc != null) {
                _placeUnBindFunc.call();
                _placeUnBindFunc = null;
            }
            _place = value;
            if (_place != null) {
                _placeUnBindFunc = _treeModifiedMerger.attach(_place.TreeModified());
            }
        }
        Modified().onNext(null);
    }

    //endregion

    //region Weather

    private Weather _weather;

    private Action0 _weatherUnBindFunc;

    public Weather   getWeather() {
        return _weather;
    }

    public void setWeather(Weather value) {
        if (CompareUtils.EqualsObjects(_weather, value)) {
            return;
        }
        synchronized (_propertySetLocker) {
            if (_weatherUnBindFunc != null) {
                _weatherUnBindFunc.call();
                _weatherUnBindFunc = null;
            }
            _weather = value;
            if (_weather != null) {
                _weatherUnBindFunc = _treeModifiedMerger.attach(_weather.TreeModified());
            }
        }
        Modified().onNext(null);
    }

    //endregion

    //endregion

    //region Serialization

    private static final int _currentVersion = 0;

    @Override
    public void Serialize(BinaryWriter writer) throws Exception {
        writer.write(_currentVersion);

        writer.WriteNullable(_place, (w, o) -> o.Serialize(w));
        writer.WriteNullable(_weather, (w, o) -> o.Serialize(w));
    }

    @Override
    public Object DeSerialize(BinaryReader reader) throws Exception {
        //noinspection UnusedAssignment
        int version = reader.readInt();

        setPlace(reader.ReadNullable(r -> (Place)new Place().DeSerialize(r)));
        setWeather(reader.ReadNullable(r -> (Weather)new Weather().DeSerialize(r)));

        return this;
    }

    //endregion
}
