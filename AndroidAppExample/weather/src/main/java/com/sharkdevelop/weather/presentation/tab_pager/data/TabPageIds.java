package com.sharkdevelop.weather.presentation.tab_pager.data;

public class TabPageIds {

    public static final int PlaceInfoList = 0;

    public static final int PlaceInfoDetails = 1;

}
