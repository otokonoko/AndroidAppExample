package com.sharkdevelop.weather.di.modules.data.storage;

import android.content.Context;

import com.github.nikolaymakhonin.common_di.scopes.PerService;
import com.github.nikolaymakhonin.utils.lists.list.SortedList;
import com.github.nikolaymakhonin.utils.rx.CollectionModifiedMerger;
import com.github.nikolaymakhonin.utils.serialization.WriteItemAction;
import com.sharkdevelop.weather.R;
import com.sharkdevelop.weather.data.app.weather.WeatherDataStorage;
import com.sharkdevelop.weather.data.common.SQLiteDataStorage;
import com.sharkdevelop.weather.presentation.weather.data.PlaceInfo;

import dagger.Module;
import dagger.Provides;

@Module
public class WeatherDataStorageModule {

    @Provides
    @PerService
    public static WeatherDataStorage getWeatherDataStorage(SQLiteDataStorage dataStorage, Context appContext) {
        return new WeatherDataStorage(dataStorage);
    }
}
