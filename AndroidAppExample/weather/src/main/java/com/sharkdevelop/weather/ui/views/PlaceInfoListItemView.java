package com.sharkdevelop.weather.ui.views;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.nikolaymakhonin.common_di.contracts.IHasAppComponentBase;
import com.github.nikolaymakhonin.utils_ui.patterns.mvp.BaseView;
import com.sharkdevelop.weather.R;
import com.sharkdevelop.weather.di.components.AppComponent;
import com.sharkdevelop.weather.presentation.weather.data.PlaceInfo;
import com.sharkdevelop.weather.presentation.weather.presenters.PlaceInfoListItemPresenter;
import com.sharkdevelop.weather.presentation.weather.views.IPlaceInfoListItemView;

import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;

public class PlaceInfoListItemView extends BaseView<PlaceInfoListItemPresenter, PlaceInfo> implements
                                                                                           IPlaceInfoListItemView
{

    private static final int LAYOUT = R.layout.place_info_list_item;

    protected AppComponent _appComponent;
    private   CardView     _cardView;
    private   TextView     _nameTextView;
    private   TextView     _temperatureTextView;

    //region Constructors

    public PlaceInfoListItemView(Context context) {
        super(context);
    }

    public PlaceInfoListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PlaceInfoListItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public PlaceInfoListItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    //endregion

    //region Init Controls

    @Override
    protected void initControls() {
        super.initControls();
        _appComponent = ((IHasAppComponentBase<AppComponent>) getContext().getApplicationContext()).getAppComponent();
        LayoutInflater.from(getContext()).inflate(LAYOUT, this, true);
        _cardView = (CardView) findViewById(R.id.cardView);
        _nameTextView = (TextView) findViewById(R.id.name);
        _temperatureTextView = (TextView) findViewById(R.id.temperature);

        _cardView.setOnClickListener(view -> _onClick.onNext(null));

        ViewGroup.LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(layoutParams);
    }

    //endregion

    //region Update View

    @Override
    public void updateView(PlaceInfo placeInfo) {
        if (placeInfo != null) {
            _nameTextView.setText(placeInfo.getPlace().getPlaceName());
            _temperatureTextView.setText(placeInfo.getWeather().getTemperatureString());
        }
    }

    private final Subject _onClick = PublishSubject.create();

    @Override
    public Observable onClick() {
        return _onClick;
    }

    //endregion
}
