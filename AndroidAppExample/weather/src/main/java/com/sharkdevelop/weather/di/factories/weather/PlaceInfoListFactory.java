package com.sharkdevelop.weather.di.factories.weather;

import android.view.ViewGroup;

import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.IRecyclerViewAdapterFactory;
import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.IViewModelPresenter;
import com.sharkdevelop.weather.presentation.common.EventBusCommon;
import com.sharkdevelop.weather.presentation.weather.data.PlaceInfo;
import com.sharkdevelop.weather.presentation.weather.presenters.EventBusWeather;
import com.sharkdevelop.weather.presentation.weather.presenters.PlaceInfoListItemPresenter;
import com.sharkdevelop.weather.presentation.weather.views.IPlaceInfoListItemView;
import com.sharkdevelop.weather.presentation.weather.views.IPlaceInfoView;
import com.sharkdevelop.weather.ui.views.PlaceInfoListItemView;

public class PlaceInfoListFactory implements IRecyclerViewAdapterFactory<IPlaceInfoListItemView, PlaceInfo, IViewModelPresenter<IPlaceInfoListItemView, PlaceInfo>>
{
    private final EventBusCommon  _eventBusCommon;
    private final EventBusWeather _eventBusWeather;

    public PlaceInfoListFactory(EventBusCommon eventBusCommon, EventBusWeather eventBusWeather) {
        _eventBusCommon = eventBusCommon;
        _eventBusWeather = eventBusWeather;
    }

    @Override
    public IPlaceInfoListItemView createItemView(ViewGroup parent, int viewType) {
        return new PlaceInfoListItemView(parent.getContext());
    }

    @Override
    public IViewModelPresenter<IPlaceInfoListItemView, PlaceInfo> createItemPresenter(IPlaceInfoListItemView view,
        int viewType)
    {
        PlaceInfoListItemPresenter presenter = new PlaceInfoListItemPresenter(_eventBusCommon, _eventBusWeather);
        presenter.setView(view);
        return presenter;
    }

    @Override
    public int getItemViewType(int position, PlaceInfo instagramPost) {
        return 0;
    }
}
