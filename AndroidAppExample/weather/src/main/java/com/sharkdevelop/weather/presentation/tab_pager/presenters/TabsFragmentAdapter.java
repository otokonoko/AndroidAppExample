package com.sharkdevelop.weather.presentation.tab_pager.presenters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.github.nikolaymakhonin.logger.Log;
import com.sharkdevelop.weather.presentation.common.EventBusCommon;
import com.sharkdevelop.weather.presentation.tab_pager.data.TabPageIds;
import com.sharkdevelop.weather.ui.fragments.PlaceInfoDetailsFragment;
import com.sharkdevelop.weather.ui.fragments.PlaceInfoListFragment;

import java.util.ArrayList;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;

public class TabsFragmentAdapter extends FragmentStatePagerAdapter {

    private class TabPageState {

        public String Title;

        public Fragment Fragment;

        private TabPageState(String title, Fragment fragment) {
            Title = title;
            Fragment = fragment;
        }
    }

    private static final String LOG_TAG = "TabsFragmentAdapter";

    private final List<TabPageState> _pages = new ArrayList<>();

    private final EventBusCommon _eventBusCommon;

    public TabsFragmentAdapter(FragmentManager fm, EventBusCommon eventBusCommon) {
        super(fm);
        _eventBusCommon = eventBusCommon;
        _pages.add(new TabPageState("Places", PlaceInfoListFragment.getInstance()));
        _pages.add(new TabPageState("Place Details", PlaceInfoDetailsFragment.getInstance()));

        _eventBusCommon
            .doSetTabPageName()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(args -> {
                TabPageState tabPageState = getTabPageState(args.getLeft());
                if (tabPageState == null) {
                    return;
                }
                tabPageState.Title = args.getRight();
                notifyDataSetChanged();
            });
    }

    private TabPageState getTabPageState(int tabPageId) {
        switch (tabPageId) {
            case TabPageIds.PlaceInfoList:
                return _pages.get(0);
            case TabPageIds.PlaceInfoDetails:
                return _pages.get(1);
            default:
                Log.e(LOG_TAG, "Error tabPageId: " + tabPageId);
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return _pages.get(position).Title;
    }

    @Override
    public Fragment getItem(int position) {
        return _pages.get(position).Fragment;
    }

    @Override
    public int getCount() {
        return _pages.size();
    }
}
