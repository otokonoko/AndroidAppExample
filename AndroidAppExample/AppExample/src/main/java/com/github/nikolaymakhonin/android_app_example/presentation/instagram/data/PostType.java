package com.github.nikolaymakhonin.android_app_example.presentation.instagram.data;

public class PostType {
    public static final int Image = 0;
    public static final int Video = 1;
}
