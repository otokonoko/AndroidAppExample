package com.github.nikolaymakhonin.android_app_example.presentation.instagram.views;

import com.github.nikolaymakhonin.android_app_example.presentation.instagram.data.InstagramPost;
import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.IViewModelView;

public interface IInstagramPostView extends IViewModelView<InstagramPost> {

}
