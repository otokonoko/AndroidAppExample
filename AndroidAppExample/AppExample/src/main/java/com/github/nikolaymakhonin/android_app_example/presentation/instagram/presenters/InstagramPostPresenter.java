package com.github.nikolaymakhonin.android_app_example.presentation.instagram.presenters;

import com.github.nikolaymakhonin.android_app_example.presentation.instagram.data.InstagramPost;
import com.github.nikolaymakhonin.android_app_example.presentation.instagram.views.IInstagramPostView;
import com.github.nikolaymakhonin.utils.contracts.patterns.mvp.ViewModelPresenter;

public class InstagramPostPresenter extends ViewModelPresenter<IInstagramPostView, InstagramPost> {


}
