package com.github.nikolaymakhonin.android_app_example.di.factories.instagram;

import com.github.nikolaymakhonin.android_app_example.data.apis.whats_there.dto.InstagramSearchResponse;
import com.github.nikolaymakhonin.android_app_example.presentation.instagram.data.InstagramPost;
import com.github.nikolaymakhonin.android_app_example.presentation.instagram.data.Media;
import com.github.nikolaymakhonin.android_app_example.presentation.instagram.data.PostType;
import com.github.nikolaymakhonin.android_app_example.presentation.instagram.data.User;
import com.github.nikolaymakhonin.logger.Log;
import com.github.nikolaymakhonin.utils.time.DateTime;

public class InstagramDataFactory {

    private static final String LOG_TAG = "InstagramDataFactory";

    public static InstagramPost fromWhatsThereDTO(InstagramSearchResponse.Post data) {
        if (data == null) {
            return null;
        }

        InstagramPost viewModel = new InstagramPost();

        viewModel.setCreatedTime(new DateTime(data.createdTime));
        viewModel.setMedia(
            fromWhatsThereDTO(data.images != null ? data.images.standardResolution : data.videos.standardResolution));
        viewModel.setPostLink(data.link);
        viewModel.setPostType(fromWhatsThereDTO(data.postType));
        viewModel.setUser(fromWhatsThereDTO(data.user));
        viewModel.setTitle(data.caption == null ? null : data.caption.text);

        return viewModel;
    }

    public static int fromWhatsThereDTO(InstagramSearchResponse.Post.PostType data) {
        switch (data){
            case Image:
                return PostType.Image;
            case Video:
                return PostType.Video;
            default:
                Log.e(LOG_TAG, "Unknown PostType: " + data);
                return -1;
        }
    }

    public static User fromWhatsThereDTO(InstagramSearchResponse.Post.User data) {
        if (data == null) {
            return null;
        }

        User viewModel = new User();

        viewModel.setIdentityName(data.username);
        viewModel.setDisplayName(data.fullName);
        viewModel.setAvatar(data.profilePicture);
        viewModel.setId(data.id);

        return viewModel;
    }

    public static Media fromWhatsThereDTO(InstagramSearchResponse.Post.MediaSamples.Media data) {
        if (data == null) {
            return null;
        }

        Media viewModel = new Media();

        viewModel.setMediaLink(data.url);
        viewModel.setHeight(data.height);
        viewModel.setWidth(data.width);

        return viewModel;
    }

}
