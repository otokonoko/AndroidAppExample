package com.github.nikolaymakhonin.utils_tests;

import com.github.nikolaymakhonin.utils.rx.DynamicObservablesMerger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.LessOrEqual;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Action0;
import rx.observables.ConnectableObservable;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class RxTests extends TestBase {

    @Test
    public void mergeTest() throws Exception {

        Subject<Integer, Integer> observable1 = PublishSubject.create();
        Subject<Integer, Integer> observable2 = PublishSubject.create();

        Subject observables = PublishSubject.create();

        observables.onNext(observable1); //should not observe

        int[]     counter = { 0 };
        final int limit   = 5;

        observable1.onNext(-1); //should not observe

        Observable mergedObservable = Observable.merge(observables);

        observables.onNext(observable1); //should not observe

        mergedObservable.subscribe(o -> {
            assertEquals((int) o, counter[0]);

            int count = ++counter[0];
            assertThat(count, is(new LessOrEqual<>(limit)));

            if (count == limit) {
                _lock.countDown();
            }
        });

        observable1.onNext(-1); //should not observe

        observables.onNext(observable1);
        observables.onNext(observable2);

        observable1.onNext(0);
        observable2.onNext(1);
        observable1.onNext(2);
        observable1.onNext(3);
        observable2.onNext(4);

        assertTrue("Request timeout", _lock.await(5, TimeUnit.SECONDS));
    }

    @Test
    public void mergeCachedTest() throws Exception {

        Subject<Integer, Integer> observable1 = PublishSubject.create();
        Subject<Integer, Integer> observable2 = PublishSubject.create();

        Subject observables = PublishSubject.create();

        observables.onNext(observable1); //should not observe

        int[]     counter = { 0 };
        final int limit   = 5;

        observable1.onNext(-1); //should not observe

        Observable mergedObservable = Observable.merge(observables);

        observables.onNext(observable1); //should not observe

        observable1.onNext(-1); //should not observe
        observable1.onNext(-1); //should not observe
        observable1.onNext(-1); //should not observe

        mergedObservable.subscribe(o -> {
            assertEquals((int) o, counter[0]);

            int count = ++counter[0];
            assertThat(count, is(new LessOrEqual<>(limit)));

            if (count == limit) {
                _lock.countDown();
            }
        });

        observables.onNext(observable1);
        observables.onNext(observable2);

        observable1.onNext(0);
        observable2.onNext(1);
        observable1.onNext(2);
        observable1.onNext(3);
        observable2.onNext(4);

        assertTrue("Request timeout", _lock.await(5, TimeUnit.SECONDS));
    }

    @Test
    public void dinamicMergeTest() throws Exception {

        Subject<Integer, Integer> observable1 = PublishSubject.create();
        Subject<Integer, Integer> observable2 = PublishSubject.create();
        Subject<Integer, Integer> observable3 = PublishSubject.create();

        int[]     counter = { 0 };
        final int limit   = 8;

        observable1.onNext(-1); //should not observe

        DynamicObservablesMerger dynamicObservablesMerger = new DynamicObservablesMerger(observable1);
        Observable mergedObservable = dynamicObservablesMerger.observable();

        Action0 detachFunc2 = dynamicObservablesMerger.attach(observable2);

        observable1.onNext(-1); //should not observe
        observable2.onNext(-1); //should not observe
        observable3.onNext(-1); //should not observe

        mergedObservable.subscribe(o -> {
            //assertEquals((int) o, counter[0]);

            int count = ++counter[0];
            assertThat(count, is(new LessOrEqual<>(limit)));

            if (count == limit) {
                _lock.countDown();
            }
        });

        Action0 detachFunc3 = dynamicObservablesMerger.attach(observable3);

        observable1.onNext(0);
        observable2.onNext(1);
        observable3.onNext(2);

        detachFunc2.call();
        observable3.onNext(3);
        observable2.onNext(-1); //should not observe
        observable1.onNext(4);

        detachFunc2 = dynamicObservablesMerger.attach(observable2);

        observable2.onNext(5);
        observable1.onNext(6);
        observable3.onNext(7);

        assertTrue("Request timeout", _lock.await(5, TimeUnit.SECONDS));
    }
}
