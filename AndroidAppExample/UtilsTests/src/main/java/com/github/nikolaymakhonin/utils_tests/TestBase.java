package com.github.nikolaymakhonin.utils_tests;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.concurrent.CountDownLatch;

@RunWith(MockitoJUnitRunner.class)
public abstract class TestBase {

    /**
     * Countdown latch
     */
    protected final CountDownLatch _lock = new CountDownLatch(1);

    @Before
    public void setup() {

    }
}
