package com.github.nikolaymakhonin.utils_tests;

import android.content.Context;


import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

//docs: https://developer.android.com/training/testing/unit-testing/local-unit-tests.html
@RunWith(MockitoJUnitRunner.class)
public abstract class AndroidTestBase extends TestBase {

    //region Init

    private static final String FAKE_APP_NAME = "AppName";

    @Mock
    public Context _mockContext;

    protected void initMockContext() {
        // Given a mocked Context injected into the object under test...
        Mockito.when(_mockContext.getString(R.string.app_name)).thenReturn(FAKE_APP_NAME);
    }

    @Override
    @Before
    public void setup() {
        initMockContext();
    }

    //endregion

    //region Base tests

    @Test
    public void testMockContext() {
        // ...when the string is returned from the object under test...
        String result = _mockContext.getString(R.string.app_name);

        // ...then the result should be the expected one.
        Assert.assertThat(result, CoreMatchers.is(FAKE_APP_NAME));
    }

    //endregion

}